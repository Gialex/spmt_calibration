#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
  
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include <math.h>

#include "spmt_shm.h"

struct spmt_str {
  int client_pid; /* process Id of the client */
  int ev_size;    /* size of the event        */
  int NEV;        /* maximum number of events in the buffer */
  int nev;        /* number of events (it is just a counter) */
  int wr;         /* which buffer is going to be write (server) */
  int rd;         /* which buffer is going to be read - this parameter
		     would not affect the server */

  int pmt_mask;   /* which PMTs will be considered to be used in the trigger */
  int nt1;        /*number of T1 which have been look */
};

struct spmt_ev_h
{
  int32_t nev;
};

struct spmt_threshold_param 
{
  int th1[3],th2[3],outrange[3];
  int counter1[3],counter2[3];
  int mask;
  int t,dt;

  
};

#define SIG SIGRTMIN+15

int spmt_open(struct spmt_shm_str *p,int size,int ev_size)
{
  int ps;
  struct spmt_str *pt;
  FILE *arq;
  ps=sysconf(_SC_PAGE_SIZE); //page length.
  p->fd=shm_open("spmt_shm",O_RDWR|O_CREAT,S_IRUSR|S_IWUSR);
  printf("Small PMT - open shared memory: ");
  if(p->fd>0){
    p->size=((size+sizeof(struct spmt_str)+ps-1)/ps)*ps; //size in integer multiple of page length
    if(ftruncate(p->fd,p->size)==0){
      p->addr=mmap(NULL,p->size,PROT_READ|PROT_WRITE,MAP_SHARED,
		   p->fd,0);
      if(p->addr!=MAP_FAILED){
	pt=(struct spmt_str *) p->addr;
	pt->ev_size=ev_size+sizeof(struct spmt_ev_h);
	pt->NEV=(p->size-sizeof(struct spmt_str))/pt->ev_size;
	pt->nt1=0;
	pt->pmt_mask=7;
	printf("Address: %d; ev_size: %d (include header)",p->addr,pt->ev_size);
	printf("Ok \n");
	return 0;
      } else {
	printf("Map failed \n");
      }
    } else {
      printf("Truncate fail while resizing to: %d\n",p->size);
    }
    close(p->fd);
  } else {
    printf("fail to open shared memory spmt_shm\n");
  }
  p->fd=-1;
  return(1);
}

int spmt_link(struct spmt_shm_str *p)
{
  int ps;
  struct stat st;
  sigset_t sig_set;

  struct spmt_str *pt;
  printf("Small PMT - link to shared memory: ");  
  p->fd=shm_open("spmt_shm", O_RDWR, S_IRUSR|S_IWUSR);
  if(p->fd>0){
    if(fstat(p->fd,&st)==0){
      p->size=st.st_size;
      p->addr=mmap(NULL,p->size,PROT_READ|PROT_WRITE,MAP_SHARED,
		   p->fd,0);
      if(p->addr!=MAP_FAILED){
	pt=(struct spmt_str *)p->addr;
	pt->client_pid=getpid();
	if( sigemptyset(&sig_set)==0 &&
	    sigaddset(&sig_set,SIG)==0 &&
	    sigprocmask(SIG_BLOCK,&sig_set,NULL)==0 ){
	  printf("Ok\n");
	  return 0;
	} else {
	  printf("problem in implement signal: SIG");
	}
      } else {
	printf("could not map spmt_shm\n");
      }
    } else {
      printf("could not see the status of spmt_shm\n");
    }
    close(p->fd);
  } else {
    printf("could not open the shared memory: spmt_shm\n");
  }
  return(1);
}


void spmt_finish(struct spmt_shm_str *p)
{
  /*this fuction would be used by the client to finish the operation */
  struct spmt_str *pt;

  pt=(struct spmt_str *)p->addr;

  if(pt->client_pid==getpid()){
    pt->client_pid=0;
    close(p->fd); 
  }
}

void spmt_close(struct spmt_shm_str *p)
{
  struct spmt_str *pt;

  pt=(struct spmt_str *)p->addr;
  if(pt->client_pid>0){
    kill(pt->client_pid, SIG);
  }
  munmap(p->addr,p->size);
  shm_unlink("spmt_shm");
}

char *spmt_get_address(struct spmt_shm_str *p)
{
  /* it is just a circular buffer */
  struct spmt_str *pt;
  struct spmt_ev_h *header;
  char *addr;
  pt=(struct spmt_str *)p->addr;

  header=(struct spmt_ev_h*)(((char *)pt)+sizeof(struct spmt_str) +
			     pt->wr * pt->ev_size);
  header->nev=pt->nev;
  addr=(((char *)header) + sizeof(struct spmt_ev_h));
  
  
  return(addr);
}

void spmt_wr_ev(struct spmt_shm_str *p)
{
  /* it is just a circular buffer */
  struct spmt_str *pt;
  //printf("spmt_wr_ev enter \n");fflush(stdout);
  pt=(struct spmt_str *)p->addr;
  //printf("addr: %d %d %d\n",pt,pt->wr,pt->NEV);fflush(stdout);
  pt->wr=(pt->wr + 1) % pt->NEV;
  pt->nev++;
  //printf("ev - ok \n");fflush(stdout);
  
  if(pt->client_pid>0){
    //printf("client PID: %d\n",pt->client_pid);
    //fflush(stdout);
    kill(pt->client_pid,SIG);
  }
}


int spmt_get_evt(struct spmt_shm_str *p,char *evt,int *nevt)
{
  struct spmt_str *pt;

  struct spmt_ev_h *header;
  
  sigset_t sig_set;
  siginfo_t sig_info;
  struct timespec timeout;
  sigemptyset(&sig_set);
  sigaddset(&sig_set, SIG);
  pt=(struct spmt_str *)p->addr;
  if(pt->rd != pt->wr){
    header=(struct spmt_ev_h *)(((char *)pt) +
				sizeof(struct spmt_str) +
				pt->rd*pt->ev_size);
    *nevt=header->nev;
    memcpy(evt,
	   (((char *)header)+sizeof(struct spmt_ev_h)),
	   pt->ev_size-sizeof(struct spmt_ev_h));
    //printf("%d ... %d (1)\n",pt->rd,pt->wr);
    pt->rd=(pt->rd + 1) % pt->NEV;
    timeout.tv_sec=0;
    timeout.tv_nsec=0;
    sigtimedwait(&sig_set,&sig_info,&timeout); //just throw away the signal 
    //printf("%d ... %d (1.1)\n",pt->rd,pt->wr);
    return 0;
  }
  
  timeout.tv_sec=1;
  timeout.tv_nsec=0;
  if(sigtimedwait(&sig_set,&sig_info,&timeout)==SIG){
    pt=(struct spmt_str *)p->addr;
    if(pt->rd != pt->wr){
      header=(struct spmt_ev_h*)((char *)pt  + sizeof(struct spmt_str) + pt->rd*pt->ev_size);
      *nevt=header->nev;
      
      memcpy(evt,
	     ((char *)header) + sizeof(struct spmt_ev_h),
	     pt->ev_size-sizeof(struct spmt_ev_h));
      pt->rd=(pt->rd + 1) % pt->NEV;
      //printf("%d ... %d (2)\n",pt->rd,pt->wr);      
      return 0;
    }    
  } //else {
    //printf("Time out ...\n");
    //}
  return(1);
}

#define NSEC_SETTING 100
int spmt_calc_threshold(struct spmt_threshold_param *th, int peak[],int tt)
{
  /*
    input:
    peak -> peak values of the PMT1, PMT2 and PMT3  (WCD)
    tt-> time in second (mostly to keep track of the number
    event rate).

    it mostly change the th 

    function idea:
    =============
    
    the number of event for a given threshold for one particular
    PMT looks to follow the following expression:
    \[N(s)=\beta (s)^{-\lambda}$ \]
    with $\lambda\approx 3$
    This would be a very approximate distribution.

    Therefore, if we want to calculate the threshold, we can
    count first the number of events for a given threshold and then,
    recalculate the threshold again using the expression:

    \[th=\sqrt(\frac{N}{N_w} th_0 \]

    where $N$ is the number of events counted with the threshold $th_0$,
    and $N_w$ the number of events which one want to have.
    
    The following algorithms will try to get two threshold: one to
    have an rate about one per second and another one to have about
    20/hour.

  */

  int retval,ndt,deltat;
  int i;

  int rate_too_far;

  retval=0;
  deltat=tt - th->t;
  rate_too_far=0;
  for(i=0;i<3;i++){
    if(th->th1[i] < peak[2*i]   &&   (th->mask & (1<<i))){
      th->counter1[i]++;
      if( NSEC_SETTING < th->dt && th->th2[i] < peak[2*i] ){
	th->counter2[i]++;
	retval |= 1<<i;
      }
    }
    if((th->mask&(1<<i)) &&
       (th->counter1[i] < deltat/2 || 2*deltat < th->counter1[i]))
      rate_too_far=1; //the rate is too far from the proper value.
  }
  
  if(th->dt < deltat || //time to recalculate the threshold.
     (NSEC_SETTING<th->dt && 100 < deltat  &&  rate_too_far )
     ){ // the threshold is probably wrong

    //reset thee threshold 
    ndt=3600;
    for(i=0;i<3;i++){
      if(fabs(deltat - th->counter1[i]) > 5.*sqrt(deltat) &&
	 (th->mask & (1<<i))){
	//the rate deviation is too high. It will recalculate the
	// threshold and check the problem for a time interval of 5
	// minutes (NSEC_SETTING seconds).
	ndt=NSEC_SETTING; 
      }
      th->th2[i] = sqrt(th->counter1[i]*15./deltat) * th->th1[i]; //90=(3600 sec)/(240 evt) - for single PMT.
      th->th1[i] = sqrt(th->counter1[i]*1./deltat ) * th->th1[i]; //1=(3600 sec)/(3600 evt)
      if(th->th1[i]<20){
	th->th1[i]=20;
	th->outrange[i]++;
      } else if(200<th->th1[i]) {
	th->th1[i]=200;
	th->outrange[i]++;
      } else {
	th->outrange[i]=0;
      }
      if(th->outrange[i]>10){
	th->mask &= ~(1<<i); //mask the PMT in the trigger algorithms (for SPMT).
      }
      th->counter1[i]=0;
      th->counter2[i]=0;
    }
    retval |= 0x8;
    th->dt = ndt;
    th->t  = tt;
  }
  return(retval);
}

int spmt_set_peak(struct spmt_shm_str *p,
		  int peak[],
		  int tt,
		  struct spmt_add_info *info)
{
  /*return 1 if the PMTs which would be considere are above the threshold
    0 - otherwise.

    it is used also to update some parameters for the clients to see that
    the system is working.
  */
  struct spmt_str *pt;
  int aux,i;
  int peak_max,pk;
  int v,n;

  static struct spmt_threshold_param th;

  pt=(struct spmt_str *)p->addr;
  if(pt->nt1==0){
    for(i=0;i<3;i++){
      th.counter1[i]=0;
      th.counter2[i]=0;
      th.th1[i]=50;
      th.th2[i]=600;
    }
    th.dt=NSEC_SETTING; 
    th.t=tt;
    th.mask=pt->pmt_mask;
  }
  pt->nt1++;

  info->th[0] = th.th2[0];
  info->th[1] = th.th2[1];
  info->th[2] = th.th2[2];
  info->mask=th.mask;
  v=spmt_calc_threshold(&th,peak,tt);
  
  /*count the number of PMTs in the of PMTs which have been trigged
    (above the threshold th.th2) 
  */
  n=0;aux=0;
  for(i=0;i<3;i++){
    if(v & (1<<i)){ 
      n++;
    }
    if(info->mask & (1<<i)){ 
      aux++;
    }
  }
  if( (1<aux && n >= aux-1) || //it would have at least n_working - 1 trigged PMTs
      (aux==1 && n>0) ){ //in case there are only 1 PMT working
    return(1);
  }
  return(0);
}

int spmt_running(struct spmt_shm_str *p)
{
  struct spmt_str *pt;
  int i;
  static int nt1=0;
  
  pt=(struct spmt_str *)p->addr;
  i=pt->nt1 - nt1;
  nt1=pt->nt1;
  return(i);
}
