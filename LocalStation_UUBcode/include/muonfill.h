#ifndef _MUONFILL
#define _MUONFILL
#include <stdint.h>
#define NBBIN 69

struct muon_histo {
  uint16_t Offset[4];
  //uint16_t BaseAvg[4];
  uint16_t Base[4][20];
  uint16_t Peak[4][150];  // note: this is bitshifted by 2 (ie x4)
  uint16_t Charge[4][600]; // note: this is bitshifter by 3 (ie x8)
  int32_t Shape[4][NBBIN]; // note: baseline substracted (can be negative)
  uint32_t StartSecond;
  uint32_t EndSecond;
  uint32_t NEntries;
};

struct muon_calib
{
  /*data for SiPM (Calibration channel)  - 
    SiPM want to have a histogram of single Photo-electron, in this way,
    they can have a proper idea of MIP signal in photo-electrons.

    The basic idea is to transfer a histogram of single Photo-electron in the
    place of SSD charge histogram in alternate way (photo-electron histogram, 
    usual histogram, photo-electron histogram, ...) in the T3 requests.

    Therefore, it will be needed to have these informations in available for
    the EvtSrv all the time, in the way the process can decide which information
    would go in the main T3 data stream.

    To have a reasonable histogram, it is needed to have a acquisition of
    about 15 minutes. During this period, it will keep all the variables as 
    0. Therefore, if "StartSecond" is 0, it means that there are not available
    histogram.
   */
  uint32_t StartSecond; /*will be copied as Shape[3][22] */
  uint16_t dtSecond;    /*will be copied as Shape[3][23] */
  uint16_t Offset;      /*will be copied directly to the offset variable */
  uint16_t Charge[600]; /*will be copied to the Charge histogram */
  uint16_t Base[20];    /*will be copied to the Shape[3][1:20] */
  uint16_t Base_excess[2];   /*will be copied as Shape[3][0] and Shape[3][21] */
  /* others Shape[3] values will be set as 0 */
};
  
struct muon_extra
{
  struct muon_calib calibch;
};
  
struct muon_histo_complete
{
  struct muon_histo histo; /*this are the structure which will be sent to CDAS*/
  struct muon_extra extra; /*this is the internal structure to be
			     managed internally*/
};

#define MUON_BUFF_NAME "muon_buff"
#define MUON_BUFF_SIZE 11*sizeof(struct muon_histo)

#endif
