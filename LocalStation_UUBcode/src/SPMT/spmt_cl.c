#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <sys/socket.h>
#include <netdb.h>

#include <bzlib.h>

#include <shwr_evt_defs.h>
#include <spmt_shm.h>

#include <evbdefs.h>
#include <muonfill.h>
#include <run_config.h>

#include "spmt_prot.h"
struct conn_ctrl
{
  char prefix[80];
  int fd;
  int t_hour;
  int nw;
};

void muon_histo_init(EvbId *h)
{
  int err;
  EvbId aux;
  while(( err= EvbInit( &aux, MUON_BUFF_NAME,
			MUON_BUFF_SIZE) ) != SUCCESS){
    sleep(1);
  }
  printf("%d\n",aux);
  *h=aux;
}

int get_muon_histogram(EvbId *h_m, struct muon_histo_complete *h_compl)
{
  int nb;

  struct muon_histo *h;

  h=&(h_compl->histo);

  nb = sizeof( struct muon_histo );
  if ( EvbGetLastTag( *h_m , (void *) (h),&nb, 5, 0, 0) != SUCCESS ) {
    printf("Problem to get histo\n");
    return 1;
  }
  return(0);
}


int muon_histogram_calc_param(struct muon_histo *h,struct muon_histo_param *p)
{

  p->StartSec=h->StartSecond;

  int i,pos;
  
  for(i=0;i<4;i++){

  // copy the offset from muon_histo struct
    p->offset[i] = h->Offset[i];

  // find "head-and-shoulder": from the upper side of the histogram,
  // search for local maximum (head), surrounded by drops (shoulders)
  // with shoulder/head value ratio of ChargeShoulderHeadRatio

    int chargeBin = 0;
    int shoulderLow = 0;
    int shoulderHigh = 0;

    float ChargeShoulderHeadRatio = 0.75;
    int LowerLimitCountsPerBin = 320;
    const int size = 400;
    int ANN[4] = {80,80,80,6}; // PMT1, PMT2, PMT3, SSD

    int start = size - 1;
    while(h->Charge[i][start] < LowerLimitCountsPerBin){
      --start;
      if(start <= 2) break;
    }

    int maxBin = start;
    int maxValue = h->Charge[i][start];
    int value = h->Charge[i][start-1];
    int value1 = h->Charge[i][start-2];
    int value2, reducedMax;

    for (pos = start - 1; pos >= 2; --pos) {
      if (maxValue < value) {
        maxValue = value;
        maxBin = pos;
      }
      reducedMax = maxValue * ChargeShoulderHeadRatio;
      // require 3 consecutive values to be lower than reducedMax
      // to qualify as a shoulder
      value2 = h->Charge[i][pos-2];
      if (value <= reducedMax && value1 <= reducedMax && value2 <= reducedMax) {
        shoulderLow = pos;
        break;
      }
      value = value1;
      value1 = value2;
    }
    
    if (shoulderLow) {
      // find upper shoulder
      reducedMax = maxValue * ChargeShoulderHeadRatio;
      value = h->Charge[i][maxBin+1];
      value1 = h->Charge[i][maxBin+2];
      for (pos = maxBin + 1; pos < size - 2; ++pos) {
        value2 = h->Charge[i][pos+2];
        if (value <= reducedMax && value1 <= reducedMax && value2 <= reducedMax) {
          shoulderHigh = pos;
          break;
        }
        value = value1;
        value1 = value2;
      }
    
      if (shoulderHigh) {

        //Searching for the maximum btw shoulderLow & shoulderHigh
           //The main algorithms in this code is:
           //\sum_{i=2}^{N+1} x_i=(\sum_{i=1}^{N} x_i) + x_{N+1} - x_1
           //However, as we want just to see where is the maximum of the distribution,
           //we can look the maximum value only for the
           //a_{i+1}=a_{i} + x_{N+1} - x_i
           //because the difference on the sum and a_{i} will be just a constant.
        //The average is taken over ann bins.

	int v,max,bmax,j,ann;
        int amin = shoulderLow;
        int amax = shoulderHigh;

        v=0;
        max=v;
        bmax=amin;
	      if(ANN[i] < amax-amin) ann = ANN[i];
	      else ann = amax-amin;

        for(j=amin+1;j<amax-ann;j++){
          v += h->Charge[i][j+ann]-h->Charge[i][j-1];
          if(max<v){
            max=v;
            bmax=j;
          }
        }
        chargeBin = bmax+ann/2;

      }//sholderHigh
    }//shoulderLow

    p->charge[i]=chargeBin;

  }//PMTs

/*
  int v,max,s,ss,bmax,i,j;

  int pmin[4]={30,30,30,15},
    pmax[4]={100,100,100,100},
    pnn[4]={20,20,20,20};
  int amin[4]={80,80,80,40},
    amax[4]={400,400,400,400},
    ann[4]={30,30,30,30};

  int pmin[3]={30,30,30},
    pmax[3]={100,100,100},
    pnn[3]={20,20,20};
  int amin[3]={80,80,80},
    amax[3]={400,400,400},
    ann[3]={30,30,30};


  //calculate the baseline - \frac{\sum_i  i*base_i}{\sum_i base_i}
  //The Offset is not considered because it would be the
  //same for all the histograms (base, peak and charge).

  for(i=0;i<3;i++){
    s=0;ss=0;
    for(j=0;j<20;j++){
      s += h->Base[i][j];
      ss+= h->Base[i][j] * (j-10);
    }
    if(s==0){
      memset(p,0,sizeof(struct muon_histo_param));
      return(1);
    }
    p->base[i]=ss*100/s; // it is a fixed point calculation
  }

  //Searching for the maximum of the Peak histogram.
  //It consider the maximum when the histogram is maximum
  //between the histogram bin: pmin and pmax. The average is taken from
  //pnn bins.

    //The main algorithms in this code is:
    //\sum_{i=2}^{N+1} x_i=(\sum_{i=1}^{N} x_i) + x_{N+1} - x_1

    //However, as we want just to see where is the maximum of the distribution,
    //we can look the maximum value only for the
    //a_{i+1}=a_{i} + x_{N+1} - x_i
    //because the difference on the sum and a_{i} will be just a constant.

  for(i=0;i<3;i++){
    v=0;
    max=v;
    bmax=pmin[i];
    for(j=pmin[i]+1; j<pmax[i]-pnn[i]; j++){
      v +=  h->Peak[i][j+pnn[i]] - h->Peak[i][j-1] ;
      if(max<v){
	max=v;
	bmax=j;
      }
    }
    p->peak[i]=bmax+pnn[i]/2;
  }

  //Searching for the maximum of the Charge histogram.
  //Similar algorithms used in the Peak.

  for(i=0;i<3;i++){
    v=0;
    max=v;
    bmax=amin[i];
    for(j=amin[i]+1;j<amax[i]-ann[i];j++){
      v += h->Charge[i][j+ann[i]]-h->Charge[i][j-1];
      if(max<v){
	max=v;
	bmax=j;
      }
    }
    p->charge[i]=bmax+ann[i]/2;
  }
  */

  return(0);
}


//int init_conn(struct conn_ctrl *c)
//{
//  struct sockaddr_in MyConn;
//  struct hostent *Host;
//  if(c->network==1){
//    c->ok=1;  /* it will consider always fine for network connection */
//    Host = gethostbyname (c->ServerAddr);
//    if (Host == NULL) {
//      printf ("Error getting hostname\n");
//      c->fd=-1;
//      return -1;
//    }
//    MyConn.sin_family = AF_INET;
//    MyConn.sin_addr.s_addr = ((struct in_addr*)(Host->h_addr))->s_addr;
//    MyConn.sin_port = htons(c->port);
//    c->fd = socket(AF_INET, SOCK_STREAM, 0);
//    if (c->fd == -1) {
//      printf("I couldn't open TCP socket\n");
//      return -1;
//    }
//
//    // connect to the server
//    if (connect(c->fd, (struct sockaddr *)&MyConn,sizeof (MyConn)) == -1){
//      printf ("Error connecting to server\n");
//      close(c->fd);
//      c->fd=-1;
//      return -1;
//    }
//    return c->fd;
//  } else {
//    c->ok=1;
//    c->fd=open(c->ServerAddr,O_WRONLY|O_CREAT);
//    if(c->fd<0){
//      c->ok=0;
//    }
//    c->nw=0;
//  }
//}
//
//int write_to_server(struct conn_ctrl *c,char *buff,int size)
//{
//  int nw,n,nn;
//  struct timespec dt,dt_remind;
//
//  if(c->network==1){
//    if(c->fd==-1){
//      if(init_conn(c)<0){
//	printf("Not possible to connect\n");
//	return(1);
//      }
//    }
//    nw=0;
//    do{
//      if(size-nw>1024){
//	n=1024;
//      } else {
//	n=size-nw;
//      }
//      nn=write(c->fd,buff+nw,n);
//      if(nn<0){
//	close(c->fd);
//	init_conn(c);
//	return(1);
//      } else {
//	nw+=nn;
//      }
//      dt.tv_sec=1;
//      dt.tv_nsec=0;
//      nanosleep(&dt,&dt_remind);
//    } while (nw<size);
//    return(0);
//  } else {
//    printf("write to file ... %d %d\n",c->nw,c->nw_max);
//    if(c->nw < c->nw_max){
//      nw=0;
//      while(nw<size){
//	nn=write(c->fd,buff+nw,size-nw);
//	if(nn<0){
//	  c->nw_max=c->nw;
//	  close(c->fd);
//	  c->fd=-1;
//	  return(1);
//	} else {
//	  nw+=nn;
//	}
//      }
//      c->nw+=nw;
//      return(0);
//    } else {
//      close(c->fd);
//      c->ok=0;
//    }
//  }
//  return(1);
//}

int new_file(struct conn_ctrl *c,int t)
{
  char filename[100];
  if(c->fd>0){
    close(c->fd);
  }
  c->t_hour=(t/3600)*3600;
  sprintf(filename,"%s_%010d",c->prefix,c->t_hour);
  c->fd=open(filename,O_RDWR|O_CREAT|O_APPEND,S_IRWXU);
  printf("FD...%d \n",c->fd);
  return(c->fd);
}

int spmt_wr(struct conn_ctrl *conn,struct spmt_msg *msg,
	    struct buff_str *dt)
{
  int s,nw,i,err,size;
  char *pt;
  size=SPMT_MAX_PACKET_SIZE;
  if(BZ2_bzBuffToBuffCompress(msg->dat,&(size),
			      (char *)(dt->dat),dt->used+1,
			      1,0,0)==BZ_OK){
    msg->h.size=size;
    printf("Going to write to the server/file: %d \n",msg->h.size);
    i=write(conn->fd,"SPMT",4);
    if(i!=4){
      printf("Error while writting the file ...\n");
      return(1);
    }
    err=0;
    nw=0;
    s=msg->h.size + sizeof(msg->h);
    pt=(char *)msg;
    while(nw<s && err<10){
      i=write(conn->fd,pt+nw,s-nw);
      if(i>0){
	nw+=i;
	err=0;
      } else {
	err++;
      }
    }
    printf("written: %d\n",nw);
    if(err<10){
      return(0);
    }
  }
  return(1);
}

void use_as(char *cmd)
{
  printf("use as: \n %s <dirname>\n",cmd);
  exit(0);
}


int add_buff(struct buff_str *dt,char *msg,int size,enum buff type)
{
  //include the message in the buffer.
  //the buffer structure would have the following structure:
  //size,type,data,size,type,data,...,0
  //return:
  //   * the total used buffer (on success);
  //   * -1 in case it is not possible to include the message
  printf("add_buff: %d  %d %d ",type,size,dt->used);
  if(0<size && size<256 && size+2+1<BUFF_SIZE_MAX-dt->used){//2+1: total message size plus one additional byte to indicate end of message
    dt->dat[dt->used]=size+2;//2 is to include the size and type information
    dt->dat[dt->used+1]=type;
    memcpy(dt->dat+dt->used+2,msg,size);
    dt->used+=dt->dat[dt->used];
    dt->dat[dt->used]=0;
    printf(" ... %d\n",dt->used);
    return(dt->used);
  }
  printf("Not used \n");
  return(-1);
}


main(int narg,char *argv[])
{
  int i;
  CONFIG *cf;

  uint32_t stid,dtsize;
  int newsize;
  EvbId h_m;

  int fd;
  int ndwr;

  int flag,first_evt;
  struct spmt_shm_str pt;/*to manage the event appearing ... */

  struct buff_str dt;

  struct spmt_msg *msg; /*structure of the data to be transfered */
  int hist_prev_sec;

  struct conn_ctrl conn;

  int ns_nt1;
  struct muon_histo_complete histos_complete;
  struct muon_histo *histos;
  struct muon_histo_param h_param;
  uint32_t h_prev_startsecond;
  struct spmt_evt *evt;
  struct evt_features evt_info;
  struct spmt_add_info spmt_daq;
  struct timeval tv;
  int tt;

  while(gettimeofday(&tv,NULL)!=0);
  tt=tv.tv_sec;
  if(narg<2){
    use_as(argv[0]);
  }

  strcpy(conn.prefix,argv[1]);
  conn.fd=-1;
  if(new_file(&conn,tt)<0){
    printf("Not possible to open file\n");
    return(1);
  }
  cf=configlib_Initialize();
  stid=cf->LogicalId;

  muon_histo_init(&h_m);

  evt=(struct spmt_evt *)malloc(sizeof(struct spmt_evt));
  msg=(struct spmt_msg *)malloc(sizeof(struct spmt_msg));

  ns_nt1=0;
  flag=spmt_link(&pt);
  printf("spmt link ... %d\n",flag);
  dt.used=0;
  dt.dat[dt.used]=0;
  msg->h.Id=stid;
  first_evt=1; //it is the first event in the file.
  h_prev_startsecond=0;
  if(flag==0){
    printf("Start the main loop\n");
    while(conn.fd>0 && ns_nt1<130){ /*ns_nt1 controls the number of
				    seconds which there are not any
				    event (no T1)*/
      if(spmt_get_evt(&(pt),(char *)evt,&(evt_info.nevt))==0){
	ns_nt1=0;
	if(spmt_daq.th[0]!=evt->info.th[0] ||
	   spmt_daq.th[1]!=evt->info.th[1] ||
	   spmt_daq.th[2]!=evt->info.th[2] ||
	   spmt_daq.t_start!=evt->info.t_start ||
	   first_evt){
	  //add daq information
	  add_buff(&dt,(char *)&(evt->info),sizeof(struct spmt_add_info),DAQ_INFO);
	  for(i=0;i<3;i++){
	    spmt_daq.th[i]=evt->info.th[i];
	    spmt_daq.r[i] =evt->info.r[i];
	  }
	  spmt_daq.t_start=evt->info.t_start;

	  printf("adding trigger info %d %d %d  %d %d %d\n",
		 spmt_daq.th[0],
		 spmt_daq.th[1],
		 spmt_daq.th[2],
		 spmt_daq.r[0],
		 spmt_daq.r[1],
		 spmt_daq.r[2]
		 );
	}

	if(get_muon_histogram(&h_m,&histos_complete)!=0){
	  printf("Histogram not found ...\n");
	  memset(&h_param,0,sizeof(struct muon_histo_param));
	  add_buff(&dt,(char *)&h_param,sizeof(h_param),MUON_INFO);
	} else {
	  histos=&(histos_complete.histo);
	  if(h_prev_startsecond!=histos->StartSecond || first_evt){
	    muon_histogram_calc_param(histos,&h_param);
	    //add muon histogram information
	    add_buff(&dt,(char *)&h_param,sizeof(h_param),MUON_INFO);
	    h_prev_startsecond=histos->StartSecond;
	    printf("adding new histo info %d\n",h_prev_startsecond);
	  }
	}
	//include the event information
	evt_info.sec=evt->evt.raw.ev_gps_info.second;
	//evt_info.ticks=evt->evt.raw.ev_gps_info.ticks;

	//IF WE WANT THE SPARE
	//for(i=0;i<4;i++) evt_info.AREA_PEAK[i]=evt->evt.extra.AREA_PEAK[2*i];
	//evt_info.AREA_PEAK[4]=evt->evt.extra.AREA_PEAK[7];
	//IF WE WANT THE SSD LOW GAIN
	for(i=0;i<5;i++) evt_info.AREA_PEAK[i]=evt->evt.extra.AREA_PEAK[2*i];

	//for(i=0;i<4;i++) evt_info.BASELINE[i]=evt->evt.extra.BASELINE[i];
	//evt_info.triggers=evt->evt.extra.trigger;
	//evt_info.extra=evt->evt.extra;
	add_buff(&dt,(char *)&(evt_info),sizeof(evt_info),EVT);
	printf("adding new event %d\n",dt.used);

	if(gettimeofday(&tv,NULL)==0){
	  tt=tv.tv_sec;
	}
	if( (BUFF_SIZE_MAX-(dt.used+1) <
	     (sizeof(evt_info)+sizeof(struct spmt_add_info)+sizeof(h_param)+2*3))
	    || conn.t_hour+3600 < tt
	    ) {
	  //it would be safe to close everything and restart again.
	  msg->h.Id=stid;
	  spmt_wr(&conn,msg,&dt);
	  close(conn.fd);
	  return(0);
	  //first_evt=1;
	}
	//else {
	first_evt=0;
	//
      }	else {
	fflush(stdout);
	if(spmt_running(&(pt))==0){
	  ns_nt1++;
	} else {
	  ns_nt1=0;
	}
	if(ns_nt1>0){
	  printf("get out ... %d\n",ns_nt1);
	}
      }
    } //while(conn.ok && ns_nt1<130) - end

    if(dt.used>0){
      msg->h.Id=stid;
      spmt_wr(&conn,msg,&dt);
    }
    if(conn.fd>0){
      close(conn.fd);
    }
  }
}
