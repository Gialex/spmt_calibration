#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "spmt_read.h"

#define dac_range 4096
#define c_in_max 2.5
#define c_in_to_HV 1050
#define gain_a -16.5
#define gain_b 7.5

float log_gain_calc(int dac){

  float c_in_volt = c_in_max * ((float)dac)/dac_range;
  float SPMT_HV = c_in_volt * c_in_to_HV;
  float log_gain = gain_a + gain_b * log10(SPMT_HV);
  return log_gain;
}

int dac_calc(float log_gain){

  float SPMT_HV = (log_gain - gain_a)/gain_b;
  SPMT_HV = pow(10,SPMT_HV);
  float c_in_volt = SPMT_HV/c_in_to_HV;
  int dac = ((int)(c_in_volt * dac_range/c_in_max));
  return dac;
}


int main(int argc,char *argv[])
{
  
  int spmt_dac_init = atoi(argv[1]);

  char set_dac[100];
  snprintf(set_dac,100,"slowc -p 4 -v %d",spmt_dac_init);
  printf("%s \n",set_dac);
  //int out = system(set_dac);
  //printf("System out = %d \n",out);
  
  float log_gain_init = log_gain_calc(spmt_dac_init);
  
  struct spmt_read_str *spmt_rd;
  struct muon_histo_param mm;
  struct spmt_add_info    th;
  struct evt_features     evt;

  int MAX_EVT = 10000; // Gialex 03/07/19
  int aux,i;
  
  if(argc<3){
    printf("use as: %s <initial dac> <inputfile1> [inputfile2,...]\n",argv[0]);
    return(0);
  }
  spmt_rd=spmt_read_init(argc-2,argv+2);

// Gialex 03/07/19 --> using the 'spmt_read_getevt' function 
// to check if there are events to start with, causes the loss
// of the first event for the calibration

  if(spmt_read_getevt(spmt_rd,&mm,&th,&evt)!=0)
    return(0);
  
  int totalsmall=0; // Number of events

  int m1, m2, m3, sat1, sat2, sat3, sat4, sat5;
  int GPS, GPSns, nevt;
  int ch1, ch2, ch3, ch4, ch5;
  int peak1, peak2, peak3, peak4, peak5;
  int b1, b2, b3, b4, bhg1, bhg2, bhg3, bhg4;
  int off1, off2, off3, th1, th2, th3;
  float base1, base2, base3;
  double ad1, ad2, ad3, p1, p2, p3, VEM1, VEM2, VEM3;
  int GPStime = 0;

  int PoP_entries[3] = {0,0,0};
  float mean_PoP[3] = {0,0,0};

  int AoA_entries[3] = {0,0,0};
  float mean_AoA[3] = {0,0,0};
  
  for(i=0;i<MAX_EVT;i++) {

    //printf("nevt: %d\n",i);

    if(spmt_read_getevt(spmt_rd,&mm,&th,&evt)!=0){
      printf("nevt: %d\n",i);
      break;
    }

// Gialex 03/07/19
// Control if the given maximum number of events is reached
    if(i==MAX_EVT-1) printf("\nMax number of events (= %d) reached\n\n",MAX_EVT);

   totalsmall++;

   GPStime=evt.sec;
   nevt=evt.nevt;
   ch1=(evt.AREA_PEAK[0]& 0x7FFFF);
   ch2=(evt.AREA_PEAK[1]& 0x7FFFF);
   ch3=(evt.AREA_PEAK[2]& 0x7FFFF);
   ch4=(evt.AREA_PEAK[3]& 0x7FFFF);
   ch5=(evt.AREA_PEAK[4]& 0x7FFFF);
   peak1=((evt.AREA_PEAK[0]>>19)& 0xFFF);
   peak2=((evt.AREA_PEAK[1]>>19)& 0xFFF);
   peak3=((evt.AREA_PEAK[2]>>19)& 0xFFF);
   peak4=((evt.AREA_PEAK[3]>>19)& 0xFFF);
   peak5=((evt.AREA_PEAK[4]>>19)& 0xFFF);
   sat1=((evt.AREA_PEAK[0]>>31)&0x1);
   sat2=((evt.AREA_PEAK[1]>>31)&0x1);
   sat3=((evt.AREA_PEAK[2]>>31)&0x1);
   sat4=((evt.AREA_PEAK[3]>>31)&0x1);
   sat5=((evt.AREA_PEAK[4]>>31)&0x1);
   m1=((th.mask & 1) == 1);
   m2=((th.mask & 2) == 2);
   m3=((th.mask & 4) == 4);
   th1=th.th[0];
   th2=th.th[1];
   th3=th.th[2];
   ad1=(th.r[0]!=0 ? th.r[0]/100. : 32.);
   ad2=(th.r[1]!=0 ? th.r[1]/100. : 32.);
   ad3=(th.r[2]!=0 ? th.r[2]/100. : 32.);
   off1=mm.offset[0];
   off2=mm.offset[1];
   off3=mm.offset[2];
   VEM1=(mm.charge[0]<<=3);
   VEM2=(mm.charge[1]<<=3);
   VEM3=(mm.charge[2]<<=3);

   if(peak4 > 0 && ch4 > 0){
     if(!sat1 && m1){
       mean_PoP[0] += ((float)peak1)/peak4;
       PoP_entries[0]++;
       mean_AoA[0] += ((float)ch1)/ch4;
       AoA_entries[0]++;
     }
     if(!sat2 && m2){
       mean_PoP[1] += ((float)peak2)/peak4;
       PoP_entries[1]++;
       mean_AoA[1] += ((float)ch2)/ch4;
       AoA_entries[1]++;
     }
     if(!sat3 && m3){
       mean_PoP[2] += ((float)peak3)/peak4;
       PoP_entries[2]++;
       mean_AoA[2] += ((float)ch3)/ch4;
       AoA_entries[2]++;
     }
}
   
  }//end loop on events

  spmt_read_finish(spmt_rd); //F. Fenu 21/03/2019

  
  if(PoP_entries[0] > 0){
    mean_PoP[0] /= PoP_entries[0];
    mean_AoA[0] /= PoP_entries[0];
  }
  if(PoP_entries[1] > 0){
    mean_PoP[1] /= PoP_entries[1];
    mean_AoA[1] /= PoP_entries[1];
  }
  if(PoP_entries[2] > 0){
    mean_PoP[2] /= PoP_entries[2];
    mean_AoA[2] /= PoP_entries[2];
  }
  

  printf("Peak ratios : %.2f - %.2f - %.2f \n",mean_PoP[0],mean_PoP[1],mean_PoP[2]);
  printf("Charge ratios : %.2f - %.2f - %.2f \n",mean_AoA[0],mean_AoA[1],mean_AoA[2]);
  
  float PoP = 0;
  if(m1) PoP = mean_PoP[0];
  else if(m2) PoP = mean_PoP[1];
  else if(m3) PoP = mean_PoP[2];

  if(!(PoP > 0)){
    printf("Impossibile SPMT setting !  No peak_over_peak available \n");
    return(1);
  }

  printf("SPMT current PoP = %.1f \n",PoP);

  float PoP_wanted = 40.;

  if( PoP > 0.95 * PoP_wanted && PoP < 1.05 * PoP_wanted ){
    printf("SPMT setting accomplished with PoP = %.1f \n",PoP);
    return(0);
  }
  else{
    float factor = PoP/PoP_wanted;
    printf("SPMT gain needs to be multiplied by a factor %.2f \n",factor);

    float new_log_gain = log_gain_init + log10(factor);
    int new_dac = dac_calc(new_log_gain);

    if(new_dac < 1248){
       printf("SPMT init. dac = %d  -- final dac %d \n",spmt_dac_init, new_dac);
       new_dac = 1248;
    }
    if(new_dac > 2450){
      printf("SPMT init. dac = %d  -- final dac %d \n",spmt_dac_init, new_dac);
      new_dac = 2450;
    }
    
    printf("SPMT init. dac = %d  -- final dac %d \n",spmt_dac_init, new_dac);
    snprintf(set_dac,100,"slowc -p 4 -v %d",new_dac);
    printf("%s \n",set_dac);
    //out = system(set_dac);
    //printf("System out = %d \n",out);
    
  }
  
  
}
