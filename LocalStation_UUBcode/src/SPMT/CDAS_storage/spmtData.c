#include "spmtData.h"

void spmtData::Print() const {
  std::cout << "spmtData : tankID = " << tankID << ", N = " << Nevents << std::endl;
}

void spmtData::ResetData() {

  tankID = 0;
  maskedStatus = 7;
  Nevents = 0;
  GPSsec.clear();
  evtID.clear();
  Ncalibrations = 0;
  ChargeCalibTime.clear();
  Nthresholds = 0;
  ThresholdTime_LPMTs.clear();

  for(int i=0;i<5;i++){
    charge_FADC[i].clear();
    peak_FADC[i].clear();
    saturation[i].clear();
    if(i<4){
      VEM[i].clear();
      Offset[i].clear();
      if(i<3){
	Threshold_LPMTs[i].clear();
	HG_LG[i].clear();
      }
    }
  }

}

ClassImp(spmtData);
