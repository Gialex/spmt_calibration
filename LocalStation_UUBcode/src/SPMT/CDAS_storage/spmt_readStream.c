#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <shwr_evt_defs.h>
#include <muonfill.h>
#include <bzlib.h>
#include <spmt_prot.h>

#include "spmt_readStream.h"


struct spmt_read_str *spmt_stream_init()
{
  struct spmt_read_str *str;
  str=(struct spmt_read_str *)malloc(sizeof(struct spmt_read_str));
  if(str!=NULL){
    str->index=0;
    str->nd_raw=0;
    str->pt=0;
    str->nd=0;
  } else {
    printf("Not possible to allocate the memory for SPMT data management\n");
    return NULL;
  }
  return(str);
}

void spmt_stream_finish(struct spmt_read_str *str)
{
  if(str!=NULL){
    free(str);
  }
}


int spmt_read_data(char* complete_buffer, unsigned int complete_buffer_size, struct spmt_read_str *str)
{
  struct spmt_h h;
  char *pt;
  int n,missing_data,bzerr;
  do{
    printf("Enter in new data loop \n");
    missing_data=1;
    if(4 + sizeof(struct spmt_h) < str->nd_raw ){
      printf("there are some data ... %d\n",str->nd_raw);
      pt=(char *)memmem((void *)str->raw_buff,str->nd_raw,"SPMT",4);
      if(pt!=NULL){
	n=pt - str->raw_buff ;
	printf("Preamble found (shifted of %d char)\n",n);
	if(pt!=str->raw_buff && n < str->nd_raw ){
	  memmove(str->raw_buff, pt, str->nd_raw - n );
	  str->nd_raw -=n;
	}
	if(4 + sizeof(struct spmt_h) < str->nd_raw){
	  memcpy(&h,str->raw_buff + 4,sizeof(h)); //4 is to skip "SPMT"
	  printf("Header found ... %d %d %d\n",h.Id,h.nevt,h.size);
	  if(4 + sizeof(h) + h.size <= str->nd_raw){
	    printf("The entire sPMT information is available.\n");
	    pt=str->raw_buff + 4 + sizeof(h);
	    str->nd=BUFF_SIZE;
	    bzerr=BZ2_bzBuffToBuffDecompress(str->buff,&(str->nd),
					     pt,h.size,0,0 );
	    if(BZ_OK==bzerr){
	      str->pt=0;
	      printf("uncompressed data: %d\n",str->nd);
	      missing_data=0;
	      n=h.size + 4 + sizeof(h) ;
	    } else {
	      printf("BZIP error!!! %d\n",bzerr);
	      n= 4 + sizeof(h);
	    }

	    if(str->nd_raw - n >0){
	      pt=str->raw_buff + n;
	      memmove(str->raw_buff,pt,str->nd_raw - n);
	      str->nd_raw -=n;
	    } else {
	      str->nd_raw=0;
	    }
	    
	  } else {
	    printf("Not enough data w.r.t. header ...\n");
	  }
	} else {
	  printf("Header not found ...\n");
	}
      } else {
	printf("Preamble not found. Discarding the current raw buffer.\n");
	str->nd_raw=0;
      }
    }

    if(missing_data){
      //not enough data to process. make another copy from stream ...
      unsigned int length = complete_buffer_size;
      printf("Adding new data from stream of length %d at position %d ...\n",length,str->index);
      
      if(complete_buffer==NULL || length == 0){
	printf("The complete buffer is empty.\n");
	return(1);
      }

      if(length - str->index > BUFF_SIZE - str->nd_raw){
	printf("Adding %d bytes at raw_buffer position %d.\n",BUFF_SIZE - str->nd_raw,str->nd_raw);
	memcpy(str->raw_buff+str->nd_raw,
	       complete_buffer+str->index,
	       BUFF_SIZE - str->nd_raw);
	n = BUFF_SIZE - str->nd_raw;
	str->nd_raw+=n;
	str->index+=n;
      } else {
	if(length - str->index > 0){
	  printf("Adding %d remaining bytes at raw_buffer position %d.\n",length - str->index,str->nd_raw);
	  memcpy(str->raw_buff+str->nd_raw,
		 complete_buffer+str->index,
		 length - str->index);
	  n=length - str->index;
	  str->nd_raw+=n;
	  str->index+=n;
	} else {
	  printf("Reached end of the buffer.\n");
	  if(str->index != length){
	    printf("Length of the complete buffer (=%d) different from final index (=%d).\n Finish anyway.\n",str->index,length);
	  }
	  //str->nd_raw=0;
	  //str->nd=0;
	  return(0);
	}
      }
      
    }
    
  }while(missing_data);

  return(0);
}


int spmt_read_getevt(struct spmt_read_str *str,
		     struct muon_histo_param *mm,
		     struct spmt_add_info *th,
		     struct evt_features *evt)
{
  int new_data=0;
  
  while( (str->pt <= str->nd ) &&
	 (str->buff[str->pt] > 0) &&
	 new_data == 0){ //there are not more data, need to request
                         //more data from the file.

    if(str->buff[str->pt + 1 ] == DAQ_INFO){
      //printf("DAQ_INFO\n"); //debug 
      memcpy(&(str->th),&(str->buff[str->pt + 2]),str->buff[str->pt]-2);
    } else if(str->buff[str->pt + 1 ] == MUON_INFO){
      //printf("MUON_INFO\n"); //debug 
      memcpy(&(str->mm),&(str->buff[str->pt + 2]),str->buff[str->pt]-2);	  
    } else if(str->buff[str->pt + 1 ] == EVT){
      //printf("EVT...\n"); //debug 
      memcpy(&(str->evt),&(str->buff[str->pt + 2]),str->buff[str->pt]-2);

      memcpy(evt,&(str->evt),sizeof(str->evt));
      memcpy(mm,&(str->mm)  ,sizeof(str->mm));
      memcpy(th,&(str->th)  ,sizeof(str->th));
      new_data=1;
      printf("evt %d - GPSsec %d\n",str->evt.nevt,str->evt.sec); //debug 
    }
    str->pt+=str->buff[str->pt];
  }

  if(new_data==0){
    //printf("No new data.\n"); //debug
    return(1);
  }
  //printf("Got new data.\n"); //debug 
  return(0);
   
}
