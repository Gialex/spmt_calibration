#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <shwr_evt_defs.h>
#include <muonfill.h>
#include <bzlib.h>
#include <spmt_prot.h>

#include "spmt_readFiles.h"

void spmt_read_finish(struct spmt_read_file *str)
{
  if(str!=NULL){
    if(str->arq!=NULL)
      fclose(str->arq);
    free(str);
  }
}

int spmt_read_open_file(struct spmt_read_file *str)
{
  // return 0 if file opening is ok
  //        1 otherwise

  if(str->arq!=NULL){
    fclose(str->arq);
    str->arq=NULL;
  }

  str->arq=fopen(str->file,"r");
  printf("going to open file %s\n",str->file);

  if(str->arq==NULL)
    return(1);

  return(0);
}

struct spmt_read_file *spmt_read_init(char *file)
{
  struct spmt_read_file *str;
  str=(struct spmt_read_file *)malloc(sizeof(struct spmt_read_file));

  if(str!=NULL){
    str->file=file;
    str->arq=NULL;
    if(spmt_read_open_file(str)!=0){
      free(str);
      printf("No file is possible to read\n");
      return(NULL);
    }
  } else {
    printf("Not possible to allocate the memory for SPMT data management\n");
    return NULL;
  }
  return(str);
}

int spmt_get_buffer(struct spmt_read_file *str)
{
  // return 0 if buffer is filled
  //        1 otherwise
  
  fseek (str->arq, 0, SEEK_END);
  str->buff_length = ftell (str->arq);
  if(str->buff_length<=0){
    printf("No information for the buffer (empty file?) \n");
    return(1);
  }
  fseek (str->arq, 0, SEEK_SET);

  str->raw_buff = (char*)malloc(str->buff_length);
  if(str->raw_buff == NULL){
    printf("Not possible to allocate the memory for SPMT buffer\n");
    return(1);
  }

  if(fread (str->raw_buff, sizeof(char), str->buff_length, str->arq)){
    return(0);
  } else {
    printf("Read 0 bytes from file !? \n");
    return(1);
  }
  
}
