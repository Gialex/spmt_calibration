#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

#include "spmt_readStream.h"
#include "spmt_ROOT.h"

#include "spmtData.h"

#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>

#define MAX_ENTRIES 1000

int HasTTree(TFile* f)
{
  if( !f->GetListOfKeys()->Contains("spmt_monit") ){
    if(MakeTTree(f)){
      return(1);
    } else {
      printf("Not possible to create the TTree...\n");
      return(0);
    }
  }
  return(1);
}

int MakeTTree(TFile* f)
{
  f->cd();
  TTree* t = new TTree("spmt_monit", "events for spmt calib");
  
  spmtData* data = new spmtData();
  t->Branch("data", &data);

  return(t->Write());
}


int AddEntry(TFile* f, UInt_t tank, struct spmt_read_str *spmt_s, char* stream, UInt_t stream_length)
{
  f->cd();

  struct muon_histo_param mm;
  struct spmt_add_info    th;
  struct evt_features     evt;

  TTree* t = (TTree*)f->Get("spmt_monit");

  spmtData* data = NULL;
  t->SetBranchAddress("data", &data);

/////

  data->ResetData();
  UInt_t lastCalibGPS = 0;
  UInt_t lastThresholdsGPS =	0;
 
// Loop over events in file 
  for(int i=0;i<MAX_ENTRIES;i++) {

    if(spmt_read_getevt(spmt_s,&mm,&th,&evt)!=0){
      if(spmt_read_data(stream, stream_length,spmt_s)==0){
	if(spmt_read_getevt(spmt_s,&mm,&th,&evt)!=0){
	  break;
	}
      }
    }

    if(i==MAX_ENTRIES-1) printf("\nMax number of allowed events per hour (= %d) reached\n\n",MAX_ENTRIES);
	
    data->Nevents++;
    data->maskedStatus = data->maskedStatus & th.mask;
   
    data->GPSsec.push_back(evt.sec);
    data->evtID.push_back(evt.nevt);

    for(int j=0; j<5; j++){
      data->charge_FADC[j].push_back((evt.AREA_PEAK[j]& 0x7FFFF));
      data->peak_FADC[j].push_back(((evt.AREA_PEAK[j]>>19)& 0xFFF));
      data->saturation[j].push_back(((evt.AREA_PEAK[j]>>31)&0x1));
      if(j<4) data->VEM[j].push_back(4+(mm.charge[j]<<3));
    }

    if(mm.StartSec != lastCalibGPS){
      lastCalibGPS = mm.StartSec;
      data->Ncalibrations++;
      data->ChargeCalibTime.push_back(mm.StartSec);
      for(int j=0; j<4; j++)
	data->Offset[j].push_back(mm.offset[j]);
    }
    
    if(th.t_start != lastThresholdsGPS){
      lastThresholdsGPS = th.t_start;
      data->Nthresholds++;
      data->ThresholdTime_LPMTs.push_back(th.t_start);
      for(int j=0; j<3; j++){
	data->Threshold_LPMTs[j].push_back(th.th[j]);
	data->HG_LG[j].push_back(float(th.r[j])/100.);
      }
    }
    
  }//end loop on events

  if(data->Nevents>0){
    data->tankID = tank;
    t->Fill();
    // Write the new TTree (with updated entries)
    // and only after erase the old one
    t->Write(0,TObject::kWriteDelete,0);

  }

  return(data->Nevents);
}
