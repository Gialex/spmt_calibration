#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

#include "spmt_readFiles.h"
#include "spmt_readStream.h"
#include "spmt_ROOT.h"

#include <TROOT.h>
#include <TFile.h>


int main(int argc,char *argv[])
{

  if(argc<4){
    printf("use as: %s <tankID> <date (yyyymmdd)> <inputfile> \n",argv[0]);
    return(0);
  }

  // DATA STREAM
  // Should be given from an external process.
  // In this application, it is filled from file
  char*	data_buffer;
  UInt_t data_buffer_length;

//////////
// Reading of smpt calib file.
// This part will be removed
// when spmt data will actually
// arrive to CDAS as a stream

  struct spmt_read_file *spmt_f;
  spmt_f=spmt_read_init(argv[3]);
  if(spmt_f==NULL){
    spmt_read_finish(spmt_f);
    return(1);
  }

  if(spmt_get_buffer(spmt_f)!=0){
    spmt_read_finish(spmt_f);
    return(1);
  }

  printf("Added a stream from file of length %d \n",spmt_f->buff_length);

  // Copy the information from the readFile structure
  // to a generic buffer to simulate the data stream
  data_buffer_length = spmt_f->buff_length;
  data_buffer = (char*)malloc(data_buffer_length);
  memcpy(data_buffer,spmt_f->raw_buff,data_buffer_length);

  spmt_read_finish(spmt_f);
  
//////////
//////////

  struct spmt_read_str *spmt_s;
  
// Start reading data stream
  // Blocks of dimension BUFF_SIZE are copied
  // from the stream to a temporary buff
  // inside the spmt_read_str structure to
  // simplify and secure the data management.
  spmt_s=spmt_stream_init();
  if(spmt_read_data(data_buffer,data_buffer_length,spmt_s)!=0){
    spmt_stream_finish(spmt_s);
    free(data_buffer);
    return(1);
  }

// ROOT file
  char title[50]; strcpy(title,"spmt_");
  strcat(title,argv[2]); // add date to the title
  strcat(title,".root");
  TFile* f = new TFile(title, "UPDATE");

// Checks presence of monit TTree in the ROOT file
// It creates the TTree if not present
  if(HasTTree(f)){
    printf("spmt_monit TTree present in %s.\n",title);
  } else {
    spmt_stream_finish(spmt_s);
    free(data_buffer);
    f->Close();
    return(1);
  }

// Add one entry (correspondent to a certain number N of small showers)
// for one tank (given in argv[1]) to the TTree. If N=0, no events found.
  int N = AddEntry(f,atoi(argv[1]),spmt_s,data_buffer,data_buffer_length);
  if(N){
    printf("\n Adding an entry with %d events for tank %d. \n",N,atoi(argv[1]));
  } else {
    printf("\n No events to be added !?\n");
  }

/////
  
  f->Close();
  spmt_stream_finish(spmt_s);
  free(data_buffer);

  return(0);
}
