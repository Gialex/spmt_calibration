#ifndef SPMT_READSTREAM
#define SPMT_READSTREAM

#include <spmt_prot.h>
#include <spmt_shm.h>
#define BUFF_SIZE SPMT_MAX_PACKET_SIZE+10

struct spmt_read_str
{
  unsigned int nd_raw,pt,nd;
  unsigned int index;
  char raw_buff[BUFF_SIZE],buff[BUFF_SIZE];
  struct muon_histo_param mm;
  struct spmt_add_info th;
  struct evt_features evt;
};

struct spmt_read_str *spmt_stream_init();
void spmt_stream_finish(struct spmt_read_str *str);
int spmt_read_data(char* complete_buffer, unsigned int complete_buffer_size,
		   struct spmt_read_str *str);
int spmt_read_getevt(struct spmt_read_str *str,
		     struct muon_histo_param *mm,
		     struct spmt_add_info *th,
		     struct evt_features *evt);

#endif 
