#ifndef SPMT_READFILES
#define SPMT_READFILES

#include <spmt_prot.h>
#include <spmt_shm.h>


struct spmt_read_file
{
  char *file;
  FILE *arq;
  unsigned int buff_length;
  char *raw_buff;
};

int spmt_read_open_file(struct spmt_read_file *str);
struct spmt_read_file *spmt_read_init(char *file);
void spmt_read_finish(struct spmt_read_file *str);
int spmt_get_buffer(struct spmt_read_file *str);


#endif 
