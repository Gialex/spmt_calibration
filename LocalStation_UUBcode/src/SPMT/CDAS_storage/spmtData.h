#ifndef _spmtData_
#define _spmtData_

#include <TObject.h>
#include <iostream>
#include <vector>

class spmtData : public TObject
{
 public:

  spmtData(){ };
  virtual ~spmtData(){ };

  UInt_t tankID;
  UInt_t maskedStatus;
  UInt_t Nevents;
  std::vector<UInt_t> GPSsec;
  std::vector<UInt_t> evtID;
  std::vector<UInt_t> charge_FADC[5];
  std::vector<UInt_t> peak_FADC[5];
  std::vector<UInt_t> saturation[5];
  std::vector<UInt_t> VEM[4];

  UInt_t Ncalibrations;
  std::vector<UInt_t> ChargeCalibTime;          // GPS time of the VEM/MIP calibration
  std::vector<UInt_t> Offset[4]; 		// offset (or pedestal) of the VEM/MIP charge custom calculation

  UInt_t Nthresholds;				// number of threshold check/changes
  std::vector<UInt_t> ThresholdTime_LPMTs; 	// GPS time of the threshold change for the event selection 
  std::vector<UInt_t> Threshold_LPMTs[3]; 	// thresholds for the event selection
  std::vector<Float_t> HG_LG[3];
  void ResetData();
  void Print() const;

  ClassDef(spmtData,2);
};
#endif
