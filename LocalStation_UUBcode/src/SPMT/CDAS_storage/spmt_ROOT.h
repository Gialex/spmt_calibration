#ifndef SPMT_ROOT
#define SPMT_ROOT

#include "spmt_readStream.h"

#include <TROOT.h>
#include <TSystem.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>


int HasTTree(TFile* f);
int MakeTTree(TFile* f);
int AddEntry(TFile* f, UInt_t tank, struct spmt_read_str *str,
	     char* stream, UInt_t stream_length);

#endif 
