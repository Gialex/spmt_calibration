#!/bin/bash

run_dir=/home/root

mkdir $run_dir/daq/spmt/

$run_dir/spmt_cl $run_dir/daq/spmt/spmt &> $run_dir/daq/spmt/spmt.ou

sleep 2

## Then, line to add to /etc/inittab
# spmt:2345:respawn:/home/root/spmt.sh
# and then run "telinit q"
