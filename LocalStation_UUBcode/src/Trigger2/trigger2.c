/*
   The idea in the development of trigger2 is to
     enable part by part of the code, in the way it is possible to
     test what have been developed.
   The original had not "//" comments, Therefore most
     most of the code which has "//" is because was in the original code.
     
   Aug 2013: 
   changed _asm() to PROG_VERSION() (in compat.h header)
   changed _os_ things to defs in compat.h header


   $Author: guglielm $
   $Date: 2008-03-26 10:32:32 $
   $Log: not supported by cvs2svn $
   Revision 1.3  2007/10/30 11:31:52  guglielm
   Send message if cursec too old

   Revision 1.2  2007/09/12 14:32:34  guglielm
   Cleanup (replaced st-> by st-> and cf-> by cf->)

   Revision 1.1.1.1  2007/09/11 09:55:01  guglielm
   First import Src

   Revision 1.8  2006/03/10 19:57:32  barawn
   Import of v0r9b4p0: added Cyclone compatibility, CLF handling,
   T3Already/NotFound FD bug

   Revision 1.7  2005/08/31 16:12:30  bertou
   v0r9b3p1
   resetting TT if GPS!=TT
   'GRB' scaler is now difference of scaler 1 and 2
   Scalers read on 1 PPS and no longer on 1st T1
   putting S/N info in sn.dat

    Modified Files:
    	Defs/calibx.h Defs/evbdefs.h Defs/fedefs.h Defs/feevts.h
    	Defs/fev3_defs.h Defs/logfile.h Defs/monitor.h
    	Defs/run_config.h Defs/sigauger.h Defs/status.h Defs/svrdm.h
    	Defs/versions.h Src/Control/control.c Src/Gpsctrl/gpsprocess.c
    	Src/Gpsirq/gpsirq.c Src/MiniSpy/minispy.c
    	Src/MsgSvrIn/msgsvrin.c Src/Trigger2/trigger2.c
    	Src/Trigger2/xbtrig.c

   Revision 1.5  2004/05/20 14:51:03  bertou
   Mufill with muon decay stuff. A few D/A things.
    Modified Files:
    	Dmaread/dmaread.c MuFill/mufill.c Trigger2/trigger2.c
    	Trigger2/xbtrig.c

   Revision 1.4  2004/01/22 14:51:26  barawn
   Fit anode to dynode D/A method implemented in xbtrig.c.
   Right now it writes it to a file "dynodeanode.fit" so
   we can check the stability of the method and how it compares
   to the rate-based method.
   xbtrig.c also now has a bunch of "utility functions" for math,
   like "DivideAndRound" (which now handles all positive integer divisions),
   Shift1/6/7, which shifts and rounds as well.

   Revision 1.3  2003/12/29 16:08:20  bertou
   Removing LED, and using rate based method for D/A estimation
    Modified Files:
    	ledana.c trigger2.c xbtrig.c

   Revision 1.2  2003/12/15 21:08:24  bertou
   Better handling of PMTs appearing and changing gain.
   TOT are now correctly stamped.
    Modified Files:
    	trigger2.c xbtrig.c

   Revision 1.18  2002/04/11 09:14:40  guglielm
   Many changes
   Implemented use of Xavier T2 algo and calibration
   Added tests on bad edges, bad time order and bad anode/dynode orde.
   Cleanup
   Cosmetics

   Revision 1.17  2001/12/04 09:37:49  guglielm
   Added use of _EVBDEFS_MAIN_ for definition of evbid and mubid.
   Added OS9 module edition number with TRIGGER2_VERSION

   Revision 1.16  2001/11/13 16:40:25  guglielm
   Cosmetics
   Added test discripancy between GPS current time and TTAG time
   Suppressed references to EA_BUFFER
   Added trigger type handling

   Revision 1.13  2001/05/28 09:01:22  guglielm
   Cosmetics

   Revision 1.12  2001/05/15 08:49:10  guglielm
   *** empty log message ***

   Revision 1.11  2001/05/14 13:05:17  guglielm
   Modify energy evaluation (EA)
   Add 2 T2 algorithms

   Revision 1.10  2001/05/09 09:42:21  guglielm
   Cosmetics, cleanup, ...

   Revision 1.9  2001/04/24 08:23:22  revenu
   computation of the energy optimized

   Revision 1.8  2001/04/23 12:24:26  revenu
   bug corrected : pdata->fadc123 instead of fadc123..
   idem for fadc456

   Revision 1.7  2001/04/23 12:18:04  revenu
   new evalution of the deposed energy (with the EA buffer format,
   without sommation over the PMTs)

   Revision 1.6  2001/04/12 15:29:10  guglielm
   Cosmetics

   Revision 1.5  2001/04/12 11:17:22  guglielm
   Reshuffled for use of siglib

   Revision 1.3  2001/04/04 07:39:46  revenu
   no message

   Revision 1.2  2001/03/29 10:21:11  jmb

   Use nano second instead of deca-nano

   Revision 1.1.1.1  2001/03/28 14:33:19  revenu
   new LS

   Revision 1.6  2001/03/12 09:46:18  os9

   Src/Trigger2/trigger2.c Src/Trigger2/trigger2.r

   Revision 1.5  2001/03/01 13:01:56  os9
   debugged version (1/03/01)

*/
/* 
   trigger2 : Local Station Software - Pierre Auger Project
   ----------
   JM Brunet, L Gugliemi, B. Revenu, G Tristram
   College de France - Paris

   Created    01/06/97 - JMB
   Modified : 13/10/97 - Send all T2 contained in one second
   16/11/98 - Version 2
   02/03/98 - Version 2.2:Data sent to Central Station for each
   Trigger 2 (20 bits) : 
   -  4 bits : energy evaluation
   - 20 bits : micro second of the 1 part.
   03/01/01 - Version for Engineering Array

   --------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>

#include <time.h>
#include <errno.h>
#include <string.h>
//#include <semaphore.h>
//
//#define MAIN
#define MYNAME  TRIGGER2
//
#include "ready_acq.h"

//#include "dmcompatlib.h"
//
//
//#include "central.h"

//
//#define _CALIBX_MAIN_
//#include "calibx.h"
//
//#define _EVBDEFS_MAIN_ 

//#include "gps_defs.h"
//#include "versions.h"
//#include "compat.h"
//
#include <logfile.h>
#include <fastlib.h> 
#include <feshwrread_defs.h> /*it is mostly to define where and size of 
			       fastlib definition */
#include <evbdefs.h>
#include <fe_lib.h>
#include <siglib.h>
#include <sigauger.h>
#include <augererr.h>
#include <linux.h>

#include "calibx.h"

#include "trigger2_global.h"
#include "trace_modif.h"
#include "xbtrig.h"
#include "spmt_shm.h"
#include "trigger2_evt_serv_imp.h"


/*not really used in general code. It is mostly to make a spmt_debug ... 
  --- init*/
#include <sys/mman.h>
/*---- end */

///** UB Changes: PSA 4/19/03... or 4/20... it's too late to tell.
//
//    Here's the code to handle the buffer shifts: anywhere where there
//    was pdata+1, p+1, etc. to increment to the anode, use (p+ANODE_OFFSET).
//    Anyplace where you have p++, change to (p+ANODE_OFFSET).
//
//    Anyplace you have p++, p += 2 to shift to the next sample, 
//    use p += INCREMENT. Make sure you change any p++ -> anode shifts to
//    (p+ANODE_OFFSET). You can also replace anywhere you have i*2, or 760*2,
//    etc. with i*INCREMENT.
//
//    The D/A tags also changed, so there were declared here. **/
//
//#define BAD_EDGE_EVENT 0x8000
//
//PROG_VERSION( TRIGGER2_VERSION );
//
//
//
//int EnergyThresh= 0xFFF;           /**** for T2Algorithm= 2   ****/
//
//#define NMAX_MEAN  100             /**** for PMT mean trigger ****/
//
//
//extern void *_glob_data; //normally it is not needed
//
//
//int PrevEdge = TTAG_GOOD_EDGES ;
//
//
//#include "augerirq.h"
//static event_id EnableIrqEvtId;
//
struct spmt_calib
{
  /* structure to store information of some calibration parameters
     
     It is mainly to calculate the HG/LG ratio
   */
  int t_start; //start time.
  int32_t s[3],n[3]; //for each WCD PMT.
  int32_t r[3];//ratios ...
  
};
struct spmt_manager
{
  struct spmt_calib calib;  
  struct spmt_shm_str shm;
};


struct trigger2_global_str
{
  FB_handler *fastbuffer;
  EvbId evbid; /*this is not clear if it is really needed to be in this process ...*/
  int T2Algorithm;
  struct spmt_manager spmt;
  //  struct debug_with_trig_str *debug_trigg;

  
};

static struct trigger2_global_str gl;
struct trigger2_GLOBAL_str GL; 

struct debug_trigger2
{
  int first;
  uint32_t id[4];
  uint32_t sec[4],ticks[4];
  int index;

  uint32_t t2_prev_sec;
  uint32_t t2_list[50];
  int nt2;

  int nerr;
};
static struct debug_trigger2 debug_tr2;


struct spmt_debug_str{
  int32_t tt[10];
  int32_t counter[10];
  int32_t val[1004];
} *spmt_debug;

void debug_tr2_init()
{
  debug_tr2.first=1;
  debug_tr2.index=0;
  debug_tr2.nerr=0;
  debug_tr2.nt2=0;
  debug_tr2.t2_prev_sec=0;
}
void debug_tr2_update(struct shwr_evt_complete *ev_c)
{
  int p,n,i;
  struct shwr_evt_raw *ev;
  ev=&(ev_c->raw);
  p=(debug_tr2.index+3) & 0x3;
  n=(debug_tr2.index+1) & 0x3;

  if(debug_tr2.first==0){
    if(ev->id !=debug_tr2.id[p]+1){
      printf("(id,sec,ticks):\n");
      for(i=0;i<4;i++){
	printf("%d %d %d\n",
	       debug_tr2.id[(n+i)&3],
	       debug_tr2.sec[(n+i)&3],
	       debug_tr2.ticks[(n+i)&3]);
      }
      printf("ev: %d  %d  %d\n",
	     ev->id,
	     ev->ev_gps_info.second,
	     ev->ev_gps_info.ticks);	     
    }
  } 
  i=debug_tr2.index;
  debug_tr2.first=0;
  debug_tr2.id[i]=ev->id;
  debug_tr2.sec[i]=ev->ev_gps_info.second;
  debug_tr2.ticks[i]=ev->ev_gps_info.ticks;

  debug_tr2.index=n;
}


void debug_show()
{
  int n,i;
  printf("show !!!\n");
  n=debug_tr2.index;
  for(i=0;i<4;i++){
    printf("%d %d %d\n",
	   debug_tr2.id[(n+i)&3],
	   debug_tr2.sec[(n+i)&3],
	   debug_tr2.ticks[(n+i)&3]);
  }
  debug_tr2.nerr++;
  if(debug_tr2.nerr>3){
    exit(0);
  }
}

void debug_t2(uint32_t e,uint32_t sec)
{
  int i;
  if(debug_tr2.t2_prev_sec!=sec){
    if(debug_tr2.t2_prev_sec>0){
      printf("TRIGGER2 DEBUG t2 sec: %d %d:\n",debug_tr2.t2_prev_sec,debug_tr2.nt2);
    }
    debug_tr2.t2_prev_sec=sec;
    debug_tr2.nt2=0;
  }
  if(debug_tr2.nt2<50){
    debug_tr2.t2_list[debug_tr2.nt2]=e;
  }
  debug_tr2.nt2++;
}

/*end xxx */
///* Gamma Ray Burst function*/
//int addGrbScaler( unsigned char *buff ) ;
//
///* Special CLF handling. Set to 1 by signal SIG_ENABLE_CLF,
//   set to 0 by SIG_DISABLE_CLF
//*/
//int enableClf = 0 ;
//int addToClfData( TimeStamp *date0, TimeStamp *date1, int micros ) ;
//void getClfCfg() ;
//int finishClf() ;
//
//
//
//
//
void sig_hand( signal_code s )
{
  PushSig( s ) ;
  /*  signalHandlerFinish(); */
}
//
//static LinkIrqEvt()
//{
//  int32 wait_inc= -1, sig_inc=1;
//  u_int32 perm = 0x777, color = 0;
//
//  if (EnableIrqEvtId != 0) return;
//  if (eventLink(&EnableIrqEvtId, ENABLE_IRQ_EVT_NAME ) != SUCCESS ) {
//    PrintLog( LOG_ERROR, "Can't link %s!\n",
//	      ENABLE_IRQ_EVT_NAME) ;
//    exit(1) ;
//  }
//}
//
//static void StopTriggers()
//{
//  unsigned int value ;
//  
//  eventSetOr( EnableIrqEvtId, &value, DISABLE_IRQ_TTAG_MASK, 0 );
//}
//
//static void StartTriggers()
//{
//  unsigned int value ;
//  
//  eventSetOr( EnableIrqEvtId, &value, ENABLE_IRQ_MASK, 0);
//}
//
//void ResetTtagFE( void )
//{
//  u_int32 tm=0x200; /* To sleep ~ 2 seconds */
//  signal_code dummy;
//
//  LinkIrqEvt();
//
//  StopTriggers() ;
//  
//  FB_reset(gl.fastbuffer) ;
//
//  /* One should also set the event to 0, avoiding getting fakes Fast Buffers */
//  eventSet( st->event.IdT1Trigger2, 0, 0x8000 ) ;
//  PrintTimedLog( LOG_WARNING, "FB reset and T1Tg2 event cleared\n" ) ;
//
//  compatSleep( &tm, &dummy );
//  
//  StartTriggers() ;
//}



error_code CheckOrder( struct shwr_evt_raw *pevt )
{
  static int second = 0, nanos = 0, badorder = 0 ;
  
  if ( pevt->ev_gps_info.second != second ) {
    second = pevt->ev_gps_info.second ;
    nanos = pevt->ev_gps_info.ticks;
  } else {
    if ( pevt->ev_gps_info.ticks <= nanos ) {
      /* We have a problem here.
	 Should try to reset TTAG and FE
      */
      if ( badorder == 0 ) {
	/*rs ... start  it would include st->inter...*/
	PrintTimedLog( LOG_WARNING, "Bad order( %d <= %d)\n",
		       pevt->ev_gps_info.ticks, nanos) ;
	/*rs ... end */
//	if ( Debug ) SendGenericMsgStr( GL.Pproc->mbx_out, CONTROL_VERSION,
//  "Bad order %d", pevt->ev_gps_info.second ) ;
	/* Disable triggers - wait for a while
	   enable triggers */
	badorder = 1 ;
      }
      return -1 ;
    }
    nanos = pevt->ev_gps_info.ticks;
  }
  badorder = 0 ;
  
  return SUCCESS ;
}


void spmt_send(struct spmt_manager *p,struct shwr_evt_complete *evt_c,
	       struct trace_parameters *evt_param)
{
  static int nn=0;
  int peak[6],i,x,y;
  
  struct spmt_evt *ev;

  struct spmt_add_info addinfo;

  if (p->shm.fd>0){ /*it has a shared memory */
    for(i=0;i<6;i++){
      peak[i]=(evt_c->extra.AREA_PEAK[i]>>19) & 0xFFF;
    }

    /*simple calibration calculation - it would be better to do it better ...
      
      It simple average calculation of ratio $\frac{A_HG}{A_LG}$,
      and looks the signal of HG is quite big and not saturated.
    */
    if(spmt_debug->val[0]!=evt_c->raw.ev_gps_info.second){
      spmt_debug->val[0]=evt_c->raw.ev_gps_info.second;
      spmt_debug->tt[0]=spmt_debug->val[0];
      spmt_debug->counter[0]=0;

    }
    spmt_debug->counter[0]++;
    if(p->calib.t_start==0){
      p->calib.t_start=evt_c->raw.ev_gps_info.second;
      spmt_debug->tt[1]=spmt_debug->val[0];
      spmt_debug->counter[1]++;
    } else {
      spmt_debug->tt[2]=spmt_debug->val[0];
      spmt_debug->counter[2]++;      
      if(evt_c->raw.ev_gps_info.second < p->calib.t_start + 600){
        spmt_debug->tt[3]=spmt_debug->val[0];
	spmt_debug->counter[3]++;	
	for(i=0;i<3;i++){
	  if(1500<peak[2*i+1] &&
	     (evt_c->extra.AREA_PEAK[2*i+1] & 0x80000000)==0
	     ){
	    //signal is big enough (HG) and the signal is not saturated
	    x=evt_c->extra.AREA_PEAK[2*i]&0x3FFFF;
	    y=evt_c->extra.AREA_PEAK[2*i+1]&0x3FFFF;
	    if(x>0){
	      p->calib.n[i]++;
	      p->calib.s[i]+=y*100/x;
	    }
	  }
	}
      } else {
        spmt_debug->tt[4]=spmt_debug->val[0];
	spmt_debug->counter[4]++;	
	PrintLog(LOG_INFO,"SPMT =================xx : %d; ",
		 p->calib.t_start);

	for(i=0;i<3;i++){
	  if(p->calib.n[i]>0){
	    p->calib.r[i]=p->calib.s[i]/p->calib.n[i];
	  } else {
	    p->calib.r[i]=0;
	  }
	  p->calib.s[i]=0;
	  p->calib.n[i]=0;
	}
	p->calib.t_start=0;
      }
    }

    
    if(spmt_set_peak(&(p->shm),peak,evt_c->raw.ev_gps_info.second,&addinfo)==1){
      spmt_debug->tt[5]=spmt_debug->val[0];
      spmt_debug->counter[5]++;
      nn++;
      addinfo.r[0]=p->calib.r[0];
      addinfo.r[1]=p->calib.r[1];
      addinfo.r[2]=p->calib.r[2];
      addinfo.t_start=p->calib.t_start;
      ev=(struct spmt_evt *)spmt_get_address(&(p->shm));
      
      memcpy(&(ev->evt),evt_c,sizeof(struct shwr_evt_complete));
      memcpy(&(ev->info),&addinfo,sizeof(struct spmt_add_info));
      spmt_wr_ev(&(p->shm));
    }    
  }
}


//
//#define MAX_TABLE_AGE 900 /* 15 minutes */
//
//void applyOffset(struct shwr_evt *pevt, OffsetTable *ttable)
//{
//  static int bad_table = 0;
//  unsigned int index;
//
//  index = pevt->date0.nano >> TABLE_SHIFT;
//  if (index >= TTABLE_NUMBER) index = TTABLE_NUMBER-1;
//  if (abs(pevt->date0.second - ttable->create_time) > MAX_TABLE_AGE)
//    {
//      int create_time = ttable->create_time;
//      pevt->micro_off = 0;
//      if (!bad_table)
//	{
//	  bad_table = 1;
//	  SendGenericMsgStr(GL.Pproc->mbx_out, CONTROL_VERSION,
//			    "Bad timing table: %d (cur: %d) - using none",
//			    create_time, pevt->date0.second);
//	  PrintLog(LOG_WARNING, "No good timing table (%d/%d).\n",
//		   create_time, pevt->date0.second);
//	}
//      return;
//    }
//  else
//    {
//      bad_table = 0;
//      pevt->micro_off = ttable->offset[index];
//    }
//}


int addGrbScaler( unsigned char *buff )
{
  unsigned int value, rawvalue ;
  static u_int32 prev_micro = 0 ;
  float ratio ;

  if ( GL.cf->FeParams.V2Settings.GrbEnabled == 0 ) return 0 ;
  value = GL.st->GrbValue ;
  GL.st->GrbCount++ ;
  if ( value == 0 ) return ;

  /* Add to T2 bufffer */
  if ( value >= 0xFFFFF ) value = 0xFFFFF ;
  *buff++ = (value >> 16) | GRB_T2_TAG ;
  *buff++ = (value >> 8 ) & 0xFF ;
  *buff++ = value & 0xFF ;
  return 1 ;
}




int T2Algo( struct shwr_evt_raw *evt, struct trace_parameters *evt_param,
	    uint32_t *pemic )
{
  unsigned int eval= 0x7;
  uint32_t micro=0;
  static double ticks2micro=0;
  static int sec=0;
  if(sec!=evt->ev_gps_info.second){
    sec=evt->ev_gps_info.second;
    if(evt->ev_gps_info.ticks_prev_pps<60000000){
      printf("TRIGGER2: WARNING!!! - The number of counter clock cicles \n"
	     "is less the 1/2 of expected value. It would be 120000000\n"
	     "and have count %d\n",evt->ev_gps_info.ticks_prev_pps);
    }
    ticks2micro=1.e6/evt->ev_gps_info.ticks_prev_pps;
  }
  micro = (uint32_t)(evt->ev_gps_info.ticks*ticks2micro); 
  /*
   * PSA - OK, here's where we need to apply the timing offsets.
   * They're already in the event structure.
   */
  micro += evt->micro_off;
//
//  /* Addition for CLF handling */
//  if ( enableClf ) addToClfData( &evt->ev_gps_info, &evt->date1, micro ) ;
//
  //if (micro>=1000000) {
  //    PrintTimedLog(LOG_ERROR,"Microsecond too high: %d (%d), calib100 %d\n",
  //		    micro, evt->micro_off,
  //		    GL.st->SawTooth.table[evt->ev_gps_info.second & SAWTOOTH_MASK].calib100);
  //    
  //}
  switch( gl.T2Algorithm ) {
  //case RANDOM_T2_ALGO:                   /* Random 1/5 */
  //  if( !prob( 5)) return 0;
  //  break;
  case XAVIER_T2_ALGO:                    /* Xavier function */
    eval= XbCalibTrigger( evt,evt_param);
    if( !eval) break ;
  default:                           /* Any other value = no algorithm */
    eval = 1 ;
    break;
  }
  *pemic = eval << 20 | micro ;
  //printf("TRIGGER2 - check event type \n");
  if( (evt->Evt_type_1 | evt->Evt_type_2 )
      & ( FE_SHWR_TRIG_MASK_TOT |
	  FE_SHWR_TRIG_MASK_TOTD |
	  FE_SHWR_TRIG_MASK_MOPS ) 
      )
    /*the event have been trigged as ToT type ( the Evt_type_1 has
      one, at least, one of the bits set: TOT, TOTD and MOPS).
      Or, at least, may be considerer as ToT type (The Evt_type_2
      has one TOT type set */
    *pemic |= TOT_T2_TAG ;
  return eval ;
}

int trigger2( struct shwr_evt_raw *pevt,
	      struct trace_parameters *evt_param)
{
  static MESSAGE *pmes;
  static int size_t2_yes= 0, oldsec= 0;
  error_code err;
  uint32_t Emicro, t;
//  signal_code dummy;
  static unsigned char *mesbuf;
  static int IsAlloc= 0;
  int cursec= pevt->ev_gps_info.second;
  int diff, count, doSend;

  if ( GL.st->GpsState != GPS_STATUS_OK ){
    if ( IsAlloc) {
      pmes->mheader.length= size_t2_yes;
      PostSendShrink(  GL.Pproc->mbx_out, pmes, size_t2_yes);
    }
    size_t2_yes = 0 ;
    IsAlloc = 0 ;
    oldsec = cursec ;
    return 0 ;
  }
  if( pevt == NULL) {  /* Pause run requested */
    if( IsAlloc) {
      pmes->mheader.length= size_t2_yes;
      PostSendShrink(  GL.Pproc->mbx_out, pmes, size_t2_yes);
    }
    size_t2_yes= 0;
    IsAlloc = 0 ; 
    return 0 ;
  }
  /* Compute E, micro & T2 algorithm */
  //printf("TRIGGER2 - going to enter in the T2Algo \n");
  if( !T2Algo( pevt, evt_param, &Emicro)){
    return 0;
  }
  debug_t2(Emicro,pevt->ev_gps_info.second);
  /* If new second, send all previous T2 
     and allocate a new mailbox buffer */
  if( IsAlloc && cursec != oldsec) {
    GL.st->trg2.second = cursec;
    /* Now time to read and clear the Gamma Ray Burst scaler
       And add to message the message with the proper T2 Tag
    */
    if ( addGrbScaler( mesbuf ) ) {
      mesbuf += 3 ;
      size_t2_yes += 3 ;
    }
    if( size_t2_yes != 0) {
      doSend = 1 ;
      pmes->mheader.length= size_t2_yes;
      /* at this point, oldsec should be equal to st->GpsCurrentSec -1
	 or may be st->GpsCurrentSec if the cursec event occurred
	 just before gpsirq has been awakened, but in any case should
	 never be higher than st->GpsCurrentSec !
      */
      if ( oldsec > GL.st->GpsCurrentSec ) {
	if (!diff)
	  diff = oldsec - GL.st->GpsCurrentSec;
	if (oldsec - GL.st->GpsCurrentSec == diff) {
	  if (count++ > 10) {
	    count = 0;
	    /* Reset the TTAG. Maybe it just blipped. */
	    GL.st->GpsSeconds = GL.st->GpsCurrentSec;
	    /* This isn't the right way to do it. It's not
	       safe. But this is a f****ed situation, and
	       I'm just trying to recover -anything- */
	    /* This pb should be detected somewhere else (gpsirq, gpsctrl) */
	    /* GPS_reset(); */
	  }
	}
	
	//	SendGenericMsgStr( GL.Pproc->mbx_out, 0, " T2 in future:%d > %d",
	//			   oldsec, GL.st->GpsCurrentSec ) ;
	PrintLog( LOG_ERROR, " T2 in future: %d > %d - Discard\n",
		  oldsec, GL.st->GpsCurrentSec ) ;
	doSend = 0 ;
      }
      else if ( oldsec < (GL.st->GpsCurrentSec -1) ) {
	/* Too old second:
	    discard data
	    send generic message
	*/
	//	SendGenericMsgStr( GL.Pproc->mbx_out, 0, " T2 too old: %d << %d",
	//			   oldsec, GL.st->GpsCurrentSec ) ;
	PrintLog( LOG_ERROR, " T2 too old: %d << %d - Discard\n",
		  oldsec, GL.st->GpsCurrentSec ) ;
	err=PostDealloc( GL.Pproc->mbx_out, pmes ) ;
	doSend = 0 ;/* Dont send any message */
      }
      IsAlloc= 0;
      if( doSend ) {
	err=PostSendShrink(  GL.Pproc->mbx_out, pmes, size_t2_yes) ;
	size_t2_yes=0;
	if ( err != E_SUCCESS) {
	  PrintLog( LOG_ERROR, "SendShrink Err %d\n", err);
	}
      }
      else {
	size_t2_yes= 0;
	return 0 ;
      }
    }
  } //end of if( IsAlloc && cursec != oldsec) {
  if( !IsAlloc) {                      /* Ask for a new mailbox */
    while(( err= PostAlloc( GL.Pproc->mbx_out, (void **)&pmes, 
    			    GL.Pproc->Bsize_out)) != E_SUCCESS) {
      if( err == POST_ERR_FULL) {
    	struct timespec t;
    	t.tv_nsec=100000000;
    	t.tv_sec=0;
    	nanosleep(&t,NULL);
      }
    }
    IsAlloc= 1;                        /* New buffer : store second */
    oldsec= cursec;
    mesbuf= pmes->buf;
    pmes->mheader.type= M_T2_YES;
    pmes->mheader.version = TRIGGER2_VERSION;
    *(int *) mesbuf= htonl(oldsec);
    mesbuf += sizeof( oldsec);
    /* header + second         */
    size_t2_yes= sizeof( MESSAGE_HEADER)+ sizeof( oldsec);
  }

  /* Limit T2 rate */
  if ( size_t2_yes >
       (sizeof( MESSAGE_HEADER) + 4 + (GL.cf->AcqParams.MaxT2Rate*3)) ) {
    return 0 ;
  }
    
  GL.st->trg2.T2++;
  *mesbuf++= (Emicro >> 16) & 0xFF;     /* Store Eval & Micro : 24 bits */
  *mesbuf++= (Emicro >>  8) & 0xFF;
  *mesbuf++=  Emicro        & 0xFF;
  size_t2_yes += 3;
  return 1;
}




void byebye( error_code err)
{
  FB_finish(gl.fastbuffer);                /* Unlink Fast buffer */

  statuslib_finish();

  EvbFinish(&gl.evbid);                    /* Unlink Event Buffer */
  PostClose( &GL.Pproc->mbx_out);          /* Close Mailbox */
//  /* Unlink + delete OS9-event */
//  while( eventUnlink( st->event.IdT1Trigger2));
//  eventDelete( st->event.EvNameT1Tg2);
//  if( Signal) {
//    PrintLog( LOG_ERROR, "Ended by signal %d\n", Signal);
//  }
//  exit( err );
}

void init( void)
{
  error_code err;
  void *dummy_dat;
//  u_int32 perm= 0x777;
  signal_code sig;

  int spmt_debug_aux;
  
  /* Intercept Signals and mask them */

  //FeReset(); - this might be dangerous for others processes which
  //      use also the FPGA. Moving this reset to control process.

  setSignalHandler( sig_hand, dummy_dat); /* RS: consider all possible signals
					     it maybe not a good idea ...*/

  trace_init(); /*it is to set the initial trigger_bin parameter for
		  peak, area and base calculation. It is needed before
		  call trace_raw2evt */


  GL.st = statuslib_Initialize();
  if (GL.st == NULL) {
    PrintLog(LOG_ERROR, "Can't initialize status module.\n");
    exit(1);
  }
  GL.cf = configlib_Initialize();
  if (GL.cf == NULL) {
    PrintLog( LOG_ERROR, "Can't create CONFIG!\n");
    exit( err ) ;
  }

  if(FeInit(GL.cf->sys.UIO)!=0){
    PrintLog( LOG_ERROR, "Could not start properly the Fe to set trigger\n");
    exit(1);
  }
  PrintLog(LOG_INFO,"==>%s<==   \n",((GL.cf->sys.UIO==1)?"Using UIO":"Using /dev/mem"));
  
  gl.T2Algorithm = GL.cf->AcqParams.Trigger2Algorithm;
  
  PrintLog( LOG_INFO, "Algo %d\n", gl.T2Algorithm);
  
  if(( GL.Pproc= myprocess( MYNAME)) == NULL) {
    PrintLog( LOG_ERROR, "Myprocess Err\n" );
    exit( 1 );
  }

  /* Init Fast Buffers */
  if((gl.fastbuffer = FB_init_cl(FESHWRREAD_SHWR_NAME))==NULL) {
    PrintLog( LOG_ERROR, "Init FB Err \n");
    exit( 1 );
  }


  /* Event Buffer initialisation */
  while(( err= EvbInit(&gl.evbid, EVT_BUFFER_NAME, EVBSIZE)) ==  EVB_ERR_UNKNOWN);
  if( err) {
    PrintLog( LOG_ERROR, "EvbInit Err %d\n", err);
    byebye( err);
  } 
  /* Calibration "alaxav" initialisation (used in xbtrig.c)*/
  while(( err= EvbInit( &GL.calibxid, CALIBX_BUFFER_NAME, 
			CALIBX_SIZE) ) == EVB_ERR_UNKNOWN);
  if( err) {
    PrintLog( LOG_ERROR, "EvbInit Err %d\n", err);
    byebye( err);
  }
  if(( err= ReadyInit( &ReadyEvId, ReadyEvName))) {
    PrintLog( LOG_ERROR, "ReadyInit Err %d\n", err);
    byebye( err);
  }
  if(( err= ReadyToGo( ReadyEvId, READY_TRIGGER2))) {
    PrintLog( LOG_ERROR, "ReadyToGo Err %d\n", err);
    byebye( err);
  } 
  
  /* Wait for msgsvrout (MailBox) */
  if(( err= ReadyAll( ReadyEvId, &sig, READY_MSGSVROUT)) != E_SUCCESS) {
    if(( err= ReadyAll( ReadyEvId, &sig, 0)) != E_SUCCESS) {
      PrintLog( LOG_ERROR,
		"ReadyAll Err %d %d\n", 
		sig, err);
      byebye( err);
    }
  }

  /* Open Mailbox in send mode */
  if((err = PostLink( &(GL.Pproc->mbx_out), GL.Pproc->pnameout ))) {
    PrintLog( LOG_ERROR, "PostLink Err  %d\n", err ) ;
    byebye( err);
  }

  spmt_open(&(gl.spmt.shm),
	    4*sizeof(struct spmt_evt),
	    sizeof(struct spmt_evt)); /* it is enouth for 4 events */

  memset(&(gl.spmt.calib),0,sizeof(gl.spmt.calib));


  
  err= ReadyFinish( ReadyEvId, ReadyEvName);

  /*disable all shower trigger */
  FeShwrEnableTriggerMask(FE_SHWR_TRIG_MASK_SB |
                          FE_SHWR_TRIG_MASK_TOT |
                          FE_SHWR_TRIG_MASK_TOTD |
                          FE_SHWR_TRIG_MASK_MOPS,
                          0);

  PrintLog( LOG_INFO, "Ready\n" );




  
  spmt_debug_aux=shm_open("spmt_debug",O_RDWR | O_CREAT , S_IRWXU);
  if(spmt_debug_aux<0){
    printf("open shared memory for spmt_debug problem\n");
    exit(0);
  }
  if(ftruncate(spmt_debug_aux,sizeof(struct spmt_debug_str))<0){
    printf("spmt_debug ftruncate problem \n");
    exit(0);
  }
  spmt_debug=(struct spmt_debug_str *)mmap(NULL,
                                           sizeof(struct spmt_debug_str),
                                           PROT_READ|PROT_WRITE,
                                           MAP_SHARED,
                                           spmt_debug_aux, 0);
  if((void *)spmt_debug == MAP_FAILED){
    printf("spmt_debug mapp failed ...\n");
    exit(0);
  }
  close(spmt_debug_aux);
  trigger2_evt_serv_imp_init(); //this is for someone which want to get the trace.
  //  gl.debug_trigg=debug_with_trig_init();

}

void DoWork(struct shwr_evt_complete *evt_c)
{
//  int32 value, min_val=1, max_val=0x1000;
//  signal_code signal;
  error_code err;
  struct shwr_evt_complete *evt;
  struct trace_parameters evt_param;

  static int FirstEventHandled = 1 ;

  unsigned char tag[16] ;

  int i;

  evt=NULL;
  memset( tag, 0, 16 ) ;
  if ( GL.st->RunStatus != RUN_STARTED ){
    //    /* Release Fast buffer */
    FB_free(gl.fastbuffer);
    return ;
  }
  if((((evt_c->raw.buffer_ev_status)>>9) & 0x3)==3){
    //the three SHWR FPGA buffers where full.
    GL.st->trg2.nb_warning_full_buffer++;
  } 
  trigger2_evt_serv_imp_wr(evt_c);
  if(evt_c->raw.Evt_type_1 & FE_SHWR_TRIG_MASK_LED ){
    FB_free(gl.fastbuffer);    /* Release Fast buffer */
    return;
  } 
  if(evt_c->raw.Evt_type_1 & ( FE_SHWR_TRIG_MASK_SB| FE_SHWR_TRIG_MASK_SB_FULL)){
    GL.st->trg2.nT1++ ;
  }
  if ((evt_c->raw.Evt_type_1 | evt_c->raw.Evt_type_2 ) &
	   (FE_SHWR_TRIG_MASK_TOT | 
	    FE_SHWR_TRIG_MASK_TOTD|
	    FE_SHWR_TRIG_MASK_MOPS)) {
    GL.st->trg2.nTOT++;
  }
  if ( CheckOrder( &(evt_c->raw) ) != SUCCESS ) {
    PrintLog( LOG_WARNING, "Order problem ...\n");
    debug_show();
    FB_free(gl.fastbuffer);    /* Release Fast buffer */
    return ;
  }
  //
  //  if ( (evt_raw->Evt_type_2 & BAD_EDGE_EVENT ) != 0 ) {
  //    /* Was a bad edge event, discard it */
  //    PrintLog( LOG_WARNING, "Got a Bad Edge event at %d, discard\n",
  //	      evt_raw->shwr_gps_info.second ) ;
  //    FB_free(gl.fastbuffer) ;
  //    return ;
  //  }
  //
  /* Event Buffer Allocation */
  while( err = EvbAlloc(gl.evbid, (void **)&evt, sizeof(struct shwr_evt_complete))) {
    if( err == EVB_ERR_FULL) {
      while( err = EvbFree(gl.evbid, NB_EVB_RELEASE)) {
	if ( err == EVB_ERR_UNKNOWN )
	  /* Interrupted by signal. Ignore and try again */
	  continue ;
	else {
	  PrintLog( LOG_ERROR, "EvbFree Fatal Error %d\n", err ) ;
	  byebye( err ) ;
	}
      }
      /*rsato - the following two lines looks very terrible!!!  since
	it would be managed by the Evb library. The two lines are
	sharing the data with all the others processes - in principle
	they are not really need. Still need to be verified */
      //      st->trg2.NEvtBusy -= NB_EVB_RELEASE;
      //      st->buffer.EvbUsed -= NB_EVB_RELEASE;
    }
    else if ( err == EVB_ERR_UNKNOWN )
      /* Interrupted by signal. Ignore and try again */
      continue ;
    else if ( err == EVB_ERR_UNKNOWN || err == EVB_ERR_NOTYOURS ) {
      PrintLog( LOG_ERROR, "EvbAlloc Fatal Error %d\n", err ) ;
      byebye( err ) ;
    }
  }

  /*make the copy from the primary(FastBuffer) to secundary buffer(EvtBuffer) */
  /* Inform gpsirq that it should now read the GRB scaler */
  if ( FirstEventHandled && GL.cf->FeParams.V2Settings.GrbEnabled != 0 ) {
    GL.st->StartGrb = 1 ;
  }
  memcpy(evt,evt_c,sizeof(struct shwr_evt_complete));
  trace_raw2param(evt,&evt_param);

  if ( FirstEventHandled && gl.T2Algorithm == XAVIER_T2_ALGO ) {
    InitFirstTime( &(evt->raw), &evt_param);
    FirstEventHandled = 0 ;
  }
  FB_free(gl.fastbuffer);           /* Release Fast buffer */
  /*rs start */
  //  /* Apply offset */
  //  applyOffset(evt, &st->TimingTable);
  evt->raw.micro_off=0;
  /*rs end */
  /* Check nano order */
  //if ( CheckOrder( evt ) != SUCCESS ) {
  //  printf("Order problem --------- \n");
  //  debug_show();
  //  printf("Order problem !!!\n");
  //  fflush(stdout);
  //  //exit(0);
  //  return ;
  //}

  trigger2( &(evt->raw),&evt_param);            /* Is it a T2 event ? */
  /* Mark Event buffer as ready */
  if( err= EvbReady( gl.evbid, (void *)evt, sizeof(struct shwr_evt_complete),
		     tag)) {
    PrintLog( LOG_ERROR, "EvbReady Err %d\n", err);
    return ;
  }
  /*rsato - begin 
    The following lines looks very terrible!!!  since
    it would be managed by the Evb library. The two lines are
    sharing the data with all the others processes - in principle
    they are not really need, since all the informations
    are already managed internally in EvtBuffer library.
    Still need to be verified!!!
  */
  
  //  /* Event buffer occupancy */
  //  if ( st->buffer.FastMax < st->buffer.FastUsed )
  //    st->buffer.FastMax = st->buffer.FastUsed ;
  //  if ( st->buffer.FastMin > st->buffer.FastUsed )
  //    st->buffer.FastMin = st->buffer.FastUsed ;
  //  st->buffer.FastUsed--;
  //  st->buffer.EvbUsed++;
  //  st->trg2.NEvtBusy++;
  //  if( (st->trg2.T1Fast0 + st->trg2.T1Fast1) > 1000) {
  //    n= st->trg2.NEvtBusy;
  //    if( n > st->buffer.EvbMax) st->buffer.EvbMax= n;
  //    if( n < st->buffer.EvbMin) st->buffer.EvbMin= n;
  //  } 
  //  if( st->buffer.EvbMin > st->buffer.EvbUsed) 
  //    st->buffer.EvbMin= st->buffer.EvbUsed;
  //  if( st->buffer.EvbMax < st->buffer.EvbUsed) 
  //    st->buffer.EvbMax= st->buffer.EvbUsed;
  //

  /*small PMT ...*/
  //if(Peak ...);

  for(i=0;i<SHWR_RAW_NCH_MAX;i++){
    GL.st->trg2.trace_val[i] = evt_c->raw.fadc_raw[i][evt_c->raw.trace_start];
  }
  GL.st->trg2.trace_val[5] = evt_c->extra.radio_info.fadc_raw[0];
  

  spmt_send(&(gl.spmt),evt_c,&evt_param);
}
 

main( int argc, char *argv)
{
  int nbsig ;
  //  int32 value, min_val=1, max_val=0x1000;
  //  error_code err;
  //  signal_code dummy ;
  struct shwr_evt_complete *evt;
  //struct shwr_evt *ev; 
  int Signal,Ok;
  debug_tr2_init();
//
  LogProgName = "Trigger2" ;
  //  PrintLog( LOG_INFO, "Version %d, Compiled %s %s\n",
//	    TRIGGER2_VERSION, __DATE__, __TIME__ ) ;
//  if ( IsFake( "TRIGGER2_DEBUG" ) ) Debug = 1 ;
//  if ( IsFake( "TRIGGER2_DEBUG_HIGH" ) ) Debug = 2 ;
  init();
  //  
  //  InitTimeLog( &st->GpsCurrentSec ) ;
  //
  printf("Trigger2: %d %d\n",getpid(),SIG_RUN_START_REQ);
  Ok=1;
  PrintLog( LOG_INFO, "Going to start main loop\n");;
  while( Ok ) {    /***** Start infinite Loop *****/
    while( Signal = PopSig( &nbsig ) ) {
      /*rs start - start 
	implementing a start the acquisition ... 
	it was implemented in diferent way in the OS9000 */
      if(Signal==SIGQUIT){
	byebye(0);
      }
      if(Signal==SIG_RUN_START_REQ){ //defined at sigauger.h
	uint32_t th[3]={1000,1000,1000};
	FeShwrSetThreshold(FE_SHWR_TRIG_SB,th);
	FeShwrEnablePMT_tr_set(FE_SHWR_TRIG_SB,7,1);
	FeShwrNCoincidence(FE_SHWR_TRIG_SB,1);
	FeShwrEnableTrigger(FE_SHWR_TRIG_SB,1);
      } else if(Signal==SIGTSTP){
	printf("TRIGGER2: Receiver Signal :%d. Stopping Everything\n",Signal);
	Ok=0;
      }
      /*it is just sending enabling the trigger ... to the FPGA */
    
      /*rs stop */     

//      if( Signal == SIG_STOP) {            /* Stop Run */
//	PrintLog( LOG_INFO, "Run Stopped\n" ) ;
//	if( (err= EvbReset(evbid))) {      /* Reset Event Buffer */
//	  PrintLog( LOG_ERROR,
//		    "EvbReset Err %d\n", err);
//	}
//      }
//      else if ( Signal == SIG_PAUSE) {           /* Pause Run */
//	PrintLog( LOG_INFO, "Run Paused\n" ) ;
//	trigger2( NULL);                   /* Send remaining T2s to CS */
//      }
//      else if ( Signal == SIG_CLF_END ) {
//	enableClf = 0 ;
//	finishClf() ;
//      }
//      else if ( Signal == SIG_CLF_DISABLE ) {
//	enableClf = 0 ;
//      }
//      else if ( Signal == SIG_CLF_ENABLE ) {
//	enableClf = 1 ;
//	/* Read from the clf.cfg file (if it exists) the clf_time and
//	   the clf_delta */
//	getClfCfg() ;
//      }
//      else if ( Signal == SIG_ENABLE_HISTO ||
//		Signal == SIG_MUON_RUN ||
//		Signal == SIG_SHAPE_RUN
//		) {
//#define MUFILL_RUN_SIG 77
//	kill(st->mufill.PidMuFill, MUFILL_RUN_SIG);
//      }
//      else if ( Signal == SIG_SET_DEBUG ) Debug++ ;
//      else if ( Signal == SIG_UNSET_DEBUG ) Debug = 0 ;
//      else if ( Signal == SIG_AFFECTS_DAC1 ) { affectsdac = 30 ; modifdac=1; }
//      else if ( Signal == SIG_AFFECTS_DAC2 ) { affectsdac = 30 ; modifdac=2; }
//      else if ( Signal == SIG_AFFECTS_DAC3 ) { affectsdac = 30 ; modifdac=3; }
//      else {
//	PrintLog( LOG_ERROR, "Got Sig %d\n", Signal ) ;
//	byebye( Signal ) ;
//      }
    }
    if((evt=FB_wait_get_ev(gl.fastbuffer))!=NULL){
      debug_tr2_update(evt);
      DoWork(evt);
    }
    //      /* Check bad edges detection and if so send message to CDAS */
//      if ( GL.st->inter.BadEdges != PrevEdge ) {
//	/* Send message */
//	PrevEdge = GL.st->inter.BadEdges ;
//	if ( Debug ) {
//	  if ( PrevEdge != TTAG_GOOD_EDGES ) {
//	    SendGenericMsgStr( GL.Pproc->mbx_out, CONTROL_VERSION,
//			       "BadEdge (%x) at %d",
//			       GL.st->inter.BadEdges,
//			       GL.st->inter.BadEdgesTime ) ;
//	    PrintTimedLog( LOG_WARNING,
//			   " BadEdge %d/%d (T2: %d, T1: %d)\n",
//			   GL.st->inter.BadEdges,
//			   GL.st->inter.BadEdgesCount,
//			   GL.st->trg2.T2,
//			   GL.st->inter.T1Fast0 ) ;
//	  } else {
//	    SendGenericMsgStr( GL.Pproc->mbx_out, CONTROL_VERSION,
//			       "GoodEdge (%x) at %d",
//			       GL.st->inter.BadEdges,
//			       GL.st->inter.BadEdgesTime ) ;
//	    PrintTimedLog( LOG_WARNING,
//			   " GoodEdge (%x) (T2: %d, T1: %d)\n",
//			   GL.st->inter.BadEdges,
//			   GL.st->trg2.T2,
//			   GL.st->inter.T1Fast0 ) ;
//	  }
//	}
//      }
//    }
//    if( GL.st->RunStatus == REBOOT) byebye( Signal ) ;
  }     /* End infinite loop */
  byebye( 4000);
}





//x ------------- it looks to be just for debug ----------------
//x
//xstatic int BadAnodeDynode = 0 ;
//x
//xvoid CheckAnodeDynode( unsigned int *pdata )
//x{
//x  int i = 0 ;
//x  unsigned int *padc = pdata ;
//x
//x  for( ; i<10 ; i++ ) {
//x    if ( (*padc >> 30 ) != DYNODE_TAG || (*(padc + ANODE_OFFSET) >> 30 ) != ANODE_TAG ) {
//x      PrintTimedLog( LOG_WARNING, "Bad A/D at begin (%d)\n",
//x		     st->trg2.T1Fast0 ) ;
//x      if ( !BadAnodeDynode )
//x	SendGenericMsgStr( GL.Pproc->mbx_out, CONTROL_VERSION,
//x			   "Bad A/D begin %d",
//x			   st->trg2.T1Fast0 ) ;
//x      BadAnodeDynode = 1 ;
//x      return ;
//x    }
//x    padc += INCREMENT ;
//x  }
//x  for( i = 760, padc = pdata + INCREMENT*760 ; i<7 ; i++ ) 
//x    {
//x    if ( (*padc >> 30 ) != DYNODE_TAG || (*(padc+ANODE_OFFSET) >> 30 ) != ANODE_TAG ) {
//x      PrintTimedLog( LOG_WARNING, "Bad A/D end (%d)\n",
//x		     st->trg2.T1Fast0 ) ;
//x      if ( !BadAnodeDynode )
//x	SendGenericMsgStr( GL.Pproc->mbx_out, CONTROL_VERSION,
//x			   "Bad A/D end %d",
//x			   st->trg2.T1Fast0 ) ;
//x      BadAnodeDynode = 1 ;
//x      return ;
//x    }
//x    padc += INCREMENT ;
//x  }
//x  if ( BadAnodeDynode ) {
//x    PrintTimedLog( LOG_INFO, "Good A/D\n" ) ;
//x    SendGenericMsgStr( GL.Pproc->mbx_out, CONTROL_VERSION,
//x		       "Good A/D evt %d",
//x		       st->trg2.T1Fast0 ) ;
//x    BadAnodeDynode = 0 ;
//x  }
//x  return ;
//x}
