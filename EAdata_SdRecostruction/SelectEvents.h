

#ifndef _SelectEvents_SelectEvents_h_
#define _SelectEvents_SelectEvents_h_

#include <fwk/VModule.h>
#include <sevt/Station.h>

namespace evt {
  class Event;
}

namespace SelectEvents {

  class SelectEvents : public fwk::VModule {

  public:
    fwk::VModule::ResultFlag Init();
    fwk::VModule::ResultFlag Run(evt::Event& event);
    fwk::VModule::ResultFlag Finish();

  private:
    int fSSDPreProd;

    REGISTER_MODULE("SelectEvents", SelectEvents);
  };
}

#endif
