#ifndef _SdCalibratorOG_CalibrationParameters_h_
#define _SdCalibratorOG_CalibrationParameters_h_


namespace SdCalibratorOG {

  /**
    \class CalibrationParameters

    \author Bradley Manning
    \date Feb 2019
    \version $Id$

    These values were previously hardcoded values in SdCalibrator, designed 
    for the original unified board (UB). With the AugerUpgrade, there is an
    upgraded unified board (UUB) which has different frequencies/time scales
    etc. For SdCalibrator to cope with Upgrade, the hard coded numbers need 
    to be changed.
  */

  namespace CalibrationParameters {

    inline constexpr int GetSaturatedBinsMaximum(const bool isUub)
    { return isUub ? 900 : 300; }

    inline constexpr int GetMinLength(const bool isUub)
    { return isUub ? 120: 40; }

    inline constexpr unsigned int GetUsefulBins(const bool isUub)
    { return isUub ? 600 : 200; }

    inline constexpr int GetLargeFADCThreshold(const bool isUub)
    { return isUub ? 1200 : 300; }

    inline constexpr int GetMinFADCValue(const bool isUub)
    { return isUub ? 16 : 4; }

    inline constexpr int GetFindSignalThresholdMultiplier(const bool isUub)
    { return isUub ? 4 : 1; }

    inline constexpr int GetBinsWithLargeSignalThreshold(const bool isUub)
    { return isUub ? 1500 : 500; }

    inline constexpr int GetBinsWithSignalThreshold(const bool isUub)
    { return isUub ? 6 : 2; }

    inline constexpr int GetSignalMaxDist(const bool isUub)
    { return isUub ? 60 : 20; }

  };

}


#endif
