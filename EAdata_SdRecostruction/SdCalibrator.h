#ifndef _SdCalibratorOG_SdCalibrator_h_
#define _SdCalibratorOG_SdCalibrator_h_

#include <fwk/VModule.h>
#include <sevt/Station.h>


namespace sevt {
  class PMTRecData;
}

namespace SdCalibratorOG {

  struct StartStopParameters {
    int fSlot;
    int fWindow;
    double fThreshold;
  };


  struct TraceCleaningParameters {
    double fChargeThreshold;
    utl::ShadowPtr<StartStopParameters> fStart;
    utl::ShadowPtr<StartStopParameters> fStop;
    int fStopMargin;
  };


  /**
     \class SdCalibrator SdCalibrator.h "SdCalibratorOG/SdCalibrator.h"

     \brief Performs calibration and signal analysis of the SD event.

     Module does the following:
     <ul>
     <li> Removes stations without trigger data.
     <li> Removes stations with trigger errors.
     <li> Appropriately tags stations with Station::IsCandidate().
          Stations with StationTriggerData::GetError() == 2 and nonzero
	  StationTriggerData::GetWindow() are tagged as Station::IsSilent()
     <li> Calculates VEM peak and VEM charge values for each PMT using LS
          online calibration and histograms.
     <li> Rechecks the time-over-threshold (TOT) trigger tag for LS calibration
          versions \<= 12.
     <li> Applies nanosecond time correction to the station event time.
     <li> Applies calculated peak and charge values and generates appropriate
          PMT VEM traces.
     <li> Finds signal start, sets peak amplitude, total VEM signal and
          trace. Signal start is defined as timing of the first bin where
          all 3 PMTs have signal over the <b>\<signalTreshold\></b>.
     <li> For each PMT, calculates signal rise/fall time, peak amplitude,
          charge (=area), and area/peak.for PMTs. Fills rise/fall time and
          shape parameter (all with RMS) for stations.
	  <br>
	  You can change rise/fall time definitions in the xml file under the
	  <b>\<riseTimeFractions\></b> and <b>\<fallTimeFractions\></b>.
	  Currently (Pierre Billoir's) defaults of 0.1, 0.5 and 0.5, 0.9 are
	  used.
     </ul>
     <br>
     Differences to CDAS v3r2:
     <ul>
     <li> Total signal of the station is calculated as a sum of <b>separate</b>
          PMT VEM traces (in the integration window) and not as integral of
          a sum averaged over PMTs. Nevertheless, peak amplitude is found as
          maximum of a PMT VEM signal (same as CDAS).
     </ul>

     \author Stefano Argiro
     \author Darko Veberic
     \author Xavier Bertou

     \date 08 Oct 2003
     \date 30 Sep 2004 update, CDAS v3r2 sync
     \date 20 Oct 2004 update, CDAS v3r4 sync
     \date 05 Apr 2005 update, CDAS v4r0 sync, not much changed
     \date 02 Oct 2007 update based on Xavier's code in CDAS v4r6

     \ingroup SDRecModules
  */
  
  class SdCalibrator : public fwk::VModule {
    
  public:
    fwk::VModule::ResultFlag Init();
    fwk::VModule::ResultFlag Run(evt::Event& event);
    fwk::VModule::ResultFlag Finish();

    std::string GetSVNId() const 
    { return std::string("$Id: SdCalibrator.h 32817 2019-06-25 17:12:03Z darko $"); }
      
  private:
    /// Calculate corrected nanosecond from raw GPS parameter
    void ApplyTimeCorrection(sevt::Station& station);
    void CalculatePeakAndCharge(sevt::Station& station);

    typedef std::map<int, int> CalibrationVersionMap;
    typedef utl::SMatrix<6, 6, int> TriggerMigrationMatrix;
    void ResetStationTrigger(sevt::Station& station,
                             CalibrationVersionMap& calibrationVersions,
                             TriggerMigrationMatrix& triggerMigration) const;

    //bool ApplyStartTimeCorrection(sevt::Station& station);
    void SumPMTComponents(sevt::Station& station) const;

    // new offline calibration
    void MakeFlatBaseline(sevt::PMT& pmt, const sevt::PMTConstants::PMTGain gain) const;
    bool ComputeBaselines(sevt::Station& station) const;
    bool ComputeBaseline(const sevt::Station& station, sevt::PMT& pmt, const sevt::PMTConstants::PMTGain gain) const;
    bool MakeComponentVEMTraces(sevt::PMT& pmt) const;
    int  BuildSignals(sevt::Station& station) const;
    bool BuildSignals(sevt::PMT& pmt, const unsigned int traceLength,
                      const unsigned int saturationValue) const;
    bool MergeSignals(sevt::Station& station) const;
    bool SelectSignal(sevt::Station& station) const;
    void TreatSaturated(sevt::Station& station) const;

    void ComputeShapeRiseFallPeak(sevt::PMTRecData& pmtRecData,
                                  const double binTiming,
                                  const unsigned int startBin,
                                  const unsigned int startIntegration,
                                  const unsigned int endIntegration,
                                  const double traceIntegral) const;

    // XML config
    unsigned int fLatchBin = 0;
    unsigned int fStartThreshold = 0;
    unsigned int fIntegrationWindow = 0;
    unsigned int fSlidingWindow = 0;
    unsigned int fStartBinLowerBound = 0;
    double fSignalThreshold = 0;
    double fSignalCut = 0;
    double fPMTSummationCutoff = 0;
    std::pair<double, double> fValidDynodeAnodeRatioRange{0, 0};
    std::pair<double, double> fPeakFitRange{0, 0};
    double fPeakFitChi2Accept = 0;
    double fPeakVEMConversionFactor = 0;
    double fChargeFitShoulderHeadRatio = 0;
    double fChargeFitChi2Accept = 0;
    double fChargeVEMConversionFactor = 0;
    double fOnlineChargeVEMFactor = 0;
    double fChargeMIPConversionFactor = 0;
    std::pair<double, double> fShapeFitRangeBefore12{0, 0};
    std::pair<double, double> fShapeFitRangeSince12{0, 0};
    std::pair<double, double> fChargeSlopeFitRange{0, 0};
    std::pair<double, double> fEAChargeFitRange{0, 0};
    double fTOTVEMThreshold = 0;
    unsigned int fTOTBinsAboveThreshold = 0;
    unsigned int fTOTWindow = 0;
    std::pair<double, double> fRiseTimeFractions{0, 0};
    std::pair<double, double> fFallTimeFractions{0, 0};
    bool fForceLSTriggerRecalculation = false;
    bool fKeepLSTOTTrigger = false;
    std::set<int> fRecalculateLSTriggerForTestStations;
    utl::ShadowPtr<TraceCleaningParameters> fTraceCleaning;
    double fFindSignalThreshold = 0;
    int fFindSignalBinsAboveThreshold = 0;
    bool fTreatHGLGEqualInSignalSearch = false;
    bool fApplyBackwardFlatPieceCheck = false;
    bool fDecreaseLGFlatPieceTolerance = false;
    bool fAlwaysCalculateSignalStartTimeFromHighGain = false;
    bool fIncludeWaterCherenkovDetectorInScintillatorStartStopDetermination = false;

    // derived quantities
    unsigned int fIncreaseThresholdRegion = 0;
    sevt::StationConstants::SignalComponent fFADCSignalComponent;

    bool fToldYaPeak = false;
    bool fToldYaCharge = false;
    bool fToldYaShape = false;

    bool fIsUub = false;

    const utl::TimeRange kCommsCrisis{utl::TimeStamp{928378815}, utl::TimeStamp{942375615}};

    REGISTER_MODULE("SdCalibratorOG", SdCalibrator);
  
  };

}


#endif
