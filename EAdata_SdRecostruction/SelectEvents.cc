#include "SelectEvents.h"

#include <fwk/CentralConfig.h>
#include <det/VManager.h>
#include <utl/Branch.h>

#include <evt/Event.h>
#include <sevt/SEvent.h>
#include <sevt/Station.h>
#include <sdet/PMTConstants.h>
#include <sdet/SDetector.h>

#include <utl/ErrorLogger.h>

using namespace std;

using namespace fwk;
using namespace det;
using namespace utl;
using namespace evt;
using namespace sevt;

namespace SelectEvents {

  VModule::ResultFlag
  SelectEvents::Init()
  {
    const Branch topB = CentralConfig::GetInstance()->GetTopBranch("SelectEvents");
    Branch SSDSelection = topB.GetChild("SSDSelection");
    SSDSelection.GetChild("SSDPreProd").GetData(fSSDPreProd);
    return eSuccess;
  }

  VModule::ResultFlag
  SelectEvents::Run(Event& event)
  {
    if (!event.HasSEvent())
      return eContinueLoop;

    SEvent& sEvent = event.GetSEvent();
    int nStations = 0;
    
    for (SEvent::StationIterator sIt = sEvent.StationsBegin(), end = sEvent.StationsEnd(); sIt != end; ++sIt) {

      const sdet::Station& dStation = det::Detector::GetInstance().GetSDetector().GetStation(*sIt);

      const bool isUUB = dStation.IsUUB();
      const bool hasScintillator = dStation.HasScintillator();

      // Count depending on criteria (SSDPreProd)
      if (fSSDPreProd) { //Count UB SSD
        if (hasScintillator && !isUUB) 
          ++nStations;  
      } else { //Count UUB
      if (isUUB)
        ++nStations;
      }
    }
    
    if (!nStations) {
      if (fSSDPreProd)
        INFO("Could not find any stations with scintillator  in this event, skipping...");
      else
        INFO("Could not find any UUB stations in this event, skipping...");
      return eContinueLoop;
    }
    if (fSSDPreProd)
      INFO("Found at least 1 station with UB scintillator in this event!");
    else
      INFO("Found at least 1 station with UUB in this event!");
    return eSuccess;
  }

  VModule::ResultFlag
  SelectEvents::Finish()
  {
    return eSuccess;
  }

}
  
