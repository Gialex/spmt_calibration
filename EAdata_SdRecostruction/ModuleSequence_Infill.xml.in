<!-- A sequence for an SD only reconstruction -->

<!-- For homogeneity in the StandardApplications, standard sequences for
simulation, MC reconstruction, and data reconstruction have been added to
Framework/CentralConfig/standard**Sequences.dtd. Please refer to these
files if you have questions regarding &NAME (i.e. &SdReconstruction) -->

<!DOCTYPE sequenceFile [
  <!ENTITY % sd SYSTEM "@CONFIGDIR@/standardSdSequences.dtd">
  %sd;
] >

<sequenceFile
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation='@SCHEMALOCATION@/ModuleSequence.xsd'>

  <enableTiming/>

  <moduleControl>

    <loop numTimes="unbounded" pushEventToStack="yes">

      <module> EventFileReaderOG </module>
      <module> EventCheckerOG </module>
      <module> SelectEvents </module>
      <module> SdEACalibrationFillerKG </module> 

      <module> SdPMTQualityCheckerKG </module>
      <module> TriggerTimeCorrection </module>
      <module> SdCalibratorOG </module>

      <module> SdStationPositionCorrection </module>
      <module> SdBadStationRejectorKG </module>
      <module> SdSignalRecoveryKLT </module>
      <module> SdEventSelectorOG </module>

      <module> SdPlaneFitOG </module>
      <module> LDFFinderKG </module>

      <module> EnergyCalculationPG </module>
      <module> Risetime1000LLL </module>
      <module> DLECorrectionGG </module>
      <module> SdEventPosteriorSelectorOG </module>

     <!-- as of ICRC 2017, weather corrections must be applied externally with
          Documentation/StandardApplications/ADST/WeatherCorrections
          after reconstruction. You apply these corrections on ADSTs. -->

      <module> RecDataWriterNG </module>

    </loop>

  </moduleControl>

</sequenceFile>
