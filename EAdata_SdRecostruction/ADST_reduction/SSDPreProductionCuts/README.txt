Both the SD-1500 and SD-750 configuration (*.cfg) and cuts (*.cuts) files in
addition to the associated bad period and event id rejection files were taken
from the ICRC2019 directory under /trunk/ADST/Analysis/cuts/. Cuts on time
intervals were removed and two additional cuts were added, namely

!hottestStationIsUUB
hottestStationHasScintillator

which select only events where the hottest station (that is, the station with
the most signal) had a unified board (UB) and an SSD. All else remains the
same.