#include "RecEvent.h"
#include "RecEventFile.h"
#include "DetectorGeometry.h"
#include "FileInfo.h"
#include "GenStation.h"

#include <utl/Branch.h>
#include <utl/Reader.h>
#include <utl/GeometryUtilities.h>
#include <utl/AugerUnits.h>

#include <TSystem.h>
#include <TFile.h>
#include <TTree.h>
#include <TVector3.h>

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <map>

using namespace std;

int GetOptions(int argc, char** argv);

void Usage(const char* const exe);

void MakeROOT(const char* title);

string gOutputFile = "reduced.root";
int gVerbosity = 1;

int
main(int argc, char **argv)
{
  TTree::SetMaxTreeSize(200000000000LL);

  const int nOptions = GetOptions(argc, argv);
  if (nOptions < 0 || argc - nOptions < 1) {
    Usage(argv[0]);
    return 1;
  }

  const vector<string> dataFileNames(argv + nOptions, argv + argc);
  RecEventFile inputFile(dataFileNames);

  MakeROOT(gOutputFile.c_str());

  TFile *rootOut = new TFile(gOutputFile.c_str(), "UPDATE");
  TTree* OutputTree = (TTree*)rootOut->Get("reducedData");
  
  UInt_t sdID;
  OutputTree->SetBranchAddress("sdID", &sdID);
  Double_t LogE;
  OutputTree->SetBranchAddress("LogE", &LogE);
  UInt_t hottestStatID;
  OutputTree->SetBranchAddress("hottestStatID", &hottestStatID);
  Int_t nStations;
  OutputTree->SetBranchAddress("nStations", &nStations);
  vector<UInt_t>* statID=NULL;
  OutputTree->SetBranchAddress("statID", &statID);
  vector<Bool_t>* IsUUB=NULL;
  OutputTree->SetBranchAddress("IsUUB",&IsUUB);
  vector<UInt_t>* Status=NULL;
  OutputTree->SetBranchAddress("Status",&Status);
  vector<UInt_t>* RejectionStatus=NULL;
  OutputTree->SetBranchAddress("RejectionStatus",&RejectionStatus);
  vector<Double_t>* Stot=NULL;
  OutputTree->SetBranchAddress("Stot",&Stot);
  vector<Double_t>* SPDistance=NULL;
  OutputTree->SetBranchAddress("SPDistance",&SPDistance);
  vector<Bool_t>* LowGainSaturated=NULL;
  OutputTree->SetBranchAddress("LowGainSaturated",&LowGainSaturated); 
  vector<UInt_t>* LowGainSaturatedBins_pmt1=NULL;
  OutputTree->SetBranchAddress("LowGainSaturatedBins_pmt1",&LowGainSaturatedBins_pmt1);
  vector<UInt_t>* LowGainSaturatedBins_pmt2=NULL;
  OutputTree->SetBranchAddress("LowGainSaturatedBins_pmt2",&LowGainSaturatedBins_pmt2);
  vector<UInt_t>* LowGainSaturatedBins_pmt3=NULL;
  OutputTree->SetBranchAddress("LowGainSaturatedBins_pmt3",&LowGainSaturatedBins_pmt3);
  vector<vector<Double_t> >* LowGainSaturatedBinsPos_pmt1=NULL;
  OutputTree->SetBranchAddress("LowGainSaturatedBinsPos_pmt1",&LowGainSaturatedBinsPos_pmt1);
  vector<vector<Double_t> >* LowGainSaturatedBinsPos_pmt2=NULL;
  OutputTree->SetBranchAddress("LowGainSaturatedBinsPos_pmt2",&LowGainSaturatedBinsPos_pmt2);
  vector<vector<Double_t> >* LowGainSaturatedBinsPos_pmt3=NULL;
  OutputTree->SetBranchAddress("LowGainSaturatedBinsPos_pmt3",&LowGainSaturatedBinsPos_pmt3);

  
  RecEvent * theRecEvent = new RecEvent ();
  inputFile.SetBuffers(&theRecEvent);

  int counter = 0;

  const int nEvents = inputFile.GetNEvents();
  const int fivePercent = int(0.05*nEvents);

  while (inputFile.ReadNextEvent()==RecEventFile::eSuccess ) {

    const int currentEventNumber = inputFile.GetCurrentEvent();
    if (fivePercent && currentEventNumber && !(currentEventNumber % fivePercent))
      cout << inputFile.GetCurrentEventString() << endl;
    
    counter++;
    nStations = 0;
    statID->clear();
    Stot->clear();
    IsUUB->clear();
    Status->clear();
    RejectionStatus->clear();
    SPDistance->clear();
    LowGainSaturated->clear();
    LowGainSaturatedBins_pmt1->clear();
    LowGainSaturatedBins_pmt2->clear();
    LowGainSaturatedBins_pmt3->clear();
    LowGainSaturatedBinsPos_pmt1->clear();
    LowGainSaturatedBinsPos_pmt2->clear();
    LowGainSaturatedBinsPos_pmt3->clear();
    
    SDEvent sdevent = theRecEvent->GetSDEvent();
    sdID = sdevent.GetEventId();
    //cout << "Event ID " << sdID << endl;
    const SdRecShower& sdShower = sdevent.GetSdRecShower();
    LogE = log10(sdShower.GetEnergy());
    hottestStatID = sdevent.GetT5HottestStation();
            
    for (vector<SdRecStation>::const_iterator sIt = sdevent.GetStationVector().begin(), end = sdevent.GetStationVector().end();
	 sIt != end; ++sIt) {

      nStations++;
      statID->push_back(sIt->GetId());
      IsUUB->push_back(sIt->IsUUB());
      Stot->push_back(sIt->GetTotalSignal());
      SPDistance->push_back(sIt->GetSPDistance());
      Status->push_back(sIt->GetStatus());
      RejectionStatus->push_back(sIt->GetRejectionStatus());
      LowGainSaturated->push_back(sIt->IsLowGainSaturated());
    
      UShort_t startbin = sIt->GetSignalStartSlot();
      UShort_t endbin = sIt->GetSignalEndSlot();
      UInt_t satBins1 = 0;
      UInt_t satBins2 = 0;
      UInt_t satBins3 = 0;
      vector<Double_t> satBins_pmt1; satBins_pmt1.clear();
      vector<Double_t> satBins_pmt2; satBins_pmt2.clear();
      vector<Double_t> satBins_pmt3; satBins_pmt3.clear();
      
      for(unsigned int i = startbin; i <= endbin; i++){
	if(sIt->GetPMTTraces(eTotalTrace,1).GetLowGainComponent().size() > 0 &&
	   sIt->GetPMTTraces(eTotalTrace,1).GetLowGainComponent()[i] >= (sIt->IsUUB() ? 4095 : 1023) ){
	  satBins1++;
	  satBins_pmt1.push_back(i);
	}
	if(sIt->GetPMTTraces(eTotalTrace,2).GetLowGainComponent().size() > 0 &&
	   sIt->GetPMTTraces(eTotalTrace,2).GetLowGainComponent()[i] >= (sIt->IsUUB() ? 4095 : 1023) ){
	  satBins2++;
	  satBins_pmt2.push_back(i);
	}
	if(sIt->GetPMTTraces(eTotalTrace,3).GetLowGainComponent().size() > 0 &&
	   sIt->GetPMTTraces(eTotalTrace,3).GetLowGainComponent()[i] >= (sIt->IsUUB() ? 4095 : 1023) ){
	  satBins3++;
	  satBins_pmt3.push_back(i);
	}
      }

      LowGainSaturatedBins_pmt1->push_back(satBins1);
      LowGainSaturatedBins_pmt2->push_back(satBins2);
      LowGainSaturatedBins_pmt3->push_back(satBins3);
      LowGainSaturatedBinsPos_pmt1->push_back(satBins_pmt1);
      LowGainSaturatedBinsPos_pmt2->push_back(satBins_pmt2);
      LowGainSaturatedBinsPos_pmt3->push_back(satBins_pmt3);
      
    } // close cycle over stations
    
    OutputTree->Fill();
    
  } // close cycle over events

  rootOut->cd();
  OutputTree->Write();
  rootOut->Close();

  return 0;
  
} // close main

void
MakeROOT(const char* title){

  TFile* fout = new TFile(title,"RECREATE");
  TTree *OutputTree = new TTree("reducedData","reducedData");

  UInt_t sdID;
  OutputTree->Branch("sdID", &sdID);
  Double_t LogE;
  OutputTree->Branch("LogE", &LogE);
  UInt_t hottestStatID;
  OutputTree->Branch("hottestStatID", &hottestStatID);
  Int_t nStations;
  OutputTree->Branch("nStations", &nStations);
  vector<UInt_t> statID; statID.clear();
  OutputTree->Branch("statID", &statID);
  vector<Bool_t> IsUUB; IsUUB.clear();
  OutputTree->Branch("IsUUB",&IsUUB);
  vector<UInt_t> Status; Status.clear();
  OutputTree->Branch("Status",&Status);
  vector<UInt_t> RejectionStatus; RejectionStatus.clear();
  OutputTree->Branch("RejectionStatus",&RejectionStatus);
  vector<Double_t> Stot; Stot.clear();
  OutputTree->Branch("Stot",&Stot);
  vector<Double_t> SPDistance; SPDistance.clear();
  OutputTree->Branch("SPDistance",&SPDistance);
  vector<Bool_t> LowGainSaturated; LowGainSaturated.clear();
  OutputTree->Branch("LowGainSaturated",&LowGainSaturated);
  vector<UInt_t> LowGainSaturatedBins_pmt1; LowGainSaturatedBins_pmt1.clear();
  OutputTree->Branch("LowGainSaturatedBins_pmt1",&LowGainSaturatedBins_pmt1);
  vector<UInt_t> LowGainSaturatedBins_pmt2; LowGainSaturatedBins_pmt2.clear();
  OutputTree->Branch("LowGainSaturatedBins_pmt2",&LowGainSaturatedBins_pmt2);
  vector<UInt_t> LowGainSaturatedBins_pmt3; LowGainSaturatedBins_pmt3.clear();
  OutputTree->Branch("LowGainSaturatedBins_pmt3",&LowGainSaturatedBins_pmt3);
  vector<vector<Double_t> > LowGainSaturatedBinsPos_pmt1; LowGainSaturatedBinsPos_pmt1.clear();
  OutputTree->Branch("LowGainSaturatedBinsPos_pmt1",&LowGainSaturatedBinsPos_pmt1);
  vector<vector<Double_t> > LowGainSaturatedBinsPos_pmt2; LowGainSaturatedBinsPos_pmt2.clear();
  OutputTree->Branch("LowGainSaturatedBinsPos_pmt2",&LowGainSaturatedBinsPos_pmt2);
  vector<vector<Double_t> > LowGainSaturatedBinsPos_pmt3; LowGainSaturatedBinsPos_pmt3.clear();
  OutputTree->Branch("LowGainSaturatedBinsPos_pmt3",&LowGainSaturatedBinsPos_pmt3);

  OutputTree->Write();
  fout->Close();

  return;
}

void
Usage(const char* const exe)
{
  cout << " usage: " << exe << " [<options>] <fileNames>\n"
          "\n"
    " Options: -o: name of the output file          (default: " << gOutputFile << ")\n"
    "          -v: verbosity                        (default: 1)\n"
       << endl;
}

int
GetOptions(int argc, char** argv)
{
  int c;
  while ((c = getopt(argc, argv, "c:o:n:m:e:v:h")) != -1) {
    switch (c) {
    case 'o':
      gOutputFile = string(optarg);
      cout << " use output file: " << gOutputFile << endl;
      break;
    case 'v':
      gVerbosity = atoi(optarg);
      cout << " verbosity: " << gVerbosity << endl;
      break;
    case 'h':
      return -2;
    default:
      return -2;
    }
  }
  cout << endl;
  return optind;
}

