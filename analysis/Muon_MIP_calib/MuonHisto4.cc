#include <TROOT.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TLatex.h>
#include <TLatex.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TPaveStats.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TH1.h>
#include <TF1.h>
#include <TGraphErrors.h>
#include <THistPainter.h>
#include <TMath.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <TRandom3.h>

using namespace std;


void MuonHisto4(const char* title, const int pmtID){


    double chargeVEMConversionFactor = 1.01; // la fitted charge NON è moltiplicata
                                             // per questo fattore, per cui non
                                             // moltiplico neanche il mio risultato

  if(gSystem->AccessPathName(title)){
    cout << "Root file " << title << " not found." << "\n";
    exit(1);
  }

  TFile* f1 = TFile::Open(title);

  if( !f1->GetListOfKeys()->Contains("EA_data") ){
    cout << "TTrees not found inside " << title << "\n";
    exit(1);
  }

  int stat, pmt;
  double base, offset, VEMcharge_low, VEMcharge_high, rawcharge, fittedcharge;
  unsigned int muonhisto[600];
  unsigned int histo[600];

  TTree* t = (TTree*)f1->Get("EA_data");
  unsigned int nEntries = t->GetEntries();
  
  t->SetBranchAddress("stationID", &stat);
  t->SetBranchAddress("pmtID", &pmt);
  t->SetBranchAddress("Baseline", &base);
  t->SetBranchAddress("Offset", &offset);
  t->SetBranchAddress("VEMcharge_low", &VEMcharge_low);
  t->SetBranchAddress("VEMcharge_high", &VEMcharge_high);
  t->SetBranchAddress("RawCharge", &rawcharge);
  t->SetBranchAddress("FittedCharge", &fittedcharge);
  t->SetBranchAddress("MuonHisto", &muonhisto);

  TH1D* hVEM = new TH1D("hVEM","hVEM",101,500,2500);
  TH1D* hVEM_raw = new TH1D("hVEM_raw","hVEM_raw",101,500,2500);
  TH1D* hVEM_fitted = new TH1D("hVEM_fitted","hVEM_fitted",101,500,2500);

  TH1D* hDifference1 = new TH1D("hDifference1","hDifference1",101,-19,19);
  TH1D* hDifference2 = new TH1D("hDifference2","hDifference2",101,-19,19);
  TH1D* hDifference3 = new TH1D("hDifference3","hDifference3",101,-19,19);

  int fivepercent = ceil(float(nEntries)/20);

for(int n=0; n<nEntries-1; n++){

  if(n != 0 && n % fivepercent == 0) cout << endl << 100 * float(n)/nEntries << " %" << endl ;

  t->GetEntry(n);
  if(pmt != pmtID) continue;
  
  for(int i=0;i<600;i++) histo[i]=muonhisto[i];
  float meanfittedcharge = fittedcharge;
  n++;
  t->GetEntry(n);
  while(pmt != pmtID){
    n++;
    if(n==nEntries) break;
    t->GetEntry(n);
  }
  if(n==nEntries) break;
  for(int i=0;i<600;i++) histo[i]+=muonhisto[i];
  meanfittedcharge += fittedcharge;
  n++;
  t->GetEntry(n);
  while(pmt != pmtID){
    n++;
    if(n==nEntries) break;
    t->GetEntry(n);
  }
  if(n==nEntries) break;
  for(int i=0;i<600;i++) histo[i]+=muonhisto[i];
  meanfittedcharge += fittedcharge;
    n++;
  t->GetEntry(n);
  while(pmt != pmtID){
    n++;
    if(n==nEntries) break;
    t->GetEntry(n);
  }
  if(n==nEntries) break;
  for(int i=0;i<600;i++) histo[i]+=muonhisto[i];
  meanfittedcharge += fittedcharge;

  meanfittedcharge /= 4.;
  hVEM_fitted->Fill(meanfittedcharge);

  int v,max,s,ss,bmax,j;
  int amin = 130;
  int amax = 400;
  int ann = 30;

  v=0;
  max=v;
  bmax=amin;
  for(j=amin+1;j<amax-ann;j++){
    v += histo[j+ann]-histo[j-1];
    if(max<v){
      max=v;
      bmax=j;
    }
  }
  int chargeBin_raw = bmax+ann/2;
  int charge_raw = chargeBin_raw << 3;
  //cout << "Maximum at bin " << chargeBin << " (= " << (chargeBin*8 +4) << ")" << endl;
  hVEM_raw->Fill(charge_raw);

  // find "head-and-shoulder": from the upper side of the histogram,
  // search for local maximum (head), surrounded by drops (shoulders)
  // with shoulder/head value ratio of less than
  // fChargeWindowShoulderHeadRatio

  double fChargeFitShoulderHeadRatio = 0.75;
  const int size = 400;
  int start = size - 1;
  for ( ; start >= 2 && float(histo[start])/8 < 40; --start)
    ;
  int maxBin = start;
  double maxValue = float(histo[start])/8;
  int shoulderLow = 0;
  {
    double value = float(histo[start-1])/8;
    double value1 = float(histo[start-2])/8;
    for (int pos = start - 1; pos >= 2; --pos) {
      if (maxValue < value) {
        maxValue = value;
        maxBin = pos;
      }
      const double reducedMax = maxValue * fChargeFitShoulderHeadRatio;
      // require 3 consecutive values to be lower than reducedMax
      // to qualify as a shoulder
      const double value2 = float(histo[pos-2])/8;
      if (value <= reducedMax && value1 <= reducedMax && value2 <= reducedMax) {
        shoulderLow = pos;
        break;
      }
      value = value1;
      value1 = value2;
    }
  }
  if (shoulderLow) {
    // find upper shoulder
    const double reducedMax = maxValue * fChargeFitShoulderHeadRatio;
    const int size2 = size - 2;
    int shoulderHigh = 0;
    {
      double value = float(histo[maxBin+1])/8;
      double value1 = float(histo[maxBin+2])/8;
      for (int pos = maxBin + 1; pos < size2; ++pos) {
        const double value2 = float(histo[pos+2])/8;
        if (value <= reducedMax && value1 <= reducedMax && value2 <= reducedMax) {
          shoulderHigh = pos;
          break;
        }
        value = value1;
        value1 = value2;
      }
    }
    if (shoulderHigh) {
      double chargeLow  = shoulderLow*8 +4;
      double chargeHigh = shoulderHigh*8 +4;

      //cout << "Maximum between bin " << shoulderLow << " (= " << shoulderLow*8 +4 << ") and " << shoulderHigh << " (= " << shoulderHigh*8 +4 << ")" << endl;

      amin = shoulderLow;
      amax = shoulderHigh;
      ann = (amax-amin)/2 + (amax-amin)/3;
      v=0;
      max=v;
      bmax=amin;
      for(j=amin+1;j<amax-ann;j++){
        v += histo[j+ann]-histo[j-1];
        if(max<v){
          max=v;
          bmax=j;
        }
      }
      int chargeBin = bmax+ann/2;
      int charge_better = chargeBin*8 +4;
      //cout << "Maximum at bin " << chargeBin << " (= " << (chargeBin*8 +4) << ")" << endl;

      hVEM->Fill(charge_better);

      hDifference1->Fill(100*float(meanfittedcharge-charge_better)/meanfittedcharge);
      hDifference2->Fill(100*float(meanfittedcharge-charge_raw)/meanfittedcharge);
      hDifference3->Fill(100*float(charge_better-charge_raw)/charge_better);

    }// sholderHigh
  }//shoulderLow
}//nEntries


TPaveStats *st1 = NULL; TPaveStats *st2 = NULL; TPaveStats *st3 = NULL;

TCanvas* c0 = new TCanvas("c0","c0",800,600);
hVEM->GetXaxis()->SetTitle("charge estimation diff. [%]");
hVEM->Draw("histo");
hVEM->SetLineWidth(2); hVEM->SetLineColor(kBlack);
hVEM_raw->Draw("histosames");
hVEM_raw->SetLineWidth(1); hVEM_raw->SetLineColor(kRed+1);
hVEM_fitted->Draw("histosames");
hVEM_fitted->SetLineWidth(1); hVEM_fitted->SetLineColor(kGreen+1);
gPad->Update();
st1 = (TPaveStats*)hVEM->FindObject("stats");
        st1->SetX1NDC(0.725); st1->SetX2NDC(0.9);
        st1->SetY1NDC(0.925); st1->SetY2NDC(0.75);
st2 = (TPaveStats*)hVEM_raw->FindObject("stats");
        st2->SetX1NDC(0.725); st2->SetX2NDC(0.9);
        st2->SetY1NDC(0.725); st2->SetY2NDC(0.55);
st3 = (TPaveStats*)hVEM_fitted->FindObject("stats");
        st3->SetX1NDC(0.725); st3->SetX2NDC(0.9);
        st3->SetY1NDC(0.525); st3->SetY2NDC(0.35);
gPad->Update();
//c0->Close();
 c0->Print("c0_quad.pdf");

TCanvas* c1 = new TCanvas("c1","c1",800,600);
 c1->SetLogy();
hDifference1->GetXaxis()->SetTitle("charge estimation diff. [%]");
hDifference1->Draw("histo");
hDifference1->SetLineWidth(2); hDifference1->SetLineColor(kBlack);
hDifference2->Draw("histosames");
hDifference2->SetLineWidth(1); hDifference2->SetLineColor(kGreen+1);
//hDifference3->Draw("histosames");
//hDifference3->SetLineWidth(1); hDifference3->SetLineColor(kRed+1);
gPad->Update();
st1 = (TPaveStats*)hDifference1->FindObject("stats");
        st1->SetX1NDC(0.725); st1->SetX2NDC(0.9);
        st1->SetY1NDC(0.925); st1->SetY2NDC(0.75);
st2 = (TPaveStats*)hDifference2->FindObject("stats");
        st2->SetX1NDC(0.725); st2->SetX2NDC(0.9);
        st2->SetY1NDC(0.725); st2->SetY2NDC(0.55);
//st3 = (TPaveStats*)hDifference3->FindObject("stats");
//        st3->SetX1NDC(0.725); st3->SetX2NDC(0.9);
//        st3->SetY1NDC(0.525); st3->SetY2NDC(0.35);
gPad->Update();
//c1->Close();
 c1->Print("c1_quad.pdf");

}
