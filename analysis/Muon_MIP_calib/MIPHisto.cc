#include <TROOT.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TLatex.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TPaveStats.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TH1.h>
#include <TF1.h>
#include <TGraphErrors.h>
#include <THistPainter.h>
#include <TMath.h>
#include <TRandom3.h>

#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>

using namespace std;


void MIPHisto(const char* tank){

  const int pmtID = 5;

  std::stringstream title; title << "MIPdata" << tank << ".root";

  if(gSystem->AccessPathName(title.str().c_str())){
    cout << "Root file " << title.str().c_str() << " not found." << "\n";
    exit(1);
  }

  TFile* f1 = TFile::Open(title.str().c_str());

  if( !f1->GetListOfKeys()->Contains("EA_data") ){
    cout << "TTrees not found inside " << title.str().c_str() << "\n";
    exit(1);
  }

  int stat, pmt;
  double base, offset, VEMcharge_low, VEMcharge_high, rawcharge, fittedcharge;
  unsigned int histo[600];

  TTree* t = (TTree*)f1->Get("EA_data");
  unsigned int nEntries = t->GetEntries();

  t->SetBranchAddress("stationID", &stat);
  t->SetBranchAddress("pmtID", &pmt);
  t->SetBranchAddress("Baseline", &base);
  t->SetBranchAddress("Offset", &offset);
  t->SetBranchAddress("VEMcharge_low", &VEMcharge_low);
  t->SetBranchAddress("VEMcharge_high", &VEMcharge_high);
  t->SetBranchAddress("RawCharge", &rawcharge);
  t->SetBranchAddress("FittedCharge", &fittedcharge);
  t->SetBranchAddress("MIPHisto", &histo);

  TH1D* hVEM_fitted = new TH1D("hVEM_fitted","hVEM_fitted",101,0,1000);

  TH1D* HDIFF[5];
  TH1D* HDIFF_RAW[5];
  //TH1D* hDifference = new TH1D();
  //TH1D* hDifference_raw = new TH1D();

  int ANN[5] = {5,6,8,10,12};

  for(int i=0;i<5;i++){
    std::stringstream title1; title1 << "hDifference" << ANN[i] ;
    HDIFF[i] = new TH1D(title1.str().c_str(),title1.str().c_str(),101,-99,99);
    std::stringstream title2; title2 << "hDifferenceRaw" << ANN[i] ;
    HDIFF_RAW[i] = new TH1D(title2.str().c_str(),title2.str().c_str(),101,-99,99);
  }

  int fivepercent = ceil(float(nEntries)/20);

for(int n=0; n<nEntries; n++){

  if(n != 0 && n % fivepercent == 0) cout << endl << 100 * float(n)/nEntries << " %" << endl ;
  t->GetEntry(n);
  if(pmt != pmtID) continue;
  //if(fittedcharge<0) continue;

  hVEM_fitted->Fill(fittedcharge);

  int v,max,bmax,j, amin, amax, ann;

  for(int i=0;i<5;i++){
    amin = 10;
    amax = 400;
    v=0;
    max=v;
    bmax=amin;
    ann = ANN[i];

    for(j=amin+1;j<amax-ann;j++){
      v += histo[j+ann]-histo[j-1];
      if(max<v){
        max=v;
        bmax=j;
      }
    }
    int chargeBin_raw = bmax+ann/2;
    int charge_raw = 4 + chargeBin_raw << 3;
    //cout << "Maximum at bin " << chargeBin << " (= " << (chargeBin*8 +4) << ")" << endl;
    HDIFF_RAW[i]->Fill(100*float(fittedcharge-charge_raw)/fittedcharge);
  }

  // find "head-and-shoulder": from the upper side of the histogram,
  // search for local maximum (head), surrounded by drops (shoulders)
  // with shoulder/head value ratio of less than
  // fChargeWindowShoulderHeadRatio

  double fChargeFitShoulderHeadRatio = 0.75;
  const int size = 400;
  int start = size - 1;
  for ( ; start >= 2 && float(histo[start])/8 < 40; --start)
    ;
  int maxBin = start;
  double maxValue = float(histo[start])/8;
  int shoulderLow = 0;
  {
    double value = float(histo[start-1])/8;
    double value1 = float(histo[start-2])/8;
    for (int pos = start - 1; pos >= 2; --pos) {
      if (maxValue < value) {
        maxValue = value;
        maxBin = pos;
      }
      const double reducedMax = maxValue * fChargeFitShoulderHeadRatio;
      // require 3 consecutive values to be lower than reducedMax
      // to qualify as a shoulder
      const double value2 = float(histo[pos-2])/8;
      if (value <= reducedMax && value1 <= reducedMax && value2 <= reducedMax) {
        shoulderLow = pos;
        break;
      }
      value = value1;
      value1 = value2;
    }
  }
  if (shoulderLow) {
    // find upper shoulder
    const double reducedMax = maxValue * fChargeFitShoulderHeadRatio;
    const int size2 = size - 2;
    int shoulderHigh = 0;
    {
      double value = float(histo[maxBin+1])/8;
      double value1 = float(histo[maxBin+2])/8;
      for (int pos = maxBin + 1; pos < size2; ++pos) {
        const double value2 = float(histo[pos+2])/8;
        if (value <= reducedMax && value1 <= reducedMax && value2 <= reducedMax) {
          shoulderHigh = pos;
          break;
        }
        value = value1;
        value1 = value2;
      }
    }
    if (shoulderHigh) {
      double chargeLow  = shoulderLow*8 +4;
      double chargeHigh = shoulderHigh*8 +4;

      //cout << "Maximum between bin " << shoulderLow << " (= " << shoulderLow*8 +4 << ") and " << shoulderHigh << " (= " << shoulderHigh*8 +4 << ")" << endl;

      for(int i=0;i<5;i++){
        amin = shoulderLow;
        amax = shoulderHigh;
        v=0;
        max=v;
        bmax=amin;
        ann = std::min(ANN[i],amax-amin);

        for(j=amin+1;j<amax-ann;j++){
          v += histo[j+ann]-histo[j-1];
          if(max<v){
            max=v;
            bmax=j;
          }
        }
        int chargeBin = bmax+ann/2;
        int charge_better = chargeBin*8 +4;
        //cout << "Maximum at bin " << chargeBin << " (= " << (chargeBin*8 +4) << ")" << endl;
        HDIFF[i]->Fill(100*float(fittedcharge-charge_better)/fittedcharge);
      }

    }// sholderHigh
  }//shoulderLow
}//nEntries


TPaveStats *st1 = NULL; TPaveStats *st2 = NULL; TPaveStats *st3 = NULL;
/*
TCanvas* c0 = new TCanvas("c0","c0",800,600);
hVEM->GetXaxis()->SetTitle("VEM charge [HG FADC counts]");
hVEM->Draw("histo");
hVEM->SetLineWidth(2); hVEM->SetLineColor(kBlack);
hVEM_raw->Draw("histosames");
hVEM_raw->SetLineWidth(1); hVEM_raw->SetLineColor(kRed+1);
hVEM_fitted->Draw("histosames");
hVEM_fitted->SetLineWidth(1); hVEM_fitted->SetLineColor(kGreen+1);
gPad->Update();
st1 = (TPaveStats*)hVEM->FindObject("stats");
        st1->SetX1NDC(0.725); st1->SetX2NDC(0.9);
        st1->SetY1NDC(0.925); st1->SetY2NDC(0.75);
st2 = (TPaveStats*)hVEM_raw->FindObject("stats");
        st2->SetX1NDC(0.725); st2->SetX2NDC(0.9);
        st2->SetY1NDC(0.725); st2->SetY2NDC(0.55);
st3 = (TPaveStats*)hVEM_fitted->FindObject("stats");
        st3->SetX1NDC(0.725); st3->SetX2NDC(0.9);
        st3->SetY1NDC(0.525); st3->SetY2NDC(0.35);
gPad->Update();
//c0->Close();
 c0->Print("c0.pdf");
 */

TCanvas* cDiff = new TCanvas("cDiff","cDiff",1700,1000);
cDiff->Divide(3,2);
for(int i=0;i<5;i++){
  cDiff->cd(i+2);
  cDiff->cd(i+2)->SetLogy();
  std::stringstream temp; temp << "Average over " << ANN[i] << "bins - PMT" << pmtID;
  HDIFF[i]->SetTitle(temp.str().c_str());
  HDIFF[i]->GetXaxis()->SetTitle("MIP charge diff. [%]");
  HDIFF[i]->Draw("hist");
  HDIFF[i]->SetLineWidth(2); HDIFF[i]->SetLineColor(kBlack);
  HDIFF_RAW[i]->Draw("histsames");
  HDIFF_RAW[i]->SetLineWidth(1); HDIFF_RAW[i]->SetLineColor(kGreen+1);
  gPad->Update();
  st1 = (TPaveStats*)HDIFF[i]->FindObject("stats");
        st1->SetX1NDC(0.725); st1->SetX2NDC(0.9);
        st1->SetY1NDC(0.925); st1->SetY2NDC(0.75);
  st2 = (TPaveStats*)HDIFF_RAW[i]->FindObject("stats");
        st2->SetX1NDC(0.725); st2->SetX2NDC(0.9);
        st2->SetY1NDC(0.725); st2->SetY2NDC(0.55);
  gPad->Update();
}
cDiff->cd(1);
hVEM_fitted->GetXaxis()->SetTitle("MIP charge [HG FADC counts]");
hVEM_fitted->Draw("hist");
hVEM_fitted->SetLineWidth(2); hVEM_fitted->SetLineColor(kBlack);
gPad->Update();

std::stringstream temp; temp << "MIP_" << tank << "_PMT" << pmtID << ".pdf";
cDiff->Print(temp.str().c_str());


}
