#include <TROOT.h>
#include <TSystem.h>
#include <TStyle.h>
#include <TLatex.h>
#include <TLatex.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TPaveStats.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TH1.h>
#include <TF1.h>
#include <TGraphErrors.h>
#include <THistPainter.h>
#include <TMath.h>
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>

using namespace std;


void SmallShowersSignals(const char* title, const char* treeTitle,
                         const int dayStart, const int dayStop){

  if(gSystem->AccessPathName(title)){
    cout << "Root file " << title << " not found." << "\n";
    exit(1);
  }

  TFile* f1 = TFile::Open(title);

  if( !f1->GetListOfKeys()->Contains(treeTitle) ){
    cout << "TTrees not found inside " << title << "\n";
    exit(1);
  }

  TTree* t1 = (TTree*)f1->Get(treeTitle);
  unsigned int nEntries1 = t1->GetEntries();

  Int_t GPS = 0;
  t1->SetBranchAddress("GPS_sec", &GPS);

  Int_t p1, ch1, p2, ch2, p3, ch3, p4, ch4;
  Int_t p1_hg, ch1_hg, p2_hg, ch2_hg, p3_hg, ch3_hg;
  Bool_t m1, m2, m3, satBins1, satBins2, satBins3, satBins4;
  Bool_t satBins1_hg, satBins2_hg, satBins3_hg;
  t1->SetBranchAddress("MaskedStatus_LPMT1", &m1);
  t1->SetBranchAddress("Peak_FADC_counts_LG_LPMT1",&p1);
  t1->SetBranchAddress("Charge_FADC_counts_LG_LPMT1",&ch1);
  t1->SetBranchAddress("SaturationStatus_LG_LPMT1",&satBins1);
  t1->SetBranchAddress("Peak_FADC_counts_HG_LPMT1",&p1_hg);
  t1->SetBranchAddress("Charge_FADC_counts_HG_LPMT1",&ch1_hg);
  t1->SetBranchAddress("SaturationStatus_HG_LPMT1",&satBins1_hg);

  t1->SetBranchAddress("MaskedStatus_LPMT2", &m2);
  t1->SetBranchAddress("Peak_FADC_counts_LG_LPMT2",&p2);
  t1->SetBranchAddress("Charge_FADC_counts_LG_LPMT2",&ch2);
  t1->SetBranchAddress("SaturationStatus_LG_LPMT2",&satBins2);
  t1->SetBranchAddress("Peak_FADC_counts_HG_LPMT2",&p2_hg);
  t1->SetBranchAddress("Charge_FADC_counts_HG_LPMT2",&ch2_hg);
  t1->SetBranchAddress("SaturationStatus_HG_LPMT2",&satBins2_hg);

  t1->SetBranchAddress("MaskedStatus_LPMT3", &m3);
  t1->SetBranchAddress("Peak_FADC_counts_LG_LPMT3",&p3);
  t1->SetBranchAddress("Charge_FADC_counts_LG_LPMT3",&ch3);
  t1->SetBranchAddress("SaturationStatus_LG_LPMT3",&satBins3);
  t1->SetBranchAddress("Peak_FADC_counts_HG_LPMT3",&p3_hg);
  t1->SetBranchAddress("Charge_FADC_counts_HG_LPMT3",&ch3_hg);
  t1->SetBranchAddress("SaturationStatus_HG_LPMT3",&satBins3_hg);

  t1->SetBranchAddress("Peak_FADC_counts_sPMT",&p4);
  t1->SetBranchAddress("Charge_FADC_counts_sPMT",&ch4);
  t1->SetBranchAddress("SaturationStatus_sPMT",&satBins4);

  Double_t vem1, vem2, vem3, ad1, ad2, ad3;
  t1->SetBranchAddress("VEM_charge_HG_LPMT1", &vem1);
  t1->SetBranchAddress("AoD_LPMT1", &ad1);
  t1->SetBranchAddress("VEM_charge_HG_LPMT2", &vem2);
  t1->SetBranchAddress("AoD_LPMT2", &ad2);
  t1->SetBranchAddress("VEM_charge_HG_LPMT3", &vem3);
  t1->SetBranchAddress("AoD_LPMT3", &ad3);

////////////////////

  TH1D* hVEM1 = new TH1D("hVEM1","hVEM1",100,1000,3000);
  TH1D* hVEM2 = new TH1D("hVEM2","hVEM2",100,1000,3000);
  TH1D* hVEM3 = new TH1D("hVEM3","hVEM3",100,1000,3000);

  TH1D* hAoD1 = new TH1D("hAoD1","hAoD1",100,0,100);
  TH1D* hAoD2 = new TH1D("hAoD2","hAoD2",100,0,100);
  TH1D* hAoD3 = new TH1D("hAoD3","hAoD3",100,0,100);

int Nbins_logS = 100;
  TH1D* hlogS1 = new TH1D("hlogS1","hlogS1",Nbins_logS,0,5);
  TH1D* hlogS2 = new TH1D("hlogS2","hlogS2",Nbins_logS,0,5);
  TH1D* hlogS3 = new TH1D("hlogS3","hlogS3",Nbins_logS,0,5);

  TH1D* hlogS1_sat = new TH1D("hlogS1_sat","hlogS1_sat",Nbins_logS,0,5);
  TH1D* hlogS2_sat = new TH1D("hlogS2_sat","hlogS2_sat",Nbins_logS,0,5);
  TH1D* hlogS3_sat = new TH1D("hlogS3_sat","hlogS3_sat",Nbins_logS,0,5);

  TH1D* hlogS1_notsat = new TH1D("hlogS1_notsat","hlogS1_notsat",Nbins_logS,0,5);
  TH1D* hlogS2_notsat = new TH1D("hlogS2_notsat","hlogS2_notsat",Nbins_logS,0,5);
  TH1D* hlogS3_notsat = new TH1D("hlogS3_notsat","hlogS3_notsat",Nbins_logS,0,5);

int Nbins_S = 100;
  TH1D* hS1 = new TH1D("hS1","hS1",Nbins_S,0,1000);
  TH1D* hS2 = new TH1D("hS2","hS2",Nbins_S,0,1000);
  TH1D* hS3 = new TH1D("hS3","hS3",Nbins_S,0,1000);

  TH1D* hS1_sat = new TH1D("hS1_sat","hS1_sat",Nbins_S,0,1000);
  TH1D* hS2_sat = new TH1D("hS2_sat","hS2_sat",Nbins_S,0,1000);
  TH1D* hS3_sat = new TH1D("hS3_sat","hS3_sat",Nbins_S,0,1000);

  TH1D* hS1_notsat = new TH1D("hS1_notsat","hS1_notsat",Nbins_S,0,1000);
  TH1D* hS2_notsat = new TH1D("hS2_notsat","hS2_notsat",Nbins_S,0,1000);
  TH1D* hS3_notsat = new TH1D("hS3_notsat","hS3_notsat",Nbins_S,0,1000);

////////////////////

  int GPSsec_20171201 = 1196121618;
  int GPSsec_20180101 = 1198800018;
  int GPSsec_20180601 = 1211846418;
  int GPSsec_20380101 = 1829952018;

  int GPSstart = GPSsec_20180101 + (3600*24)*dayStart;
  int GPSstop = std::min(GPSsec_20380101, GPSsec_20180101 + (3600*24)*dayStop);
  int N_hours = (GPSstop - GPSstart)/3600;
  int N_hours_interval = 6;

  TGraph* gVEM1 = new TGraph();
  TGraph* gVEM2 = new TGraph();
  TGraph* gVEM3 = new TGraph();
  TProfile* pVEM1 = new TProfile("pVEM1","pVEM1",N_hours/N_hours_interval,(GPSstart-GPSsec_20180101)/(3600*24),(GPSstop-GPSsec_20180101)/(3600*24),0,0);
  TProfile* pVEM2 = new TProfile("pVEM2","pVEM2",N_hours/N_hours_interval,(GPSstart-GPSsec_20180101)/(3600*24),(GPSstop-GPSsec_20180101)/(3600*24),0,0);
  TProfile* pVEM3 = new TProfile("pVEM3","pVEM3",N_hours/N_hours_interval,(GPSstart-GPSsec_20180101)/(3600*24),(GPSstop-GPSsec_20180101)/(3600*24),0,0);

  TGraph* gAoD1 = new TGraph();
  TGraph* gAoD2 = new TGraph();
  TGraph* gAoD3 = new TGraph();
  TProfile* pAoD1 = new TProfile("pAoD1","pAoD1",N_hours/N_hours_interval,(GPSstart-GPSsec_20180101)/(3600*24),(GPSstop-GPSsec_20180101)/(3600*24),0,0);
  TProfile* pAoD2 = new TProfile("pAoD2","pAoD2",N_hours/N_hours_interval,(GPSstart-GPSsec_20180101)/(3600*24),(GPSstop-GPSsec_20180101)/(3600*24),0,0);
  TProfile* pAoD3 = new TProfile("pAoD3","pAoD3",N_hours/N_hours_interval,(GPSstart-GPSsec_20180101)/(3600*24),(GPSstop-GPSsec_20180101)/(3600*24),0,0);

////////////////////

  int minAcceptableVEM = 1167;

  float minSignal_fit = 200;//VEM
  float maxSignal_fit = 500;//VEM

  float factorVEM = 1.01;

  int fivepercent = ceil(float(nEntries1)/20);

  int counter = 0;
  int counter1 = 0;
  int counter2 = 0;
  int counter3 = 0;
  int counter4 = 0;
  int counter1_bis = 0;
  int counter2_bis = 0;
  int counter3_bis = 0;

  float minVEM1 = 0;
  float minVEM2 = 0;
  float minVEM3 = 0;
  float maxVEM1 = 3000;
  float maxVEM2 = 3000;
  float maxVEM3 = 3000;

  for(unsigned int n = 0; n < nEntries1; n++){
    t1->GetEntry(n);
    if( GPS < GPSstart || GPS > GPSstop ) continue;

    if(m1){
      if(vem1>minAcceptableVEM) hVEM1->Fill(vem1);
      hAoD1->Fill(ad1);
    }
    if(m2){
      if(vem2>minAcceptableVEM) hVEM2->Fill(vem2);
      hAoD2->Fill(ad2);
    }
    if(m3){
      if(vem3>minAcceptableVEM) hVEM3->Fill(vem3);
      hAoD3->Fill(ad3);
    }
  }

   minVEM1 = std::max(minAcceptableVEM, hVEM1->GetMean() - hVEM1->GetRMS());
   maxVEM1 = hVEM1->GetMean() + hVEM1->GetRMS();
   minVEM2 = std::max(minAcceptableVEM, hVEM2->GetMean() - hVEM2->GetRMS());
   maxVEM2 = hVEM2->GetMean() + hVEM2->GetRMS();
   minVEM3 = std::max(minAcceptableVEM, hVEM3->GetMean() - hVEM3->GetRMS());
   maxVEM3 = hVEM3->GetMean() + hVEM3->GetRMS();

   float lastValid_ad1 = 32;
   float lastValid_ad2 = 32;
   float lastValid_ad3 = 32;
   float lastValid_VEM1 = (maxVEM1-minVEM1)/2;
   float lastValid_VEM2 = (maxVEM2-minVEM2)/2;
   float lastValid_VEM3 = (maxVEM3-minVEM3)/2;


  for(unsigned int n = 0; n < nEntries1; n++){

//    if(n != 0 && n % fivepercent == 0) cout << endl << 100 * float(n)/nEntries1 << " %" << endl ;

    t1->GetEntry(n);
    if( GPS < GPSstart || GPS > GPSstop ) continue;
    float time = (float)(GPS - GPSsec_20180101)/(60*60*24);

    pVEM1->Fill(time,vem1);
    gVEM1->SetPoint(n, time, vem1);
    if(p1_hg>1500 && p1>0 && ch1>0 && !satBins1_hg){
      //gAoD1->SetPoint(n, time, p1_hg/p1);
      //pAoD1->Fill(time, p1_hg/p1);
      gAoD1->SetPoint(n, time, float(ch1_hg)/ch1);
      pAoD1->Fill(time, float(ch1_hg)/ch1);
    }

    pVEM2->Fill(time,vem2);
    gVEM2->SetPoint(n, time, vem2);
    if(p2_hg>1500 && p2>0 && ch2>0 && !satBins2_hg){
      //gAoD2->SetPoint(n, time, p2_hg/p2);
      //pAoD2->Fill(time, p2_hg/p2);
      gAoD2->SetPoint(n, time, float(ch2_hg)/ch2);
      pAoD2->Fill(time, float(ch2_hg)/ch2);
    }

    pVEM3->Fill(time,vem3);
    gVEM3->SetPoint(n, time, vem3);
    if(p3_hg>1500 && p3>0 && ch3>0 && !satBins3_hg){
      //gAoD3->SetPoint(n, time, p3_hg/p3);
      //pAoD3->Fill(time, p3_hg/p3);
      gAoD3->SetPoint(n, time, float(ch3_hg)/ch3);
      pAoD3->Fill(time, float(ch3_hg)/ch3);
    }

    ad1 = lastValid_ad1;
    if(vem1<minVEM1 || vem1>maxVEM1) vem1=lastValid_VEM1;
    else lastValid_VEM1=vem1;

    ad2 = lastValid_ad2;
    if(vem2<minVEM2 || vem2>maxVEM2) vem2=lastValid_VEM2;
    else lastValid_VEM2=vem2;

    ad3 = lastValid_ad3;
    if(vem3<minVEM3 || vem3>maxVEM3) vem3=lastValid_VEM3;
    else lastValid_VEM3=vem3;

    double sign1 = factorVEM*ch1*ad1/vem1;
    double sign2 = factorVEM*ch2*ad2/vem2;
    double sign3 = factorVEM*ch3*ad3/vem3;

    if(m1 && p1>0 && ch1>0){
      hlogS1->Fill(log10(sign1));
      if(sign1>=minSignal_fit && sign1<=maxSignal_fit)
        hS1->Fill(sign1);
      if(satBins1){
        hlogS1_sat->Fill(log10(sign1));
        if(sign1>=minSignal_fit && sign1<=maxSignal_fit)
          hS1_sat->Fill(sign1);
      }
      else{
        if(sign1>=minSignal_fit && sign1<=maxSignal_fit)
          hS1_notsat->Fill(sign1);
        hlogS1_notsat->Fill(log10(sign1));
        counter1_bis++;
      }
      counter1++;
    }

    if(m2 && p2>0 && ch2>0){
      hlogS2->Fill(log10(sign2));
      if(sign2>=minSignal_fit && sign2<=maxSignal_fit)
        hS2->Fill(sign2);
      if(satBins2){
        hlogS2_sat->Fill(log10(sign2));
        if(sign2>=minSignal_fit && sign2<=maxSignal_fit)
          hS2_sat->Fill(sign2);
      }
      else{
        hlogS2_notsat->Fill(log10(sign2));
        if(sign2>=minSignal_fit && sign2<=maxSignal_fit)
          hS2_notsat->Fill(sign2);
        counter2_bis++;
      }
      counter2++;
    }

    if(m3 && p3>0 && ch3>0){
      hlogS3->Fill(log10(sign3));
      if(sign3>=minSignal_fit && sign3<=maxSignal_fit)
        hS3->Fill(sign3);
      if(satBins3){
        hlogS3_sat->Fill(log10(sign3));
        if(sign3>=minSignal_fit && sign3<=maxSignal_fit)
          hS3_sat->Fill(sign3);
      }
      else{
        if(sign3>=minSignal_fit && sign3<=maxSignal_fit)
          hS3_notsat->Fill(sign3);
        hlogS3_notsat->Fill(log10(sign3));
        counter3_bis++;
      }
      counter3++;
    }

    if(!satBins4 && p4>0 && ch4>0){
      counter4++;
    }

    counter++;
  }


  float saturationValue = 0.15;

  cout << "Mean VEM1 = " << hVEM1->GetMean() << " in [" << minVEM1 << ", " << maxVEM1 << "]\n";
  cout << "Mean VEM2 = " << hVEM2->GetMean() << " in [" << minVEM2 << ", " << maxVEM2 << "]\n";
  cout << "Mean VEM3 = " << hVEM3->GetMean() << " in [" << minVEM3 << ", " << maxVEM3 << "]\n";

  for(int i=0;i<hlogS1->GetNbinsX();i++){
    if(hlogS1_sat->GetBinContent(i+1)>0){
      if(hlogS1_sat->GetBinContent(i+1)/hlogS1->GetBinContent(i+1)>saturationValue){
        cout << "LPMT1 saturating around log(S) = " << (i+0.5)*float(hlogS1->GetBinWidth(1)) << " = " << pow(10, (i+0.5)*hlogS1->GetBinWidth(1) ) << " VEM \n";
        break;
      }
    }
  }
  for(int i=0;i<hlogS2->GetNbinsX();i++){
    if(hlogS2_sat->GetBinContent(i+1)>0){
      if(hlogS2_sat->GetBinContent(i+1)/hlogS2->GetBinContent(i+1)>saturationValue){
        cout << "LPMT2 saturating around log(S) = " << (i+0.5)*hlogS2->GetBinWidth(1) << " = " << pow(10, (i+0.5)*hlogS2->GetBinWidth(1) ) << " VEM \n";
        break;
      }
    }
  }
  for(int i=0;i<hlogS3->GetNbinsX();i++){
    if(hlogS3_sat->GetBinContent(i+1)>0){
      if(hlogS3_sat->GetBinContent(i+1)/hlogS3->GetBinContent(i+1)>saturationValue){
        cout << "LPMT3 saturating around log(S) = " << (i+0.5)*hlogS3->GetBinWidth(1) << " = " << pow(10, (i+0.5)*hlogS3->GetBinWidth(1) ) << " VEM \n";
        break;
      }
    }
  }


////////////////////

  //gStyle->SetOptStat(2211);

  TFile* fOut = new TFile("output.root","RECREATE");

  ////////////////////

  TCanvas* c_LogSignalsHistos = new TCanvas("c_LogSignalsHistos","c_LogSignalsHistos",1200,1000);
  c_LogSignalsHistos->Divide(1,3);
  c_LogSignalsHistos->cd(1);
	c_LogSignalsHistos->cd(1)->SetLogy();
	hlogS1->SetLineWidth(3); hlogS1->SetLineColor(kBlack);
	hlogS1->GetXaxis()->SetTitle("log_{10}(S_{1}/VEM)");
	hlogS1->Draw("histo");
	hlogS1_sat->SetLineWidth(2); hlogS1_sat->SetLineColor(kRed+1);
	hlogS1_sat->Draw("histosames");
  hlogS1_notsat->SetLineWidth(2); hlogS1_notsat->SetLineColor(kBlue);
  hlogS1_notsat->Draw("histosames");
	gPad->Update();
  c_LogSignalsHistos->cd(2);
  c_LogSignalsHistos->cd(2)->SetLogy();
  hlogS2->SetLineWidth(3); hlogS2->SetLineColor(kBlack);
	hlogS2->GetXaxis()->SetTitle("log_{10}(S_{2}/VEM)");
	hlogS2->Draw("histo");
	hlogS2_sat->SetLineWidth(2); hlogS2_sat->SetLineColor(kRed+1);
	hlogS2_sat->Draw("histosames");
  hlogS2_notsat->SetLineWidth(2); hlogS2_notsat->SetLineColor(kBlue);
  hlogS2_notsat->Draw("histosames");
	gPad->Update();
  c_LogSignalsHistos->cd(3);
  c_LogSignalsHistos->cd(3)->SetLogy();
  hlogS3->SetLineWidth(3); hlogS3->SetLineColor(kBlack);
	hlogS3->GetXaxis()->SetTitle("log_{10}(S_{3}/VEM)");
	hlogS3->Draw("histo");
	hlogS3_sat->SetLineWidth(2); hlogS3_sat->SetLineColor(kRed+1);
	hlogS3_sat->Draw("histosames");
  hlogS3_notsat->SetLineWidth(2); hlogS3_notsat->SetLineColor(kBlue);
  hlogS3_notsat->Draw("histosames");
	gPad->Update();
  c_LogSignalsHistos->Write();


  cout << endl << endl;

  TCanvas* c_SignalsHistos = new TCanvas("c_SignalsHistos","c_SignalsHistos",1200,1000);
  c_SignalsHistos->Divide(1,3);
  c_SignalsHistos->cd(1);
	//c_SignalsHistos->cd(1)->SetLogy();
	hS1->SetLineWidth(3); hS1->SetLineColor(kBlack);
	hS1->GetXaxis()->SetTitle("S_{1} [VEM]");
	hS1->Draw("histo");
  hS1->SetMinimum(0.3);
	hS1_sat->SetLineWidth(2); hS1_sat->SetLineColor(kRed+1);
	hS1_sat->Draw("histosames");
  hS1_notsat->SetLineWidth(2); hS1_notsat->SetLineColor(kBlue);
  hS1_notsat->Draw("histosames");
    TF1 *pote1 = new TF1("pote1","[0]*pow(x/100,[1])",minSignal_fit,maxSignal_fit);
    pote1->SetParameter(0,1e3); pote1->SetParameter(1,-2.);
    //hS1->Scale(1./hS1->GetEntries());
    hS1->Fit("pote1","ILREM0","",minSignal_fit,maxSignal_fit);
    pote1->Draw("lsame");
    cout << endl;
	gPad->Update();
  c_SignalsHistos->cd(2);
  //c_SignalsHistos->cd(2)->SetLogy();
  hS2->SetLineWidth(3); hS2->SetLineColor(kBlack);
	hS2->GetXaxis()->SetTitle("S_{2} [VEM]");
	hS2->Draw("histo");
  hS2->SetMinimum(0.3);
	hS2_sat->SetLineWidth(2); hS2_sat->SetLineColor(kRed+1);
	hS2_sat->Draw("histosames");
  hS2_notsat->SetLineWidth(2); hS2_notsat->SetLineColor(kBlue);
  hS2_notsat->Draw("histosames");
    TF1 *pote2 = new TF1("pote2","[0]*pow(x/100,[1])",minSignal_fit,maxSignal_fit);
    pote2->SetParameter(0,1e3); pote2->SetParameter(1,-2.);
    //hS2->Scale(1./hS2->GetEntries());
    hS2->Fit("pote2","ILREM0","",minSignal_fit,maxSignal_fit);
    pote2->Draw("lsame");
    cout << endl;
	gPad->Update();
  c_SignalsHistos->cd(3);
  //c_SignalsHistos->cd(3)->SetLogy();
  hS3->SetLineWidth(3); hS3->SetLineColor(kBlack);
	hS3->GetXaxis()->SetTitle("S_{3} [VEM]");
	hS3->Draw("histo");
  hS3->SetMinimum(0.3);
	hS3_sat->SetLineWidth(2); hS3_sat->SetLineColor(kRed+1);
	hS3_sat->Draw("histosames");
  hS3_notsat->SetLineWidth(2); hS3_notsat->SetLineColor(kBlue);
  hS3_notsat->Draw("histosames");
    TF1 *pote3 = new TF1("pote3","[0]*pow(x/100,[1])",minSignal_fit,maxSignal_fit);
    pote3->SetParameter(0,1e3); pote3->SetParameter(1,-2.);
    //hS3->Scale(1./hS3->GetEntries());
    hS3->Fit("pote3","ILREM0","",minSignal_fit,maxSignal_fit);
    pote3->Draw("lsame");
    cout << endl;
	gPad->Update();
  c_SignalsHistos->Write();


  cout << endl << "Logarithmic fit" << endl;

  float minLogSignal_fit = log10(minSignal_fit);
  float maxLogSignal_fit = log10(maxSignal_fit);

  TF1 *line1 = new TF1("line1","[0] + [1]*x",minLogSignal_fit,maxLogSignal_fit);
  line1->SetLineColor(kRed);
  line1->SetParameter(0,1); line1->SetParameter(1,-2.);
  TGraphAsymmErrors *gLogSign1 = new TGraphAsymmErrors();
  int ccc=0;
  for(int i=0;i<hlogS1->GetNbinsX();i++){
    if(hlogS1->GetBinContent(i+1)>0 && hlogS1->GetBinCenter(i+1)>=minLogSignal_fit && hlogS1->GetBinCenter(i+1)<=maxLogSignal_fit){
      gLogSign1->SetPoint(ccc,hlogS1->GetBinCenter(i+1),log10(hlogS1->GetBinContent(i+1)));
      gLogSign1->SetPointError(ccc,hlogS1->GetBinWidth(i+1)/2.,hlogS1->GetBinWidth(i+1)/2.,
                                 log10(hlogS1->GetBinContent(i+1)) - log10(hlogS1->GetBinContent(i+1)-sqrt(hlogS1->GetBinContent(i+1))),
                                 log10(hlogS1->GetBinContent(i+1)+sqrt(hlogS1->GetBinContent(i+1))) - log10(hlogS1->GetBinContent(i+1)) );
      ccc++;
    }
  }
  gLogSign1->Fit("line1","REM0","",minLogSignal_fit,maxLogSignal_fit);
  cout << endl;

  TF1 *line2 = new TF1("line2","[0] + [1]*x",minLogSignal_fit,maxLogSignal_fit);
  line2->SetLineColor(kRed);
  line2->SetParameter(0,1); line2->SetParameter(1,-2.);
  TGraphAsymmErrors *gLogSign2 = new TGraphAsymmErrors();
  ccc=0;
  for(int i=0;i<hlogS2->GetNbinsX();i++){
    if(hlogS2->GetBinContent(i+1)>0 && hlogS2->GetBinCenter(i+1)>=minLogSignal_fit && hlogS2->GetBinCenter(i+1)<=maxLogSignal_fit){
      gLogSign2->SetPoint(ccc,hlogS2->GetBinCenter(i+1),log10(hlogS2->GetBinContent(i+1)));
      gLogSign2->SetPointError(ccc,hlogS2->GetBinWidth(i+1)/2.,hlogS2->GetBinWidth(i+1)/2.,
                                 log10(hlogS2->GetBinContent(i+1)) - log10(hlogS2->GetBinContent(i+1)-sqrt(hlogS2->GetBinContent(i+1))),
                                 log10(hlogS2->GetBinContent(i+1)+sqrt(hlogS2->GetBinContent(i+1))) - log10(hlogS2->GetBinContent(i+1)) );
      ccc++;
    }
  }
  gLogSign2->Fit("line2","REM0","",minLogSignal_fit,maxLogSignal_fit);
  cout << endl;

  TF1 *line3 = new TF1("line3","[0] + [1]*x",minLogSignal_fit,maxLogSignal_fit);
  line3->SetLineColor(kRed);
  line3->SetParameter(0,1); line3->SetParameter(1,-2.);
  TGraphAsymmErrors *gLogSign3 = new TGraphAsymmErrors();
  ccc=0;
  for(int i=0;i<hlogS3->GetNbinsX();i++){
    if(hlogS3->GetBinContent(i+1)>0 && hlogS3->GetBinCenter(i+1)>=minLogSignal_fit && hlogS3->GetBinCenter(i+1)<=maxLogSignal_fit){
      gLogSign3->SetPoint(ccc,hlogS3->GetBinCenter(i+1),log10(hlogS3->GetBinContent(i+1)));
      gLogSign3->SetPointError(ccc,hlogS3->GetBinWidth(i+1)/2.,hlogS3->GetBinWidth(i+1)/2.,
                                 log10(hlogS3->GetBinContent(i+1)) - log10(hlogS3->GetBinContent(i+1)-sqrt(hlogS3->GetBinContent(i+1))),
                                 log10(hlogS3->GetBinContent(i+1)+sqrt(hlogS3->GetBinContent(i+1))) - log10(hlogS3->GetBinContent(i+1)) );
      ccc++;
    }
  }
  gLogSign3->Fit("line3","REM0","",minLogSignal_fit,maxLogSignal_fit);
  cout << endl;

  TCanvas* c_LogSfit = new TCanvas("c_LogSfit","c_LogSfit",1200,800);
  c_LogSfit->Divide(3,1);
  c_LogSfit->cd(1);
  gLogSign1->SetMarkerStyle(24); gLogSign1->SetMarkerSize(1.5);
  gLogSign1->GetXaxis()->SetTitle("log(S/VEM)");
  gLogSign1->GetYaxis()->SetTitle("log(N. bin entries)");
  gLogSign1->Draw("ap");
  line1->Draw("same");
  gPad->Update();
  c_LogSfit->cd(2);
  gLogSign2->SetMarkerStyle(24); gLogSign2->SetMarkerSize(1.5);
  gLogSign2->GetXaxis()->SetTitle("log(S/VEM)");
  gLogSign2->GetYaxis()->SetTitle("log(N. bin entries)");
  gLogSign2->Draw("ap");
  line2->Draw("same");
  gPad->Update();
  c_LogSfit->cd(3);
  gLogSign3->SetMarkerStyle(24); gLogSign3->SetMarkerSize(1.5);
  gLogSign3->GetXaxis()->SetTitle("log(S/VEM)");
  gLogSign3->GetYaxis()->SetTitle("log(N. bin entries)");
  gLogSign3->Draw("ap");
  line3->Draw("same");
  gPad->Update();
  c_LogSfit->Write();


////////////////////

  TCanvas* c_VEM_AoD_histos = new TCanvas("c_VEM_AoD_histos","c_VEM_AoD_histos",1800,900);
  c_VEM_AoD_histos->Divide(3,2);
  c_VEM_AoD_histos->cd(1);
	c_VEM_AoD_histos->cd(1)->SetLogy();
	hVEM1->SetLineWidth(2); hVEM1->SetLineColor(kBlack);
	hVEM1->GetXaxis()->SetTitle("VEM charge - LPMT1");
	hVEM1->Draw("histo");
	gPad->Update();
  c_VEM_AoD_histos->cd(2);
	c_VEM_AoD_histos->cd(2)->SetLogy();
	hVEM2->SetLineWidth(2); hVEM2->SetLineColor(kBlack);
	hVEM2->GetXaxis()->SetTitle("VEM charge - LPMT2");
	hVEM2->Draw("histo");
	gPad->Update();
  c_VEM_AoD_histos->cd(3);
	c_VEM_AoD_histos->cd(3)->SetLogy();
	hVEM3->SetLineWidth(2); hVEM3->SetLineColor(kBlack);
	hVEM3->GetXaxis()->SetTitle("VEM charge - LPMT3");
	hVEM3->Draw("histo");
	gPad->Update();
  c_VEM_AoD_histos->cd(4);
	c_VEM_AoD_histos->cd(4)->SetLogy();
	hAoD1->SetLineWidth(2); hAoD1->SetLineColor(kBlack);
	hAoD1->GetXaxis()->SetTitle("Anode over Dynode ratio - LPMT1");
	hAoD1->Draw("histo");
	gPad->Update();
  c_VEM_AoD_histos->cd(5);
	c_VEM_AoD_histos->cd(5)->SetLogy();
	hAoD2->SetLineWidth(2); hAoD2->SetLineColor(kBlack);
	hAoD2->GetXaxis()->SetTitle("Anode over Dynode ratio - LPMT2");
	hAoD2->Draw("histo");
	gPad->Update();
  c_VEM_AoD_histos->cd(6);
	c_VEM_AoD_histos->cd(6)->SetLogy();
	hAoD3->SetLineWidth(2); hAoD3->SetLineColor(kBlack);
	hAoD3->GetXaxis()->SetTitle("Anode over Dynode ratio - LPMT3");
	hAoD3->Draw("histo");
	gPad->Update();
  c_VEM_AoD_histos->Write();
  c_VEM_AoD_histos->Close();

////////////////////

  TCanvas* c_VEM_AoD_vsTime = new TCanvas("c_VEM_AoD_vsTime","c_VEM_AoD_vsTime",1800,900);
  c_VEM_AoD_vsTime->Divide(3,2);

  c_VEM_AoD_vsTime->cd(1);
	gVEM1->GetXaxis()->SetTitle("time from 01/01/2018 [days]");
	gVEM1->GetYaxis()->SetTitle("VEM charge - LPMT1");
	gVEM1->Draw("ap");
  gVEM1->GetYaxis()->SetRangeUser(1001,1999);
	pVEM1->SetLineColor(kRed); pVEM1->SetMarkerColor(kRed);
	pVEM1->Draw("same");
	gPad->Update();
  c_VEM_AoD_vsTime->cd(2);
	gVEM2->GetXaxis()->SetTitle("time from 01/01/2018 [days]");
	gVEM2->GetYaxis()->SetTitle("VEM charge - LPMT2");
	gVEM2->Draw("ap");
  gVEM2->GetYaxis()->SetRangeUser(1001,1999);
	pVEM2->SetLineColor(kRed); pVEM2->SetMarkerColor(kRed);
	pVEM2->Draw("same");
	gPad->Update();
  c_VEM_AoD_vsTime->cd(3);
	gVEM3->GetXaxis()->SetTitle("time from 01/01/2018 [days]");
	gVEM3->GetYaxis()->SetTitle("VEM charge - LPMT3");
	gVEM3->Draw("ap");
  gVEM3->GetYaxis()->SetRangeUser(1001,1999);
	pVEM3->SetLineColor(kRed); pVEM3->SetMarkerColor(kRed);
	pVEM3->Draw("same");
	gPad->Update();
  c_VEM_AoD_vsTime->cd(4);
	gAoD1->GetXaxis()->SetTitle("time from 01/01/2018 [days]");
	gAoD1->GetYaxis()->SetTitle("Anode over Dynode ratio - LPMT1");
	gAoD1->Draw("ap");
  gAoD1->GetYaxis()->SetRangeUser(25.1,34.9);
	pAoD1->SetLineColor(kRed); pAoD1->SetMarkerColor(kRed);
	pAoD1->Draw("same");
	gPad->Update();
  c_VEM_AoD_vsTime->cd(5);
	gAoD2->GetXaxis()->SetTitle("time from 01/01/2018 [days]");
	gAoD2->GetYaxis()->SetTitle("Anode over Dynode ratio - LPMT2");
	gAoD2->Draw("ap");
  gAoD2->GetYaxis()->SetRangeUser(25.1,34.9);
	pAoD2->SetLineColor(kRed); pAoD2->SetMarkerColor(kRed);
	pAoD2->Draw("same");
	gPad->Update();
  c_VEM_AoD_vsTime->cd(6);
	gAoD3->GetXaxis()->SetTitle("time from 01/01/2018 [days]");
	gAoD3->GetYaxis()->SetTitle("Anode over Dynode ratio - LPMT3");
	gAoD3->Draw("ap");
  gAoD3->GetYaxis()->SetRangeUser(25.1,34.9);
	pAoD3->SetLineColor(kRed); pAoD3->SetMarkerColor(kRed);
	pAoD3->Draw("same");
	gPad->Update();
  c_VEM_AoD_vsTime->Write();

////////////////////////////

  fOut->Close();

}
