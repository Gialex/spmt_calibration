#ifndef SPMT_PROT
#define SPMT_PROT
#include <stdint.h>
/* structures used to communicate between station and server related with
   the small PMT. 

   At this point, the idea is to transfer a complete trace plus a histogram.
   
   The histogram may not be transfered, in the case the histogram have
   already been transfered a little before (maybe exactly the same histogram).
*/

#include "shwr_evt_defs.h"
#include "muonfill.h"


#define SPMT_MAX_PACKET_SIZE 10240

struct spmt_h
{
  uint16_t Id;
  uint16_t nevt; /*just to track if it have lost event, it would
		   return a simple counter*/
  uint16_t size; /*real buffer size */
};

struct spmt_msg
{
  struct spmt_h h;
  char dat[SPMT_MAX_PACKET_SIZE]; /*compressed data. When uncompressed it would be as
				    the structure spmt_dat_str */
};

struct muon_histo_param
{
  uint32_t StartSec;
  uint16_t base[4],peak[4],charge[4];
};
  
#define BUFF_SIZE_MAX 8192
struct buff_str
{
  int used;
  char dat[BUFF_SIZE_MAX];
};

struct evt_features
{
  uint32_t sec,ticks,nevt;
  struct shwr_evt_extra extra;
};

enum buff{
  EVT=1,
  MUON_INFO,
  DAQ_INFO
};

#endif
