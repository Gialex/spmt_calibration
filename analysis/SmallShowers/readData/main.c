#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include "spmt_read.h"
//#include "Technique2.h"

#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>

int main(int argc,char *argv[])
{

  struct spmt_read_str *spmt_rd;
  struct muon_histo_param mm;
  struct spmt_add_info    th;
  struct evt_features     evt;
  //struct Calib spmt_calib
;
  int aux,i;
  if(argc<2){
    printf("use as: %s <inputfile1> [inputfile2,...]\n",argv[0]);
    return(0);
  }
  spmt_rd=spmt_read_init(argc-1,argv+1);

// Gialex 03/07/19
// To know in advance the number of events one has to open all the files,
// then re-inizialize and start the true calibration cycle.
// This is not convenient in general, so it is preferred a large fixed variable
// (called MAX_EVT) instead.
//  aux=0;
//  while(spmt_read_getevt(spmt,&mm,&th,&evt)==0){
//    aux++;
//  }
//  printf("N. events: %d", aux);
//  spmt_read_finish(spmt);
//  spmt=spmt_read_init(argc-1,argv+1);

  int MAX_EVT = 1000000000; // Gialex 03/07/19
  //spmt_calib=SPMTcal(spmt,MAX_EVT); //F. Fenu 21/03/2019. Needed to work on tank 39.

  //memset(&Result,0,sizeof(Result));

  if(spmt_read_getevt(spmt_rd,&mm,&th,&evt)!=0)
    return(0);				//not implemented function that fills the information for the structs

// Gialex 03/07/19 --> using the 'spmt_read_getevt' function
// to check if there are events to start with, causes the loss
// of the first event for the calibration

  int totalsmall=0; // Number of events

  //std::ofstream myfile;
  //myfile.open ("results.txt");

  TFile* f = new TFile("data.root", "RECREATE");
    bool m1, m2, m3, sat1, sat2, sat3, sat1_hg, sat2_hg, sat3_hg, sat4;
    int GPS, ch1, ch2, ch3, ch1_hg, ch2_hg, ch3_hg, ch4;
    int peak1, peak2, peak3, peak1_hg, peak2_hg, peak3_hg, peak4;
    double ad1, ad2, ad3, VEM1, VEM2, VEM3, base1, base2, base3;
    int tr1, tr2, tr3;
    int GPStime = 0;
    int GPStimeVEM = 0;
    int GPStimeTrigger = 0;

  TTree* t = new TTree("data", "data");
  t->Branch("GPS_sec", &GPStime);
  t->Branch("GPS_sec_VEMcalib", &GPStimeVEM);
  t->Branch("GPS_sec_trigger", &GPStimeTrigger);

  t->Branch("MaskedStatus_LPMT1", &m1);
  t->Branch("Peak_FADC_counts_LG_LPMT1", &peak1);
  t->Branch("Charge_FADC_counts_LG_LPMT1", &ch1);
  t->Branch("SaturationStatus_LG_LPMT1", &sat1);
  t->Branch("Peak_FADC_counts_HG_LPMT1", &peak1_hg);
  t->Branch("Charge_FADC_counts_HG_LPMT1", &ch1_hg);
  t->Branch("SaturationStatus_HG_LPMT1", &sat1_hg);
  t->Branch("AoD_LPMT1", &ad1);
  t->Branch("VEM_charge_HG_LPMT1", &VEM1);
  t->Branch("VEM_baseline_LPMT1", &base1);
  t->Branch("triggerValue_LPMT1", &tr1);

  t->Branch("MaskedStatus_LPMT2", &m2);
  t->Branch("Peak_FADC_counts_LG_LPMT2", &peak2);
  t->Branch("Charge_FADC_counts_LG_LPMT2", &ch2);
  t->Branch("SaturationStatus_LG_LPMT2", &sat2);
  t->Branch("Peak_FADC_counts_HG_LPMT2", &peak2_hg);
  t->Branch("Charge_FADC_counts_HG_LPMT2", &ch2_hg);
  t->Branch("SaturationStatus_HG_LPMT2", &sat2_hg);
  t->Branch("AoD_LPMT2", &ad2);
  t->Branch("VEM_charge_HG_LPMT2", &VEM2);
  t->Branch("VEM_baseline_LPMT2", &base2);
  t->Branch("triggerValue_LPMT2", &tr2);

  t->Branch("MaskedStatus_LPMT3", &m3);
  t->Branch("Peak_FADC_counts_LG_LPMT3", &peak3);
  t->Branch("Charge_FADC_counts_LG_LPMT3", &ch3);
  t->Branch("SaturationStatus_LG_LPMT3", &sat3);
  t->Branch("Peak_FADC_counts_HG_LPMT3", &peak3_hg);
  t->Branch("Charge_FADC_counts_HG_LPMT3", &ch3_hg);
  t->Branch("SaturationStatus_HG_LPMT3", &sat3_hg);
  t->Branch("AoD_LPMT3", &ad3);
  t->Branch("VEM_charge_HG_LPMT3", &VEM3);
  t->Branch("VEM_baseline_LPMT3", &base3);
  t->Branch("triggerValue_LPMT3", &tr3);

  t->Branch("Peak_FADC_counts_sPMT", &peak4);
  t->Branch("Charge_FADC_counts_sPMT", &ch4);
  t->Branch("SaturationStatus_sPMT", &sat4);

  for(int i=0;i<MAX_EVT;i++) {

    //printf("nevt: %d\n",i);

    if(spmt_read_getevt(spmt_rd,&mm,&th,&evt)!=0){
      printf("nevt: %d\n",i);
      break;
    }

// Gialex 03/07/19
// Control if the given maximum number of events is reached
    if(i==MAX_EVT-1) printf("\nMax number of events (= %d) reached\n\n",MAX_EVT);

   totalsmall++;

   GPStime = evt.sec;

   ch1 = (evt.extra.AREA_PEAK[0]& 0x7FFFF);
   ch2 = (evt.extra.AREA_PEAK[2]& 0x7FFFF);
   ch3 = (evt.extra.AREA_PEAK[4]& 0x7FFFF);
   ch4 = (evt.extra.AREA_PEAK[6]& 0x7FFFF);
   peak1 = ((evt.extra.AREA_PEAK[0]>>19)& 0xFFF);
   peak2 = ((evt.extra.AREA_PEAK[2]>>19)& 0xFFF);
   peak3 = ((evt.extra.AREA_PEAK[4]>>19)& 0xFFF);
   peak4 = ((evt.extra.AREA_PEAK[6]>>19)& 0xFFF);
   sat1 = ((evt.extra.AREA_PEAK[0]>>31)&0x1);
   sat2 = ((evt.extra.AREA_PEAK[2]>>31)&0x1);
   sat3 = ((evt.extra.AREA_PEAK[4]>>31)&0x1);
   sat4 = ((evt.extra.AREA_PEAK[6]>>31)&0x1);

   ch1_hg = (evt.extra.AREA_PEAK[0]& 0x7FFFF);
   ch2_hg = (evt.extra.AREA_PEAK[2]& 0x7FFFF);
   ch3_hg = (evt.extra.AREA_PEAK[4]& 0x7FFFF);
   peak1_hg = ((evt.extra.AREA_PEAK[0]>>19)& 0xFFF);
   peak2_hg = ((evt.extra.AREA_PEAK[2]>>19)& 0xFFF);
   peak3_hg = ((evt.extra.AREA_PEAK[4]>>19)& 0xFFF);
   sat1_hg = ((evt.extra.AREA_PEAK[0]>>31)&0x1);
   sat2_hg = ((evt.extra.AREA_PEAK[2]>>31)&0x1);
   sat3_hg = ((evt.extra.AREA_PEAK[4]>>31)&0x1);

   m1 = ((th.mask & 1) == 1);
   m2 = ((th.mask & 2) == 2);
   m3 = ((th.mask & 4) == 4);

   ad1 = (th.r[0]!=0 ? th.r[0]/100. : 32.);
   ad2 = (th.r[1]!=0 ? th.r[1]/100. : 32.);
   ad3 = (th.r[2]!=0 ? th.r[2]/100. : 32.);

   GPStimeTrigger = th.t_start;
   tr1 = th.th[0];
   tr2 = th.th[1];
   tr3 = th.th[2];

   GPStimeVEM = mm.StartSec;
   VEM1 = (mm.charge[0]<<=3);
   VEM2 = (mm.charge[1]<<=3);
   VEM3 = (mm.charge[2]<<=3);
   base1 = (mm.base[0]/100.);
   base2 = (mm.base[1]/100.);
   base3 = (mm.base[2]/100.);

   t->Fill();

  }//end loop on events

  printf("\n %d events written on file \'data.root\'\n", totalsmall);

  //myfile.close();
  t->Write();
  f->Close();

  spmt_read_finish(spmt_rd); //F. Fenu 21/03/2019

}
