#include "shwr_evt_defs.h"

struct spmt_shm_str
{
  int pmt_mask; /*which PMT would be considered in "trigger". 
		  bit 0: PMT1, bit 1: PMT2 and bit 2: PMT3 */
  int fd;
  int size;
  char *addr;
};

/* struct of the spmt event - it may include some additional information
   beside the information of event itself.
   
*/

struct spmt_add_info 
{
  int16_t th[3];//threshold of the three PMTs.
  int16_t mask;
  int32_t t_start;
  int16_t r[3];
};
struct spmt_evt
{
  struct shwr_evt_complete evt;
  struct spmt_add_info info;
};


/*functions would be used by the server (Trigger2) */
int spmt_open(struct spmt_shm_str *p,int size,int ev_size);
void spmt_close(struct spmt_shm_str *p);
char *spmt_get_address(struct spmt_shm_str *p); /*get an address to fill with event information */
void spmt_wr_ev(struct spmt_shm_str *p); /* write the event */

/*functions would be used by the client */
int spmt_link(struct spmt_shm_str *p);
void spmt_finish(struct spmt_shm_str *p);
int spmt_get_evt(struct spmt_shm_str *p,char *evt,int *nev); /* get an event or wait for one event - up to 10 seconds */
int spmt_set_peak(struct spmt_shm_str *p,
		  int peak[],
		  int tt,//event time
		  struct spmt_add_info *info);

