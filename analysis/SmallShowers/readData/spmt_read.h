#ifndef SPMT_READ

#define SPMT_READ


#include "spmt_prot.h"
#include "spmt_shm.h"
#define BUFF_SIZE SPMT_MAX_PACKET_SIZE+10

struct spmt_read_str
{
  char **flist;
  int nfiles;
  unsigned int *UnixTimes; // Gialex 05/07/19
  int ifile;
  FILE *arq;
  unsigned int nd_raw,pt,nd;
  char raw_buff[BUFF_SIZE],buff[BUFF_SIZE];
  struct muon_histo_param mm;
  struct spmt_add_info th;
  struct evt_features evt;

};
struct spmt_read_str *spmt_read_init(int nfiles,char *flist[]);
void spmt_read_finish(struct spmt_read_str *str);
int spmt_read_raw_data(struct spmt_read_str *str); //this would not be needed in the header
int spmt_read_getevt(struct spmt_read_str *str,
		     struct muon_histo_param *mm,
		     struct spmt_add_info *th,
		     struct evt_features *evt);

//int spmt_read_getevt(struct spmt_read_str *str);
#endif 
