#ifndef _MUONFILL
#define _MUONFILL
#include <stdint.h>
#define NBBIN 69

struct muon_histo {
  uint16_t Offset[4];
  uint16_t Base[4][20];
  uint16_t Peak[4][150];  // note: this is bitshifted by 2 (ie x4)
  uint16_t Charge[4][600]; // note: this is bitshifter by 3 (ie x8)
  int32_t Shape[4][NBBIN]; // note: baseline substracted (can be negative)
  uint32_t StartSecond;
  uint32_t EndSecond;
  uint32_t NEntries;
};

#define MUON_BUFF_NAME "muon_buff"
#define MUON_BUFF_SIZE 11*sizeof(struct muon_histo)

#endif
