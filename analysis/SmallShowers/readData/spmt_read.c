//#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <bzlib.h>

#include "spmt_read.h"
//#include <spmt_shm.h>
#include "shwr_evt_defs.h"
#include "muonfill.h"
#include "spmt_prot.h"

#define BUFF_SIZE SPMT_MAX_PACKET_SIZE+10


int spmt_read_open_file(struct spmt_read_str *str)
{
  //return 0 if there are more files to be read
  //      1 if there are not more files.
  //      It may happen that files could be not openned

  if(str->arq!=NULL){
    fclose(str->arq);
    str->arq=NULL;
  }
  while(str->ifile<str->nfiles && str->arq==NULL){
    str->arq=fopen(str->flist[str->ifile],"r");
    //debug
    printf("going to open file %s\n",str->flist[str->ifile]);

/*
/////  // Gialex 05/07/19
// Unix time for the file
    char * pch;
    pch=strrchr(str->flist[str->ifile],'_');
    //printf ("Last occurence of '_' found at %d \t",pch-str->flist[str->ifile]); // debug
    char time[11];
    memcpy( time, &(str->flist[str->ifile])[pch-str->flist[str->ifile]+1], 10);
    time[10] = '\0';
    //printf ("--> %s \t",time); // debug
    str->UnixTimes[str->ifile] = strtoul (time, NULL, 0);
    //printf ("--> %u \n",str->UnixTimes[str->ifile]); // debug
/////
*/
    str->ifile++;
  }//close while

  if(str->arq==NULL)
    return(1);

  /*as each file would contain self contained data, it would
    the read variables would be all reseted, discarding all
    the previous data.
  */
  str->nd_raw=0;
  str->pt=0;
  str->nd=0;
  return(0);
}

struct spmt_read_str *spmt_read_init(int nfiles,char *flist[])
{
  struct spmt_read_str *str;
  str=(struct spmt_read_str *)malloc(sizeof(struct spmt_read_str));
  if(str!=NULL){
    str->flist=flist;
    str->nfiles=nfiles;
    //str->UnixTimes=(unsigned int*)malloc(nfiles*sizeof(unsigned int)); // Gialex 05/07/19
    str->ifile=0;
    str->arq=NULL;
    if(spmt_read_open_file(str)!=0){
      free(str);
      printf("No file is possible to read\n");
      return(NULL);
    }
    str->nd_raw=0;
    str->pt=0;
    str->nd=0;
  } else {
    printf("Not possible to allocate the memory for SPMT data management\n");
    return NULL;
  }
  return(str);
}

void spmt_read_finish(struct spmt_read_str *str)
{
  if(str!=NULL){
    if(str->arq!=NULL)
      fclose(str->arq);
    free(str);
  }
}
    
int spmt_read_raw_data(struct spmt_read_str *str)
{
  struct spmt_h h;
  char *pt;
  int n,nd,missing_data,bzerr;
  do{
    //debug printf("Enter in new data loop \n");
    missing_data=1;
    if(4 + sizeof(struct spmt_h)  < str->nd_raw ){
      //debug printf("there are some data ... %d\n",str->nd_raw);
      pt=(char *)memmem((void *)str->raw_buff,str->nd_raw,"SPMT",4);
      if(pt!=NULL){
	n=pt - str->raw_buff ;
	//debug	printf("Preamble found ... %d\n",n);
	if(pt!=str->raw_buff && n < str->nd_raw ){
	  memmove(str->raw_buff, pt, str->nd_raw - n );
	  str->nd_raw -=n;
	}
	if(4 + sizeof(struct spmt_h) < str->nd_raw){
	  memcpy(&h,str->raw_buff + 4,sizeof(h)); //4 is to skeep "SPMT"
	  //debug printf("Preamble found xx ... %d %d %d\n",h.Id,h.nevt,h.size);
	  if(4 + sizeof(h) + h.size <= str->nd_raw){
	    //there are enough data for new message ...
	    //debug printf("Preamble found ... %d\n",n);
	    pt=str->raw_buff + 4 + sizeof(h);
	    str->nd=BUFF_SIZE;
	    bzerr=BZ2_bzBuffToBuffDecompress(str->buff,&(str->nd),
					     pt,h.size,0,0 );
	    if(BZ_OK==bzerr){
	      str->pt=0;
	      //debug printf("uncompressed data: %d\n",str->nd);
	      missing_data=0;
	    } else {
	      printf("BZIP error!!! %d\n",bzerr);
	    }
	    n=h.size + 4 + sizeof(h) ;
	    if(str->nd_raw - n >0){
	      pt=str->raw_buff + n;
	      memmove(str->raw_buff,pt,str->nd_raw - n);
	      str->nd_raw -=n;
	    } else {
	      str->nd_raw=0;
	    }
	  } //debug else {
	    //debug printf("not Enough data ...\n");
	    //debug }
	  
	} // else  missing data ...
      } else {
	//preamble not found. It may discart all the remind data.
	str->nd_raw=0;
      }
    }
    if(missing_data){
      //not enough data to process. make another read ...
      //debug printf("going to read more data %d %d %x\n",str->nd_raw,BUFF_SIZE,str->arq);
      if(str->arq==NULL){
	return(1);
      }
      nd=fread(str->raw_buff+str->nd_raw,1,
	       BUFF_SIZE - str->nd_raw,
	       str->arq);
      //printf("read %d\n",nd);
      if(0 < nd){
	str->nd_raw+=nd;
      } else {
	if(spmt_read_open_file(str)!=0){
	  return(1); //no more files to be read.
	}
      }      
    }
  }while(missing_data);
  return(0);
}


int spmt_read_getevt(struct spmt_read_str *str,
		     struct muon_histo_param *mm,
		     struct spmt_add_info *th,
		     struct evt_features *evt)
{
  int nd;
  int ok;
  int new_data;
  do{
    //debug printf("Enter in the loop for read_evt ...\n");
    ok=1;new_data=0;
    while( (str->pt <= str->nd ) &&
	   (str->buff[str->pt] > 0) &&
	   new_data == 0){ //there are not more data, need to request
			   //more data from the file.

      if(str->buff[str->pt + 1 ] == DAQ_INFO){
	//debug printf("DAQ_INFO\n");
	memcpy(&(str->th),&(str->buff[str->pt + 2]),str->buff[str->pt]-2);
      } else if(str->buff[str->pt + 1 ] == MUON_INFO){
	//debug printf("MUON_INFO\n");
	memcpy(&(str->mm),&(str->buff[str->pt + 2]),str->buff[str->pt]-2);	  
      } else if(str->buff[str->pt + 1 ] == EVT){
	//debug printf("Get EVT...\n");
	memcpy(&(str->evt),&(str->buff[str->pt + 2]),str->buff[str->pt]-2);

/*
    // Gialex 05/07/19
	int time_shift = (unsigned int)(str->evt.sec) + 315964800 - str->UnixTimes[0]; // 315964800 ==> shift between GPS and UNIX epoch
	//printf("Event %d time shift = %d\n",str->evt.nevt,time_shift); // debug

	int max_time_shift = (360000 * str->nfiles) + 60; // This condition allows to discrard both the events with bugged GPS time
							 // and to take into account possible holes in the data - Gialex 05/07/19 
        if( time_shift > max_time_shift || time_shift < -max_time_shift ){
	   printf("Event %d has strange time (GPS %d s -- UNIX %d s -- diff = %d s ) and will not be used\n",str->evt.nevt,str->evt.sec,str->UnixTimes[0],time_shift); // debug
	} else{
	   memcpy(evt,&(str->evt),sizeof(str->evt));
	   memcpy(mm,&(str->mm)  ,sizeof(str->mm));
	   memcpy(th,&(str->th)  ,sizeof(str->th));
	   new_data=1;
	   //printf("%d %d\n",str->evt.sec,str->evt.ticks); // debug
	}
*/

	memcpy(evt,&(str->evt),sizeof(str->evt));
	memcpy(mm,&(str->mm)  ,sizeof(str->mm));
	memcpy(th,&(str->th)  ,sizeof(str->th));
	new_data=1;
	   //printf("%d %d\n",str->evt.sec,str->evt.ticks); // debug

      }
      str->pt+=str->buff[str->pt];
      //debug printf("%d ... %d\n",str->pt,str->nd);
    }

    if(new_data==0){
      //debug printf("going to get more data ...\n");
      //no data has been found in the existing buffer. Need to look
      // for additional data.
      //look for additional data in the same file:
      if(spmt_read_raw_data(str)!=0){
	//it try to read more data and include in str->buff
	ok=0;
      }
    }
    
  }while (ok && new_data==0);

  if(ok) return(0);
  return(1);
   
}
